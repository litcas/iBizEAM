/**
 * 文档引用
 *
 * @export
 * @interface EMDRWGMap
 */
export interface EMDRWGMap {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    createdate?: any;

    /**
     * 文档引用
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    emdrwgmapname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    description?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    updatedate?: any;

    /**
     * 文档引用标识
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    emdrwgmapid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    createman?: any;

    /**
     * 引用对象
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    refobjname?: any;

    /**
     * 文档
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    drwgname?: any;

    /**
     * 引用对象
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    refobjid?: any;

    /**
     * 文档
     *
     * @returns {*}
     * @memberof EMDRWGMap
     */
    drwgid?: any;
}