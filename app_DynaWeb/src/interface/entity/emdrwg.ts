/**
 * 文档
 *
 * @export
 * @interface EMDRWG
 */
export interface EMDRWG {

    /**
     * 最后借阅人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    bpersonid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    createman?: any;

    /**
     * 内容
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    content?: any;

    /**
     * 保管人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    rempid?: any;

    /**
     * 文档代码
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    drwgcode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    updateman?: any;

    /**
     * 保管人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    rempname?: any;

    /**
     * 档案文件
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    efilecontent?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    description?: any;

    /**
     * 文档类型
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    drwgtype?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    enable?: any;

    /**
     * 存放位置
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    lct?: any;

    /**
     * 文档名称
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    emdrwgname?: any;

    /**
     * 文档状态
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    drwgstate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    orgid?: any;

    /**
     * 文档标识
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    emdrwgid?: any;

    /**
     * 文档信息
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    drwginfo?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    updatedate?: any;

    /**
     * 最后借阅人
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    bpersonname?: any;

    /**
     * 部门查询
     *
     * @returns {*}
     * @memberof EMDRWG
     */
    deptid?: any;
}