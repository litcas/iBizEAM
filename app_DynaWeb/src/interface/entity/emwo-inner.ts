/**
 * 内部工单
 *
 * @export
 * @interface EMWO_INNER
 */
export interface EMWO_INNER {

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    content?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emwotype?: any;

    /**
     * 工单编号
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emwo_innerid?: any;

    /**
     * 实际工时(分)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    worklength?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wogroup?: any;

    /**
     * 工单类型
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wotype?: any;

    /**
     * 等待配件时(分)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    waitbuy?: any;

    /**
     * 工单状态
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wostate?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    val?: any;

    /**
     * 等待修理时(分)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    waitmodi?: any;

    /**
     * 过期日期
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    expiredate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    createman?: any;

    /**
     * 是否直接关闭工单
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    closeflag?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    mdate?: any;

    /**
     * 工单内容
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wodesc?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wfstate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    orgid?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    priority?: any;

    /**
     * 影响船舶
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    yxcb?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    nval?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    activelengths?: any;

    /**
     * 工单名称
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emwo_innername?: any;

    /**
     * 执行日期
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wodate?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    archive?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    regionenddate?: any;

    /**
     * 转计划标志
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    cplanflag?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    woteam?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wfstep?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    enable?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    description?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    prefee?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    regionbegindate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    updatedate?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    vrate?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    eqstoplength?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wfinstanceid?: any;

    /**
     * 执行结果
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wresult?: any;

    /**
     * 约合材料费(￥)
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    mfee?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    updateman?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    dpname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfodename?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfoacname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfomoname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rteamname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    equipname?: any;

    /**
     * 钢丝绳位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeqlctgssname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rservicename?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wopname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    acclassname?: any;

    /**
     * 轮胎清单
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeitiresname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    objname?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wooriname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfocaname?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    dptype?: any;

    /**
     * 轮胎位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeqlcttiresname?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wooritype?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    objid?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wooriid?: any;

    /**
     * 钢丝绳位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeqlctgssid?: any;

    /**
     * 轮胎清单
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeitiresid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    acclassid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfoacid?: any;

    /**
     * 轮胎位置
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    emeqlcttiresid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfomoid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    dpid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    equipid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfocaid?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wopid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rfodeid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rteamid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rserviceid?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rdeptid?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rdeptname?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    mpersonid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    mpersonname?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rempid?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    rempname?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    recvpersonid?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    recvpersonname?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wpersonid?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_INNER
     */
    wpersonname?: any;
}