/**
 * 总帐科目
 *
 * @export
 * @interface EMACClass
 */
export interface EMACClass {

    /**
     * 总帐科目标识
     *
     * @returns {*}
     * @memberof EMACClass
     */
    emacclassid?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof EMACClass
     */
    code?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMACClass
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMACClass
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMACClass
     */
    enable?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMACClass
     */
    description?: any;

    /**
     * 总帐科目名称
     *
     * @returns {*}
     * @memberof EMACClass
     */
    emacclassname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMACClass
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMACClass
     */
    updatedate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMACClass
     */
    orgid?: any;
}