/**
 * 设备仪表读数
 *
 * @export
 * @interface EMEQMPMTR
 */
export interface EMEQMPMTR {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    createdate?: any;

    /**
     * 设备仪表读数标识
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    emeqmpmtrid?: any;

    /**
     * 设备仪表读数名称
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    emeqmpmtrname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    updateman?: any;

    /**
     * 采集时间
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    edate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    description?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    nval?: any;

    /**
     * 上次采集时间
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    bdate?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    val?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    objname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    woname?: any;

    /**
     * 仪表
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    mpname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    equipname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    woid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    equipid?: any;

    /**
     * 仪表
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    mpid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMPMTR
     */
    objid?: any;
}