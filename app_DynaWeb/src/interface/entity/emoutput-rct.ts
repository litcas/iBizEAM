/**
 * 产能
 *
 * @export
 * @interface EMOutputRct
 */
export interface EMOutputRct {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    updateman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    orgid?: any;

    /**
     * 产能区间起始
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    edate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    updatedate?: any;

    /**
     * 产能区间截至
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    bdate?: any;

    /**
     * 产能名称
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    emoutputrctname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    createman?: any;

    /**
     * 产能值
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    nval?: any;

    /**
     * 产能标识
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    emoutputrctid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    equipname?: any;

    /**
     * 能力
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    outputname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    objname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    woname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    woid?: any;

    /**
     * 能力
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    outputid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    equipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMOutputRct
     */
    objid?: any;
}