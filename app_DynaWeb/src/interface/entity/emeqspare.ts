/**
 * 备件包
 *
 * @export
 * @interface EMEQSpare
 */
export interface EMEQSpare {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    updateman?: any;

    /**
     * 备件包标识
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    emeqspareid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    createdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    enable?: any;

    /**
     * 备件包名称
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    emeqsparename?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    orgid?: any;

    /**
     * 备件包代码
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    eqsparecode?: any;

    /**
     * 备件包信息
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    eqspareinfo?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    description?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQSpare
     */
    updatedate?: any;
}