/**
 * 维护保养
 *
 * @export
 * @interface EMEQKeep
 */
export interface EMEQKeep {

    /**
     * 保养记录名称
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    emeqkeepname?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rempid?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    orgid?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    activelengths?: any;

    /**
     * 保养记录标识
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    emeqkeepid?: any;

    /**
     * 维护保养结果
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    activeadesc?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    enable?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    sfee?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    eqstoplength?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rdeptname?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    regionbegindate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rdeptid?: any;

    /**
     * 维护保养原因
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    activebdesc?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    updateman?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    prefee?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rempname?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    content?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    description?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    pfee?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    mfee?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    createman?: any;

    /**
     * 维护保养日期
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    activedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    createdate?: any;

    /**
     * 维护保养记录
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    activedesc?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    updatedate?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    regionenddate?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfomoname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    objname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfodename?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    equipname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rservicename?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfocaname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rteamname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    acclassname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    woname?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfoacname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rteamid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    woid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfocaid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfodeid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    acclassid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfoacid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rserviceid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    rfomoid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    equipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQKeep
     */
    objid?: any;
}