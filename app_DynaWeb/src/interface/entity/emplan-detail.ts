/**
 * 计划步骤
 *
 * @export
 * @interface EMPlanDetail
 */
export interface EMPlanDetail {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    createdate?: any;

    /**
     * 计划步骤名称
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    emplandetailname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    orgid?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    activelengths?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    enable?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    recvpersonname?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    orderflag?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    updateman?: any;

    /**
     * 计划步骤标识
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    emplandetailid?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    recvpersonid?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    archive?: any;

    /**
     * 生成工单种类
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    emwotype?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    content?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    eqstoplength?: any;

    /**
     * 计划步骤内容
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    plandetaildesc?: any;

    /**
     * 计划
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    planname?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    dptype?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    equipname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    dpname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    objname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    objid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    dpid?: any;

    /**
     * 计划
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    planid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlanDetail
     */
    equipid?: any;
}