/**
 * 模式
 *
 * @export
 * @interface EMRFOMO
 */
export interface EMRFOMO {

    /**
     * 模式名称
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    emrfomoname?: any;

    /**
     * 对象编号
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    objid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    createdate?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    rfomoinfo?: any;

    /**
     * 模式代码
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    rfomocode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    enable?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    description?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    orgid?: any;

    /**
     * 模式标识
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    emrfomoid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    updateman?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    rfodename?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOMO
     */
    rfodeid?: any;
}