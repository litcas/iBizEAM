/**
 * 服务商
 *
 * @export
 * @interface EMService
 */
export interface EMService {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMService
     */
    createdate?: any;

    /**
     * 帐号
     *
     * @returns {*}
     * @memberof EMService
     */
    accode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMService
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMService
     */
    enable?: any;

    /**
     * 所在地区
     *
     * @returns {*}
     * @memberof EMService
     */
    lsareaid?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof EMService
     */
    fax?: any;

    /**
     * 主营业务
     *
     * @returns {*}
     * @memberof EMService
     */
    zzyw?: any;

    /**
     * 付款方式
     *
     * @returns {*}
     * @memberof EMService
     */
    payway?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMService
     */
    wfstep?: any;

    /**
     * 服务商信息
     *
     * @returns {*}
     * @memberof EMService
     */
    serviceinfo?: any;

    /**
     * 上季度评估得分
     *
     * @returns {*}
     * @memberof EMService
     */
    pgrade?: any;

    /**
     * 帐号备注
     *
     * @returns {*}
     * @memberof EMService
     */
    accodedesc?: any;

    /**
     * 级别
     *
     * @returns {*}
     * @memberof EMService
     */
    labservicelevelid?: any;

    /**
     * 评估得分
     *
     * @returns {*}
     * @memberof EMService
     */
    sums?: any;

    /**
     * 服务商类型
     *
     * @returns {*}
     * @memberof EMService
     */
    labservicetypeid?: any;

    /**
     * 联系电话
     *
     * @returns {*}
     * @memberof EMService
     */
    tel?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMService
     */
    wfstate?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof EMService
     */
    prman?: any;

    /**
     * 合同内容
     *
     * @returns {*}
     * @memberof EMService
     */
    content?: any;

    /**
     * 服务商标识
     *
     * @returns {*}
     * @memberof EMService
     */
    emserviceid?: any;

    /**
     * 质量管理体系附件
     *
     * @returns {*}
     * @memberof EMService
     */
    qualitymana?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMService
     */
    createman?: any;

    /**
     * 税码
     *
     * @returns {*}
     * @memberof EMService
     */
    taxcode?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMService
     */
    wfinstanceid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMService
     */
    updateman?: any;

    /**
     * 邮编
     *
     * @returns {*}
     * @memberof EMService
     */
    zip?: any;

    /**
     * 服务商归属
     *
     * @returns {*}
     * @memberof EMService
     */
    range?: any;

    /**
     * 有效值备注
     *
     * @returns {*}
     * @memberof EMService
     */
    enablebz?: any;

    /**
     * 服务商名称
     *
     * @returns {*}
     * @memberof EMService
     */
    emservicename?: any;

    /**
     * 服务商代码
     *
     * @returns {*}
     * @memberof EMService
     */
    servicecode?: any;

    /**
     * 付款方式备注
     *
     * @returns {*}
     * @memberof EMService
     */
    paywaydesc?: any;

    /**
     * 服务商分组
     *
     * @returns {*}
     * @memberof EMService
     */
    servicegroup?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof EMService
     */
    att?: any;

    /**
     * 网址
     *
     * @returns {*}
     * @memberof EMService
     */
    website?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMService
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMService
     */
    description?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof EMService
     */
    logo?: any;

    /**
     * 联系地址
     *
     * @returns {*}
     * @memberof EMService
     */
    addr?: any;

    /**
     * 税类型
     *
     * @returns {*}
     * @memberof EMService
     */
    taxtypeid?: any;

    /**
     * 税备注
     *
     * @returns {*}
     * @memberof EMService
     */
    taxdesc?: any;

    /**
     * 审核时间
     *
     * @returns {*}
     * @memberof EMService
     */
    shdate?: any;

    /**
     * 资质附件
     *
     * @returns {*}
     * @memberof EMService
     */
    qualifications?: any;

    /**
     * 服务商状态
     *
     * @returns {*}
     * @memberof EMService
     */
    servicestate?: any;
}