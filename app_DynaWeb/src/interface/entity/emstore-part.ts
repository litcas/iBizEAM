/**
 * 仓库库位
 *
 * @export
 * @interface EMStorePart
 */
export interface EMStorePart {

    /**
     * 仓库库位标识
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    emstorepartid?: any;

    /**
     * 库位编号
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storepartid?: any;

    /**
     * 仓库库位名称
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    emstorepartname?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    enable?: any;

    /**
     * 仓库库位信息
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storepartinfo?: any;

    /**
     * 仓库库位代码
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storepartcode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    description?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    updateman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    orgid?: any;

    /**
     * 仓库库位名称
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storepartname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storename?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMStorePart
     */
    storeid?: any;
}