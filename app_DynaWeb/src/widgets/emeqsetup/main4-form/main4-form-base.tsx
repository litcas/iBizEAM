import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMEQSetupService from '@/service/emeqsetup/emeqsetup-service';
import Main4Service from './main4-form-service';
import EMEQSetupUIService from '@/uiservice/emeqsetup/emeqsetup-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main4EditFormBase}
 */
export class Main4EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main4Service}
     * @memberof Main4EditFormBase
     */
    public service: Main4Service = new Main4Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQSetupService}
     * @memberof Main4EditFormBase
     */
    public appEntityService: EMEQSetupService = new EMEQSetupService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeName: string = 'emeqsetup';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeLogicName: string = '更换安装';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQSetupUIService}
     * @memberof Main4Base
     */  
    public appUIService: EMEQSetupUIService = new EMEQSetupUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        eitiresid: null,
        equipname: null,
        objname: null,
        activedate: null,
        woname: null,
        activedesc: null,
        activeadesc: null,
        activebdesc: null,
        regionbegindate: null,
        regionenddate: null,
        activelengths: null,
        eqstoplength: null,
        rempid: null,
        rempname: null,
        rdeptname: null,
        rteamname: null,
        rservicename: null,
        prefee: null,
        mfee: null,
        pfee: null,
        sfee: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        content: null,
        pic5: null,
        pic4: null,
        pic3: null,
        pic1: null,
        pic2: null,
        pic: null,
        emeqsetupid: null,
        objid: null,
        rserviceid: null,
        woid: null,
        equipid: null,
        rteamid: null,
        emeqsetup: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public rules(): any{
        return {
            activedate: [
                {
                    required: this.detailsModel.activedate.required,
                    type: 'string',
                    message: `${this.$t('entities.emeqsetup.main4_form.details.activedate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.activedate.required,
                    type: 'string',
                    message: `${this.$t('entities.emeqsetup.main4_form.details.activedate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '安装记录信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqsetup.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel15: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel15', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqsetup.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel20: new FormGroupPanelModel({ caption: '财务信息', detailType: 'GROUPPANEL', name: 'grouppanel20', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqsetup.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel26: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel26', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqsetup.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage25: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage25', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        formpage33: new FormPageModel({ caption: '详细信息', detailType: 'FORMPAGE', name: 'formpage33', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        formpage35: new FormPageModel({ caption: '图片', detailType: 'FORMPAGE', name: 'formpage35', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '安装记录标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '安装记录名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eitiresid: new FormItemModel({
    caption: '出场编号', detailType: 'FORMITEM', name: 'eitiresid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activedate: new FormItemModel({
    caption: '更换安装日期', detailType: 'FORMITEM', name: 'activedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        woname: new FormItemModel({
    caption: '工单', detailType: 'FORMITEM', name: 'woname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activedesc: new FormItemModel({
    caption: '更换安装记录', detailType: 'FORMITEM', name: 'activedesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activeadesc: new FormItemModel({
    caption: '更换安装理由', detailType: 'FORMITEM', name: 'activeadesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activebdesc: new FormItemModel({
    caption: '更换安装结果', detailType: 'FORMITEM', name: 'activebdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '起始时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionenddate: new FormItemModel({
    caption: '结束时间', detailType: 'FORMITEM', name: 'regionenddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activelengths: new FormItemModel({
    caption: '持续时间(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqstoplength: new FormItemModel({
    caption: '停运时间(分)', detailType: 'FORMITEM', name: 'eqstoplength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamname: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rservicename: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        prefee: new FormItemModel({
    caption: '预算(￥)', detailType: 'FORMITEM', name: 'prefee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mfee: new FormItemModel({
    caption: '材料费(￥)', detailType: 'FORMITEM', name: 'mfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pfee: new FormItemModel({
    caption: '人工费(￥)', detailType: 'FORMITEM', name: 'pfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sfee: new FormItemModel({
    caption: '服务费(￥)', detailType: 'FORMITEM', name: 'sfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        content: new FormItemModel({
    caption: '详细内容', detailType: 'FORMITEM', name: 'content', visible: true, isShowCaption: false, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic5: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic5', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic4: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic4', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic3: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic3', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic1: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic1', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic2: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic2', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pic: new FormItemModel({
    caption: '图片', detailType: 'FORMITEM', name: 'pic', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqsetupid: new FormItemModel({
    caption: '安装记录标识', detailType: 'FORMITEM', name: 'emeqsetupid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rserviceid: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        woid: new FormItemModel({
    caption: '工单', detailType: 'FORMITEM', name: 'woid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamid: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage25',
                    index: 1,
                    visible: true,
                },
                {
                    name: 'formpage33',
                    index: 2,
                    visible: true,
                },
                {
                    name: 'formpage35',
                    index: 3,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main4Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}