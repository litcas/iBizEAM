import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import DefaultService from './default-searchform-service';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMItemPUseService = new EMItemPUseService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '领料单';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemPUseUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_itemname_like: null,
        n_equipname_like: null,
        n_objname_like: null,
        n_storeid_eq: null,
        n_storepartid_eq: null,
        n_woid_eq: null,
        n_pusestate_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_itemname_like: new FormItemModel({ caption: '领料物品(文本包含(%))', detailType: 'FORMITEM', name: 'n_itemname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_equipname_like: new FormItemModel({ caption: '领料设备(文本包含(%))', detailType: 'FORMITEM', name: 'n_equipname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_objname_like: new FormItemModel({ caption: '领料位置(文本包含(%))', detailType: 'FORMITEM', name: 'n_objname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_storeid_eq: new FormItemModel({ caption: '仓库(等于(=))', detailType: 'FORMITEM', name: 'n_storeid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_storepartid_eq: new FormItemModel({ caption: '库位(等于(=))', detailType: 'FORMITEM', name: 'n_storepartid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_woid_eq: new FormItemModel({ caption: '领料工单(等于(=))', detailType: 'FORMITEM', name: 'n_woid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_pusestate_eq: new FormItemModel({ caption: '领料状态(等于(=))', detailType: 'FORMITEM', name: 'n_pusestate_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}