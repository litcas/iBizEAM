import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import Main6Service from './main6-form-service';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main6EditFormBase}
 */
export class Main6EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main6EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main6Service}
     * @memberof Main6EditFormBase
     */
    public service: Main6Service = new Main6Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof Main6EditFormBase
     */
    public appEntityService: EMItemPUseService = new EMItemPUseService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main6EditFormBase
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main6EditFormBase
     */
    protected appDeLogicName: string = '领料单';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemPUseUIService}
     * @memberof Main6Base
     */  
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main6EditFormBase
     */
    protected formKeyItemName: string = 'emitempuseid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main6EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emitempuseid: null,
        pusetype: null,
        itemname: null,
        itemid: null,
        mservicename: null,
        labservicename: null,
        asum: null,
        purplanname: null,
        aempid: null,
        aempname: null,
        adate: null,
        useto: null,
        woname: null,
        equipname: null,
        objname: null,
        deptname: null,
        equips: null,
        teamname: null,
        pusestate: null,
        apprdesc: null,
        sapcbzx: null,
        sapllyt: null,
        remark: null,
        storename: null,
        storeid: null,
        storepartname: null,
        storepartid: null,
        psum: null,
        price: null,
        amount: null,
        batcode: null,
        sempid: null,
        sempname: null,
        sdate: null,
        empid: null,
        empname: null,
        apprempid: null,
        apprempname: null,
        apprdate: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        mserviceid: null,
        teamid: null,
        deptid: null,
        objid: null,
        woid: null,
        labserviceid: null,
        equipid: null,
        purplanid: null,
        emitempuse: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main6EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main6EditFormBase
     */
    public rules(): any{
        return {
            pusetype: [
                {
                    required: this.detailsModel.pusetype.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.pusetype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.pusetype.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.pusetype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            asum: [
                {
                    required: this.detailsModel.asum.required,
                    type: 'number',
                    message: `${this.$t('entities.emitempuse.main6_form.details.asum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.asum.required,
                    type: 'number',
                    message: `${this.$t('entities.emitempuse.main6_form.details.asum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            purplanname: [
                {
                    required: this.detailsModel.purplanname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.purplanname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.purplanname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.purplanname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            useto: [
                {
                    required: this.detailsModel.useto.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.useto')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.useto.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.useto')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            equipname: [
                {
                    required: this.detailsModel.equipname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.equipname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equipname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.equipname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            objname: [
                {
                    required: this.detailsModel.objname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.objname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.objname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.objname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            equips: [
                {
                    required: this.detailsModel.equips.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.equips')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equips.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.equips')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            sapcbzx: [
                {
                    required: this.detailsModel.sapcbzx.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.sapcbzx')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.sapcbzx.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.sapcbzx')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            sapllyt: [
                {
                    required: this.detailsModel.sapllyt.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.sapllyt')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.sapllyt.required,
                    type: 'string',
                    message: `${this.$t('entities.emitempuse.main6_form.details.sapllyt')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main6Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main6EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '领料单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitempuse.main6_form', extractMode: 'ITEM', details: [] } }),

        grouppanel24: new FormGroupPanelModel({ caption: '发料信息', detailType: 'GROUPPANEL', name: 'grouppanel24', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitempuse.main6_form', extractMode: 'ITEM', details: [] } }),

        grouppanel34: new FormGroupPanelModel({ caption: '审核信息', detailType: 'GROUPPANEL', name: 'grouppanel34', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitempuse.main6_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel38: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel38', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitempuse.main6_form', extractMode: 'ITEM', details: [] } }),

        formpage37: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage37', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '领料单号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '领料单信息', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emitempuseid: new FormItemModel({
    caption: '领料单号(自动)', detailType: 'FORMITEM', name: 'emitempuseid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        pusetype: new FormItemModel({
    caption: '领料分类', detailType: 'FORMITEM', name: 'pusetype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        itemname: new FormItemModel({
    caption: '领料物品', detailType: 'FORMITEM', name: 'itemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemid: new FormItemModel({
    caption: '领料物品', detailType: 'FORMITEM', name: 'itemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mservicename: new FormItemModel({
    caption: '制造商', detailType: 'FORMITEM', name: 'mservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labservicename: new FormItemModel({
    caption: '建议供应商', detailType: 'FORMITEM', name: 'labservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        asum: new FormItemModel({
    caption: '请领数', detailType: 'FORMITEM', name: 'asum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        purplanname: new FormItemModel({
    caption: '采购计划', detailType: 'FORMITEM', name: 'purplanname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        aempid: new FormItemModel({
    caption: '申请人', detailType: 'FORMITEM', name: 'aempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        aempname: new FormItemModel({
    caption: '申请人', detailType: 'FORMITEM', name: 'aempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        adate: new FormItemModel({
    caption: '申请日期', detailType: 'FORMITEM', name: 'adate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        useto: new FormItemModel({
    caption: '用途', detailType: 'FORMITEM', name: 'useto', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        woname: new FormItemModel({
    caption: '领料工单', detailType: 'FORMITEM', name: 'woname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '领料设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '领料位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        deptname: new FormItemModel({
    caption: '领料部门', detailType: 'FORMITEM', name: 'deptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equips: new FormItemModel({
    caption: '设备集合', detailType: 'FORMITEM', name: 'equips', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        teamname: new FormItemModel({
    caption: '领料班组', detailType: 'FORMITEM', name: 'teamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pusestate: new FormItemModel({
    caption: '领料状态', detailType: 'FORMITEM', name: 'pusestate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        apprdesc: new FormItemModel({
    caption: '审核意见', detailType: 'FORMITEM', name: 'apprdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sapcbzx: new FormItemModel({
    caption: 'sap成本中心', detailType: 'FORMITEM', name: 'sapcbzx', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        sapllyt: new FormItemModel({
    caption: 'sap领料用途', detailType: 'FORMITEM', name: 'sapllyt', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        remark: new FormItemModel({
    caption: '备注', detailType: 'FORMITEM', name: 'remark', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storename: new FormItemModel({
    caption: '仓库', detailType: 'FORMITEM', name: 'storename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storeid: new FormItemModel({
    caption: '仓库', detailType: 'FORMITEM', name: 'storeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storepartname: new FormItemModel({
    caption: '库位', detailType: 'FORMITEM', name: 'storepartname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storepartid: new FormItemModel({
    caption: '库位', detailType: 'FORMITEM', name: 'storepartid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        psum: new FormItemModel({
    caption: '实发数', detailType: 'FORMITEM', name: 'psum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        price: new FormItemModel({
    caption: '单价', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        amount: new FormItemModel({
    caption: '总金额', detailType: 'FORMITEM', name: 'amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        batcode: new FormItemModel({
    caption: '批次', detailType: 'FORMITEM', name: 'batcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sempid: new FormItemModel({
    caption: '发料人', detailType: 'FORMITEM', name: 'sempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sempname: new FormItemModel({
    caption: '发料人', detailType: 'FORMITEM', name: 'sempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sdate: new FormItemModel({
    caption: '发料日期', detailType: 'FORMITEM', name: 'sdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empid: new FormItemModel({
    caption: '领料人', detailType: 'FORMITEM', name: 'empid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empname: new FormItemModel({
    caption: '领料人', detailType: 'FORMITEM', name: 'empname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        apprempid: new FormItemModel({
    caption: '批准人', detailType: 'FORMITEM', name: 'apprempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        apprempname: new FormItemModel({
    caption: '批准人', detailType: 'FORMITEM', name: 'apprempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        apprdate: new FormItemModel({
    caption: '批准日期', detailType: 'FORMITEM', name: 'apprdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        mserviceid: new FormItemModel({
    caption: '制造商', detailType: 'FORMITEM', name: 'mserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        teamid: new FormItemModel({
    caption: '领料班组', detailType: 'FORMITEM', name: 'teamid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        deptid: new FormItemModel({
    caption: '领料部门', detailType: 'FORMITEM', name: 'deptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '领料位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        woid: new FormItemModel({
    caption: '领料工单', detailType: 'FORMITEM', name: 'woid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labserviceid: new FormItemModel({
    caption: '建议供应商', detailType: 'FORMITEM', name: 'labserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '领料设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        purplanid: new FormItemModel({
    caption: '采购计划', detailType: 'FORMITEM', name: 'purplanid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage37',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof Main6EditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): Promise<void> {
                





















        if (Object.is(name, '') || Object.is(name, 'pusetype')) {
            let ret = true;
            const _pusetype = this.data.pusetype;
            if (this.$verify.testCond(_pusetype, 'NOTEQ', '99')) {
                ret = false;
            }
            this.detailsModel.purplanname.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'pusetype')) {
            let ret = false;
            const _pusetype = this.data.pusetype;
            if (this.$verify.testCond(_pusetype, 'EQ', '99')) {
                ret = true;
            }
            this.detailsModel.purplanname.setDisabled(!ret);
        }






        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'EQUIP')) {
                ret = false;
            }
            this.detailsModel.equipname.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQUIP')) {
                ret = true;
            }
            this.detailsModel.equipname.setDisabled(!ret);
        }

        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'EQLOCATION') && this.$verify.testCond(_useto, 'NOTEQ', 'EQTYPE')) {
                ret = false;
            }
            this.detailsModel.objname.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQLOCATION') || this.$verify.testCond(_useto, 'EQ', 'EQTYPE')) {
                ret = true;
            }
            this.detailsModel.objname.setDisabled(!ret);
        }


        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'MS')) {
                ret = false;
            }
            this.detailsModel.equips.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'MS')) {
                ret = true;
            }
            this.detailsModel.equips.setDisabled(!ret);
        }





































        if (Object.is(name, 'itemname')) {
            const details: string[] = ['storepartid', 'itemid', 'storepartname', 'price', 'storename', 'sempname', 'sempid', 'batcode', 'itemname', 'storeid', 'empname'];
            this.updateFormItems('FormUpdateByITEMID', this.data, details, true);
        }
    }

    /**
     * 新建默认值
     * @memberof Main6EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('adate')) {
            this.data['adate'] = this.$util.dateFormat(new Date());
        }
        if (this.data.hasOwnProperty('teamname')) {
            this.data['teamname'] = this.context['TEAMID'];
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main6Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}