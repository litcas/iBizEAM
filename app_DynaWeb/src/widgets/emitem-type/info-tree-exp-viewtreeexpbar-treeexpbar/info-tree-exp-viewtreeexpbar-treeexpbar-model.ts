/**
 * InfoTreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class InfoTreeExpViewtreeexpbarModel
 */
export default class InfoTreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof InfoTreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emitemtype',
        prop: 'emitemtypeid',
      },
      {
        name: 'description',
      },
      {
        name: 'itemtypecode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'emitemtypename',
      },
      {
        name: 'updateman',
      },
      {
        name: 'itemtypeinfo',
      },
      {
        name: 'createdate',
      },
      {
        name: 'itemtypepcode',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypepname',
      },
      {
        name: 'itemtypepid',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itembtypeid',
      },
    ]
  }


}