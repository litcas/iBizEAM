/**
 * ItemTreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class ItemTreeExpViewtreeexpbarModel
 */
export default class ItemTreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ItemTreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emitemtype',
        prop: 'emitemtypeid',
      },
      {
        name: 'description',
      },
      {
        name: 'itemtypecode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'emitemtypename',
      },
      {
        name: 'updateman',
      },
      {
        name: 'itemtypeinfo',
      },
      {
        name: 'createdate',
      },
      {
        name: 'itemtypepcode',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypepname',
      },
      {
        name: 'itemtypepid',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itembtypeid',
      },
    ]
  }


}