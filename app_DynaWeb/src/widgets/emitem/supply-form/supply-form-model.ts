/**
 * Supply 部件模型
 *
 * @export
 * @class SupplyModel
 */
export default class SupplyModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof SupplyModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mservicename',
        prop: 'mservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'warrantyday',
        prop: 'warrantyday',
        dataType: 'FLOAT',
      },
      {
        name: 'no3q',
        prop: 'no3q',
        dataType: 'YESNO',
      },
      {
        name: 'life',
        prop: 'life',
        dataType: 'INT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'PICKUP',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}