/**
 * TabExpViewtabviewpanel4 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel4Model
 */
export default class TabExpViewtabviewpanel4Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel4Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'sapreason1',
      },
      {
        name: 'emitemroutname',
      },
      {
        name: 'batcode',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'price',
      },
      {
        name: 'emitemrout',
        prop: 'emitemroutid',
      },
      {
        name: 'description',
      },
      {
        name: 'amount',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'sap',
      },
      {
        name: 'enable',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'sdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'itemroutinfo',
      },
      {
        name: 'orgid',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'shf',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'sapreason',
      },
      {
        name: 'psum',
      },
      {
        name: 'teamid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'civo',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'storename',
      },
      {
        name: 'rname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'storeid',
      },
      {
        name: 'rid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sempname',
      },
    ]
  }


}