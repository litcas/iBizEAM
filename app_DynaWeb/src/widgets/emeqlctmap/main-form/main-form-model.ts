/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqlctmapid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqlctmapname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eqlocationname',
        prop: 'eqlocationname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqlocationpname',
        prop: 'eqlocationpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqlocationid',
        prop: 'eqlocationid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqlctmapid',
        prop: 'emeqlctmapid',
        dataType: 'GUID',
      },
      {
        name: 'eqlocationpid',
        prop: 'eqlocationpid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqlctmap',
        prop: 'emeqlctmapid',
        dataType: 'FONTKEY',
      },
    ]
  }

}