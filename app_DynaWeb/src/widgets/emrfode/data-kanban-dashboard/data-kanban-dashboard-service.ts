import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DataKanban 部件服务对象
 *
 * @export
 * @class DataKanbanService
 */
export default class DataKanbanService extends ControlService {
}