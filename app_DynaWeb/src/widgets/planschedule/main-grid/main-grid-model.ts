/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emplanname',
          prop: 'emplanname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'scheduletype',
          prop: 'scheduletype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rundate',
          prop: 'rundate',
          dataType: 'DATE',
        },
        {
          name: 'scheduleparam',
          prop: 'scheduleparam',
          dataType: 'TEXT',
        },
        {
          name: 'scheduleparam2',
          prop: 'scheduleparam2',
          dataType: 'TEXT',
        },
        {
          name: 'schedulestate',
          prop: 'schedulestate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'emplanid',
          prop: 'emplanid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'planschedulename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdatatype',
          prop: 'scheduletype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfdataaccaction',
          prop: 'planscheduleid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'planscheduleid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'planschedule',
          prop: 'planscheduleid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}