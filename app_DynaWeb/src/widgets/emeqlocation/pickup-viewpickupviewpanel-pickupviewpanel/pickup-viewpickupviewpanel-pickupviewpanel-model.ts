/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'orgid',
      },
      {
        name: 'createman',
      },
      {
        name: 'eqlocationinfo',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqlocationname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'emeqlocation',
        prop: 'emeqlocationid',
      },
      {
        name: 'eqlocationcode',
      },
      {
        name: 'eqlocationtype',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'majorequipname',
      },
      {
        name: 'majorequipid',
      },
    ]
  }


}