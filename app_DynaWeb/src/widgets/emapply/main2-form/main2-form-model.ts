/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emapplyid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emapplyname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emapplyid',
        prop: 'emapplyid',
        dataType: 'GUID',
      },
      {
        name: 'applystate',
        prop: 'applystate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'emapplyname',
        prop: 'emapplyname',
        dataType: 'TEXT',
      },
      {
        name: 'applytype',
        prop: 'applytype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'SSCODELIST',
      },
      {
        name: 'entrustlist',
        prop: 'entrustlist',
        dataType: 'SSCODELIST',
      },
      {
        name: 'plantype',
        prop: 'plantype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'applybdate',
        prop: 'applybdate',
        dataType: 'DATETIME',
      },
      {
        name: 'applyedate',
        prop: 'applyedate',
        dataType: 'DATETIME',
      },
      {
        name: 'applydesc',
        prop: 'applydesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'FLOAT',
      },
      {
        name: 'shuifei',
        prop: 'shuifei',
        dataType: 'FLOAT',
      },
      {
        name: 'zfy',
        prop: 'zfy',
        dataType: 'FLOAT',
      },
      {
        name: 'prefee1',
        prop: 'prefee1',
        dataType: 'FLOAT',
      },
      {
        name: 'pfee',
        prop: 'pfee',
        dataType: 'FLOAT',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'sfee',
        prop: 'sfee',
        dataType: 'FLOAT',
      },
      {
        name: 'fp',
        prop: 'fp',
        dataType: 'TEXT',
      },
      {
        name: 'dpdesc',
        prop: 'dpdesc',
        dataType: 'TEXT',
      },
      {
        name: 'spyj',
        prop: 'spyj',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'invoiceattach',
        prop: 'invoiceattach',
        dataType: 'LONGTEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'applydate',
        prop: 'applydate',
        dataType: 'DATETIME',
      },
      {
        name: 'closeempname',
        prop: 'closeempname',
        dataType: 'TEXT',
      },
      {
        name: 'closeempid',
        prop: 'closeempid',
        dataType: 'TEXT',
      },
      {
        name: 'closedate',
        prop: 'closedate',
        dataType: 'DATETIME',
      },
      {
        name: 'rfodename',
        prop: 'rfodename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfomoname',
        prop: 'rfomoname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfocaname',
        prop: 'rfocaname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfoacname',
        prop: 'rfoacname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emapply',
        prop: 'emapplyid',
        dataType: 'FONTKEY',
      },
    ]
  }

}