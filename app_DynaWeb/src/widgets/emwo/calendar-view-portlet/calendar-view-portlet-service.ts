import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CalendarView 部件服务对象
 *
 * @export
 * @class CalendarViewService
 */
export default class CalendarViewService extends ControlService {
}
