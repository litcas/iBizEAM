import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWOService from '@/service/emwo/emwo-service';
import EqWoTrendModel from './eq-wo-trend-chart-model';


/**
 * EqWoTrend 部件服务对象
 *
 * @export
 * @class EqWoTrendService
 */
export default class EqWoTrendService extends ControlService {

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof EqWoTrendService
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EqWoTrendService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EqWoTrendService.
     * 
     * @param {*} [opts={}]
     * @memberof EqWoTrendService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EqWoTrendModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EqWoTrendService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}