import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMDRWGService from '@/service/emdrwg/emdrwg-service';
import NewService from './new-form-service';
import EMDRWGUIService from '@/uiservice/emdrwg/emdrwg-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {NewEditFormBase}
 */
export class NewEditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof NewEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {NewService}
     * @memberof NewEditFormBase
     */
    public service: NewService = new NewService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMDRWGService}
     * @memberof NewEditFormBase
     */
    public appEntityService: EMDRWGService = new EMDRWGService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof NewEditFormBase
     */
    protected appDeName: string = 'emdrwg';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof NewEditFormBase
     */
    protected appDeLogicName: string = '文档';

    /**
     * 界面UI服务对象
     *
     * @type {EMDRWGUIService}
     * @memberof NewBase
     */  
    public appUIService: EMDRWGUIService = new EMDRWGUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof NewEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        drwgcode: null,
        emdrwgname: null,
        drwgtype: null,
        drwgstate: null,
        rempid: null,
        rempname: null,
        bpersonname: null,
        bpersonid: null,
        lct: null,
        emdrwgid: null,
        emdrwg: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof NewEditFormBase
     */
    public majorMessageField: string = 'emdrwgname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof NewEditFormBase
     */
    public rules(): any{
        return {
            drwgcode: [
                {
                    required: this.detailsModel.drwgcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emdrwg.new_form.details.drwgcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.drwgcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emdrwg.new_form.details.drwgcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            drwgstate: [
                {
                    required: this.detailsModel.drwgstate.required,
                    type: 'string',
                    message: `${this.$t('entities.emdrwg.new_form.details.drwgstate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.drwgstate.required,
                    type: 'string',
                    message: `${this.$t('entities.emdrwg.new_form.details.drwgstate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof NewBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof NewEditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '文档信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emdrwg.new_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '文档标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '文档名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        drwgcode: new FormItemModel({
    caption: '文档代码', detailType: 'FORMITEM', name: 'drwgcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        emdrwgname: new FormItemModel({
    caption: '文档名称', detailType: 'FORMITEM', name: 'emdrwgname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        drwgtype: new FormItemModel({
    caption: '文档类型', detailType: 'FORMITEM', name: 'drwgtype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        drwgstate: new FormItemModel({
    caption: '文档状态', detailType: 'FORMITEM', name: 'drwgstate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '保管人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '保管人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        bpersonname: new FormItemModel({
    caption: '最后借阅人', detailType: 'FORMITEM', name: 'bpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        bpersonid: new FormItemModel({
    caption: '最后借阅人', detailType: 'FORMITEM', name: 'bpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lct: new FormItemModel({
    caption: '存放位置', detailType: 'FORMITEM', name: 'lct', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emdrwgid: new FormItemModel({
    caption: '文档标识', detailType: 'FORMITEM', name: 'emdrwgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof NewBase
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}