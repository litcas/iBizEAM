/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'mpcode',
      },
      {
        name: 'emeqmpname',
      },
      {
        name: 'mpdesc',
      },
      {
        name: 'createman',
      },
      {
        name: 'mpinfo',
      },
      {
        name: 'emeqmp',
        prop: 'emeqmpid',
      },
      {
        name: 'normalrefval',
      },
      {
        name: 'mptypeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'mpscope',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'enable',
      },
      {
        name: 'objname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'objid',
      },
      {
        name: 'equipid',
      },
    ]
  }


}