/**
 * EqEnByYearLine 部件模型
 *
 * @export
 * @class EqEnByYearLineModel
 */
export default class EqEnByYearLineModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EqEnByYearLineDashboard_sysportlet8_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}