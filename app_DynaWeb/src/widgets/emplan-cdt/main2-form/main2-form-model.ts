/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emplancdtid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emplancdtname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'dpname',
        prop: 'dpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'dptype',
        prop: 'dptype',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'dpvaltype',
        prop: 'dpvaltype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'triggerdp',
        prop: 'triggerdp',
        dataType: 'SSCODELIST',
      },
      {
        name: 'triggerval',
        prop: 'triggerval',
        dataType: 'TEXT',
      },
      {
        name: 'nowval',
        prop: 'nowval',
        dataType: 'TEXT',
      },
      {
        name: 'lastval',
        prop: 'lastval',
        dataType: 'TEXT',
      },
      {
        name: 'planname',
        prop: 'planname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'planid',
        prop: 'planid',
        dataType: 'PICKUP',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'emplancdtid',
        prop: 'emplancdtid',
        dataType: 'GUID',
      },
      {
        name: 'dpid',
        prop: 'dpid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'emplancdt',
        prop: 'emplancdtid',
        dataType: 'FONTKEY',
      },
    ]
  }

}