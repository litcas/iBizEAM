/**
 * AppPortalView_db 部件模型
 *
 * @export
 * @class AppPortalView_dbModel
 */
export default class AppPortalView_dbModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof AppPortalView_dbModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}