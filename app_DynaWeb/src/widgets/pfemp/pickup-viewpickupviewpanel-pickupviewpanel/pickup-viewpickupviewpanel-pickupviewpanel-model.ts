/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'description',
      },
      {
        name: 'tel',
      },
      {
        name: 'workdate',
      },
      {
        name: 'pfempname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'raisedate',
      },
      {
        name: 'hometel',
      },
      {
        name: 'birthday',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'pfemp',
        prop: 'pfempid',
      },
      {
        name: 'homeaddr',
      },
      {
        name: 'empinfo',
      },
      {
        name: 'deptid',
      },
      {
        name: 'createman',
      },
      {
        name: 'empsex',
      },
      {
        name: 'certcode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'empcode',
      },
      {
        name: 'psw',
      },
      {
        name: 'teamid',
      },
      {
        name: 'maindeptcode',
      },
      {
        name: 'e_mail',
      },
      {
        name: 'enable',
      },
      {
        name: 'cell',
      },
      {
        name: 'addr',
      },
      {
        name: 'majorteamname',
      },
      {
        name: 'majorteamid',
      },
      {
        name: 'majordeptid',
      },
      {
        name: 'majordeptname',
      },
    ]
  }


}