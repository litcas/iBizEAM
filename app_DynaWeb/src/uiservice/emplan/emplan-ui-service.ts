import EMPlanUIServiceBase from './emplan-ui-service-base';

/**
 * 计划UI服务对象
 *
 * @export
 * @class EMPlanUIService
 */
export default class EMPlanUIService extends EMPlanUIServiceBase {

    /**
     * Creates an instance of  EMPlanUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}