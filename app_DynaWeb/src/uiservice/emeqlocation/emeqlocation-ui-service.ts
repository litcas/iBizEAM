import EMEQLocationUIServiceBase from './emeqlocation-ui-service-base';

/**
 * 位置UI服务对象
 *
 * @export
 * @class EMEQLocationUIService
 */
export default class EMEQLocationUIService extends EMEQLocationUIServiceBase {

    /**
     * Creates an instance of  EMEQLocationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLocationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}