import EMEQKPMapUIServiceBase from './emeqkpmap-ui-service-base';

/**
 * 设备关键点关系UI服务对象
 *
 * @export
 * @class EMEQKPMapUIService
 */
export default class EMEQKPMapUIService extends EMEQKPMapUIServiceBase {

    /**
     * Creates an instance of  EMEQKPMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}