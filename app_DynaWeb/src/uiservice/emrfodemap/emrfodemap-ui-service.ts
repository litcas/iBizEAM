import EMRFODEMapUIServiceBase from './emrfodemap-ui-service-base';

/**
 * 现象引用UI服务对象
 *
 * @export
 * @class EMRFODEMapUIService
 */
export default class EMRFODEMapUIService extends EMRFODEMapUIServiceBase {

    /**
     * Creates an instance of  EMRFODEMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}