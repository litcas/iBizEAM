import EMWPListCostUIServiceBase from './emwplist-cost-ui-service-base';

/**
 * 询价单UI服务对象
 *
 * @export
 * @class EMWPListCostUIService
 */
export default class EMWPListCostUIService extends EMWPListCostUIServiceBase {

    /**
     * Creates an instance of  EMWPListCostUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListCostUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}