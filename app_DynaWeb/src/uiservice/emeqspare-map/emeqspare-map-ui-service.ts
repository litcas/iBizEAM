import EMEQSpareMapUIServiceBase from './emeqspare-map-ui-service-base';

/**
 * 备件包引用UI服务对象
 *
 * @export
 * @class EMEQSpareMapUIService
 */
export default class EMEQSpareMapUIService extends EMEQSpareMapUIServiceBase {

    /**
     * Creates an instance of  EMEQSpareMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}