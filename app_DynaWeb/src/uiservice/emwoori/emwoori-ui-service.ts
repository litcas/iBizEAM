import EMWOORIUIServiceBase from './emwoori-ui-service-base';

/**
 * 工单来源UI服务对象
 *
 * @export
 * @class EMWOORIUIService
 */
export default class EMWOORIUIService extends EMWOORIUIServiceBase {

    /**
     * Creates an instance of  EMWOORIUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOORIUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}