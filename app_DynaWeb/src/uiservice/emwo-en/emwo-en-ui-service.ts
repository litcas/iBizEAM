import EMWO_ENUIServiceBase from './emwo-en-ui-service-base';

/**
 * 能耗登记工单UI服务对象
 *
 * @export
 * @class EMWO_ENUIService
 */
export default class EMWO_ENUIService extends EMWO_ENUIServiceBase {

    /**
     * Creates an instance of  EMWO_ENUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_ENUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}