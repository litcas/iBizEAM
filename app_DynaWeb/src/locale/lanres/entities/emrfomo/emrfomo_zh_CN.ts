import EMRFOMO_zh_CN_Base from './emrfomo_zh_CN_base';

function getLocaleResource(){
    const EMRFOMO_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFOMO_zh_CN_Base(), EMRFOMO_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;