import PFEmp_en_US_Base from './pfemp_en_US_base';

function getLocaleResource(){
    const PFEmp_en_US_OwnData = {};
    const targetData = Object.assign(PFEmp_en_US_Base(), PFEmp_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
