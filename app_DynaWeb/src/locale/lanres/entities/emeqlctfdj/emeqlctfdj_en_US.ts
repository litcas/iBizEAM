import EMEQLCTFDJ_en_US_Base from './emeqlctfdj_en_US_base';

function getLocaleResource(){
    const EMEQLCTFDJ_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLCTFDJ_en_US_Base(), EMEQLCTFDJ_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
