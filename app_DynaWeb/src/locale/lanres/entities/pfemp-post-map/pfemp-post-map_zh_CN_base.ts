import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("人事关系", null),
		fields: {
			empname: commonLogic.appcommonhandle("职员",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			pfemppostmapid: commonLogic.appcommonhandle("人事关系标识",null),
			deptid: commonLogic.appcommonhandle("部门",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			description: commonLogic.appcommonhandle("描述",null),
			deptname: commonLogic.appcommonhandle("部门",null),
			empid: commonLogic.appcommonhandle("职员",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			orderflag: commonLogic.appcommonhandle("排序值",null),
			pfemppostmapname: commonLogic.appcommonhandle("人事关系名称",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			teamname: commonLogic.appcommonhandle("班组",null),
			postname: commonLogic.appcommonhandle("岗位",null),
			teamid: commonLogic.appcommonhandle("班组",null),
			postid: commonLogic.appcommonhandle("岗位",null),
		},
		};
		return data;
}
export default getLocaleResourceBase;