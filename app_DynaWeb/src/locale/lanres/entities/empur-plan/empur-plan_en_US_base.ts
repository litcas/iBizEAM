import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			yearfrom: commonLogic.appcommonhandle("采购起始时间",null),
			assessreport: commonLogic.appcommonhandle("评估报告",null),
			empurplanname: commonLogic.appcommonhandle("采购计划项目",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			years: commonLogic.appcommonhandle("采购年度",null),
			msitemtype: commonLogic.appcommonhandle("物品类型",null),
			empurplanid: commonLogic.appcommonhandle("采购计划标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			planstate: commonLogic.appcommonhandle("计划修理状态",null),
			acceptancedate: commonLogic.appcommonhandle("验收时间",null),
			cocnt: commonLogic.appcommonhandle("数量完成比",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			yearto: commonLogic.appcommonhandle("采购结束时间",null),
			nowamount: commonLogic.appcommonhandle("剩余金额额度",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			servicecode: commonLogic.appcommonhandle("服务商编号",null),
			acceptanceresult: commonLogic.appcommonhandle("验收结果",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			puramount: commonLogic.appcommonhandle("采购金额",null),
			trackrule: commonLogic.appcommonhandle("跟踪规则",null),
			m3q: commonLogic.appcommonhandle("经理指定询价数",null),
			description: commonLogic.appcommonhandle("描述",null),
			nowcnt: commonLogic.appcommonhandle("剩余数量额度",null),
			coamount: commonLogic.appcommonhandle("金额完成比",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			rempid: commonLogic.appcommonhandle("负责人",null),
			rempname: commonLogic.appcommonhandle("负责人",null),
			istrackok: commonLogic.appcommonhandle("是否跟踪结束",null),
			contractscan: commonLogic.appcommonhandle("合同扫描件",null),
			pursum: commonLogic.appcommonhandle("采购数量",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			itemtypename: commonLogic.appcommonhandle("物品类型",null),
			embidinquiryname: commonLogic.appcommonhandle("计划修理",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			embidinquiryid: commonLogic.appcommonhandle("计划修理",null),
			unitid: commonLogic.appcommonhandle("单位",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("计划修理",null),
					title: commonLogic.appcommonhandle("计划修理数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("计划修理",null),
					title: commonLogic.appcommonhandle("计划修理选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					empurplanname: commonLogic.appcommonhandle("采购计划项目",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;