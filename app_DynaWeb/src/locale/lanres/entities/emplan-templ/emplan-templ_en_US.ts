import EMPlanTempl_en_US_Base from './emplan-templ_en_US_base';

function getLocaleResource(){
    const EMPlanTempl_en_US_OwnData = {};
    const targetData = Object.assign(EMPlanTempl_en_US_Base(), EMPlanTempl_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
