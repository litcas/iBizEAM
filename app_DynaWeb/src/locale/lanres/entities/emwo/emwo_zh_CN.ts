import EMWO_zh_CN_Base from './emwo_zh_CN_base';

function getLocaleResource(){
    const EMWO_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWO_zh_CN_Base(), EMWO_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;