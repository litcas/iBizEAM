import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			arg2: commonLogic.appcommonhandle("ARG2",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			storepartgl: commonLogic.appcommonhandle("货架管理",null),
			arg1: commonLogic.appcommonhandle("ARG1",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			arg0: commonLogic.appcommonhandle("ARG0",null),
			emcabid: commonLogic.appcommonhandle("货架标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			emcabname: commonLogic.appcommonhandle("货架名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			graphparam: commonLogic.appcommonhandle("GRAPHPARAM",null),
			emstorepartname: commonLogic.appcommonhandle("库位",null),
			emstorename: commonLogic.appcommonhandle("仓库",null),
			emstorepartid: commonLogic.appcommonhandle("库位",null),
			emstoreid: commonLogic.appcommonhandle("仓库",null),
		},
		};
		return data;
}

export default getLocaleResourceBase;