import EMEQLCTMap_zh_CN_Base from './emeqlctmap_zh_CN_base';

function getLocaleResource(){
    const EMEQLCTMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQLCTMap_zh_CN_Base(), EMEQLCTMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;