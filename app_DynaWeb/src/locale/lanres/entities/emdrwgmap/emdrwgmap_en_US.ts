import EMDRWGMap_en_US_Base from './emdrwgmap_en_US_base';

function getLocaleResource(){
    const EMDRWGMap_en_US_OwnData = {};
    const targetData = Object.assign(EMDRWGMap_en_US_Base(), EMDRWGMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
