import EMPLANRECORD_zh_CN_Base from './emplanrecord_zh_CN_base';

function getLocaleResource(){
    const EMPLANRECORD_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPLANRECORD_zh_CN_Base(), EMPLANRECORD_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;