import PFContract_en_US_Base from './pfcontract_en_US_base';

function getLocaleResource(){
    const PFContract_en_US_OwnData = {};
    const targetData = Object.assign(PFContract_en_US_Base(), PFContract_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
