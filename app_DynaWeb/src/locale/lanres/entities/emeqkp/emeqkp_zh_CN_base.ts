import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备关键点", null),
		fields: {
			emeqkpid: commonLogic.appcommonhandle("设备关键点标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			kpscope: commonLogic.appcommonhandle("关键点范围",null),
			kpdesc: commonLogic.appcommonhandle("关键点备注",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			normalrefval: commonLogic.appcommonhandle("正常参考值",null),
			kpinfo: commonLogic.appcommonhandle("关键点信息",null),
			kptypeid: commonLogic.appcommonhandle("关键点类型",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			kpcode: commonLogic.appcommonhandle("关键点代码",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			emeqkpname: commonLogic.appcommonhandle("关键点名称",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("设备关键点",null),
					title: commonLogic.appcommonhandle("设备关键点表格视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("设备关键点",null),
					title: commonLogic.appcommonhandle("设备关键点选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("设备关键点",null),
					title: commonLogic.appcommonhandle("设备关键点数据选择视图",null),
				},
				kpeditview: {
					caption: commonLogic.appcommonhandle("设备关键点",null),
					title: commonLogic.appcommonhandle("设备关键点编辑视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("设备关键点信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备关键点标识",null), 
					srfmajortext: commonLogic.appcommonhandle("关键点名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					kpcode: commonLogic.appcommonhandle("关键点代码",null), 
					emeqkpname: commonLogic.appcommonhandle("关键点名称",null), 
					kptypeid: commonLogic.appcommonhandle("关键点类型",null), 
					normalrefval: commonLogic.appcommonhandle("正常参考值",null), 
					kpscope: commonLogic.appcommonhandle("关键点范围",null), 
					kpdesc: commonLogic.appcommonhandle("关键点备注",null), 
					emeqkpid: commonLogic.appcommonhandle("设备关键点标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					kpcode: commonLogic.appcommonhandle("关键点代码",null),
					emeqkpname: commonLogic.appcommonhandle("关键点名称",null),
					kptypeid: commonLogic.appcommonhandle("关键点类型",null),
					kpscope: commonLogic.appcommonhandle("关键点范围",null),
					normalrefval: commonLogic.appcommonhandle("正常参考值",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emeqkpname_like: commonLogic.appcommonhandle("关键点名称(文本包含(%))",null), 
					n_kptypeid_eq: commonLogic.appcommonhandle("关键点类型(等于(=))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			kpeditviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;