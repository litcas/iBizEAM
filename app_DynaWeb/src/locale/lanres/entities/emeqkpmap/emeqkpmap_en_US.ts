import EMEQKPMap_en_US_Base from './emeqkpmap_en_US_base';

function getLocaleResource(){
    const EMEQKPMap_en_US_OwnData = {};
    const targetData = Object.assign(EMEQKPMap_en_US_Base(), EMEQKPMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
