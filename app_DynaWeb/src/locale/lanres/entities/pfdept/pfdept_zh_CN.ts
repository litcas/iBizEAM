import PFDept_zh_CN_Base from './pfdept_zh_CN_base';

function getLocaleResource(){
    const PFDept_zh_CN_OwnData = {};
    const targetData = Object.assign(PFDept_zh_CN_Base(), PFDept_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;