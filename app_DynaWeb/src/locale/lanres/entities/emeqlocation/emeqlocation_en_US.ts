import EMEQLocation_en_US_Base from './emeqlocation_en_US_base';

function getLocaleResource(){
    const EMEQLocation_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLocation_en_US_Base(), EMEQLocation_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
