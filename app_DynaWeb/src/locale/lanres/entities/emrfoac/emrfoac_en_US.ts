import EMRFOAC_en_US_Base from './emrfoac_en_US_base';

function getLocaleResource(){
    const EMRFOAC_en_US_OwnData = {};
    const targetData = Object.assign(EMRFOAC_en_US_Base(), EMRFOAC_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
