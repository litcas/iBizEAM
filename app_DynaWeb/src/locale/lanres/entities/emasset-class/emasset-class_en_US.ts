import EMAssetClass_en_US_Base from './emasset-class_en_US_base';

function getLocaleResource(){
    const EMAssetClass_en_US_OwnData = {};
    const targetData = Object.assign(EMAssetClass_en_US_Base(), EMAssetClass_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
