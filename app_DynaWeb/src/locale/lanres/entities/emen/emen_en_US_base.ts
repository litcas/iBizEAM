import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			createdate: commonLogic.appcommonhandle("建立时间",null),
			energydesc: commonLogic.appcommonhandle("能源备注",null),
			emenid: commonLogic.appcommonhandle("能源标识",null),
			description: commonLogic.appcommonhandle("描述",null),
			energycode: commonLogic.appcommonhandle("能源代码",null),
			energytypeid: commonLogic.appcommonhandle("能源类型",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			vrate: commonLogic.appcommonhandle("倍率",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			price: commonLogic.appcommonhandle("能源单价",null),
			emenname: commonLogic.appcommonhandle("能源名称",null),
			energyinfo: commonLogic.appcommonhandle("能源信息",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			itemmtypeid: commonLogic.appcommonhandle("物品二级类",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			itemprice: commonLogic.appcommonhandle("物品库存单价",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			itembtypeid: commonLogic.appcommonhandle("物品大类",null),
			itembtypename: commonLogic.appcommonhandle("物品大类",null),
			itemid: commonLogic.appcommonhandle("物品",null),
		},
			views: {
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("能源信息",null),
					title: commonLogic.appcommonhandle("能源信息",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源选择表格视图",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("能源信息",null),
					title: commonLogic.appcommonhandle("能源信息",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源编辑视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能源信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energycode: commonLogic.appcommonhandle("能源代码",null), 
					emenname: commonLogic.appcommonhandle("能源名称",null), 
					energytypeid: commonLogic.appcommonhandle("能源类型",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					price: commonLogic.appcommonhandle("能源单价",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					energydesc: commonLogic.appcommonhandle("能源备注",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能源信息",null), 
					grouppanel10: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energycode: commonLogic.appcommonhandle("能源代码",null), 
					emenname: commonLogic.appcommonhandle("能源名称",null), 
					energytypeid: commonLogic.appcommonhandle("能源类型",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					price: commonLogic.appcommonhandle("能源单价",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					energydesc: commonLogic.appcommonhandle("能源备注",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("能源基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energyinfo: commonLogic.appcommonhandle("能源信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					energyinfo: commonLogic.appcommonhandle("能源信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					energycode: commonLogic.appcommonhandle("能源代码",null),
					emenname: commonLogic.appcommonhandle("能源名称",null),
					energytypeid: commonLogic.appcommonhandle("能源类型",null),
					vrate: commonLogic.appcommonhandle("倍率",null),
					energydesc: commonLogic.appcommonhandle("能源备注",null),
					price: commonLogic.appcommonhandle("能源单价",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emenname_like: commonLogic.appcommonhandle("能源名称(文本包含(%))",null), 
					n_energytypeid_eq: commonLogic.appcommonhandle("能源类型(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;