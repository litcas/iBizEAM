import EMResItem_en_US_Base from './emres-item_en_US_base';

function getLocaleResource(){
    const EMResItem_en_US_OwnData = {};
    const targetData = Object.assign(EMResItem_en_US_Base(), EMResItem_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
