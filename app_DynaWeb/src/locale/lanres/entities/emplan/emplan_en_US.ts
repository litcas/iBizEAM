import EMPlan_en_US_Base from './emplan_en_US_base';

function getLocaleResource(){
    const EMPlan_en_US_OwnData = {};
    const targetData = Object.assign(EMPlan_en_US_Base(), EMPlan_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
