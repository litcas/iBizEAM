import EMMachineCategory_en_US_Base from './emmachine-category_en_US_base';

function getLocaleResource(){
    const EMMachineCategory_en_US_OwnData = {};
    const targetData = Object.assign(EMMachineCategory_en_US_Base(), EMMachineCategory_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
