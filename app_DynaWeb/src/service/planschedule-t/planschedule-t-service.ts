import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULE_TServiceBase from './planschedule-t-service-base';


/**
 * 计划_定时服务对象
 *
 * @export
 * @class PLANSCHEDULE_TService
 * @extends {PLANSCHEDULE_TServiceBase}
 */
export default class PLANSCHEDULE_TService extends PLANSCHEDULE_TServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_TService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_TService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}