import { Http } from '@/utils';
import { Util } from '@/utils';
import EMENServiceBase from './emen-service-base';


/**
 * 能源服务对象
 *
 * @export
 * @class EMENService
 * @extends {EMENServiceBase}
 */
export default class EMENService extends EMENServiceBase {

    /**
     * Creates an instance of  EMENService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}