import { Http } from '@/utils';
import { Util } from '@/utils';
import PFEmpPostMapServiceBase from './pfemp-post-map-service-base';


/**
 * 人事关系服务对象
 *
 * @export
 * @class PFEmpPostMapService
 * @extends {PFEmpPostMapServiceBase}
 */
export default class PFEmpPostMapService extends PFEmpPostMapServiceBase {

    /**
     * Creates an instance of  PFEmpPostMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpPostMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}