import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULE_WServiceBase from './planschedule-w-service-base';


/**
 * 计划_按周服务对象
 *
 * @export
 * @class PLANSCHEDULE_WService
 * @extends {PLANSCHEDULE_WServiceBase}
 */
export default class PLANSCHEDULE_WService extends PLANSCHEDULE_WServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_WService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_WService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}