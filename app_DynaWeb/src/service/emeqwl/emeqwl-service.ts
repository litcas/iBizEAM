import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQWLServiceBase from './emeqwl-service-base';


/**
 * 设备运行日志服务对象
 *
 * @export
 * @class EMEQWLService
 * @extends {EMEQWLServiceBase}
 */
export default class EMEQWLService extends EMEQWLServiceBase {

    /**
     * Creates an instance of  EMEQWLService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQWLService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}