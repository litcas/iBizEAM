import EMEQWLAuthServiceBase from './emeqwl-auth-service-base';


/**
 * 设备运行日志权限服务对象
 *
 * @export
 * @class EMEQWLAuthService
 * @extends {EMEQWLAuthServiceBase}
 */
export default class EMEQWLAuthService extends EMEQWLAuthServiceBase {

    /**
     * Creates an instance of  EMEQWLAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQWLAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}