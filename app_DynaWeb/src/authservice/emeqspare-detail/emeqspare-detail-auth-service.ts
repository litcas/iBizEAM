import EMEQSpareDetailAuthServiceBase from './emeqspare-detail-auth-service-base';


/**
 * 备件包明细权限服务对象
 *
 * @export
 * @class EMEQSpareDetailAuthService
 * @extends {EMEQSpareDetailAuthServiceBase}
 */
export default class EMEQSpareDetailAuthService extends EMEQSpareDetailAuthServiceBase {

    /**
     * Creates an instance of  EMEQSpareDetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareDetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}