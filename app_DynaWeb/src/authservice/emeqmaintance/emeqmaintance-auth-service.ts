import EMEQMaintanceAuthServiceBase from './emeqmaintance-auth-service-base';


/**
 * 抢修记录权限服务对象
 *
 * @export
 * @class EMEQMaintanceAuthService
 * @extends {EMEQMaintanceAuthServiceBase}
 */
export default class EMEQMaintanceAuthService extends EMEQMaintanceAuthServiceBase {

    /**
     * Creates an instance of  EMEQMaintanceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMaintanceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}