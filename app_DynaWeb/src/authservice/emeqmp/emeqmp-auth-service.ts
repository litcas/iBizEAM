import EMEQMPAuthServiceBase from './emeqmp-auth-service-base';


/**
 * 设备仪表权限服务对象
 *
 * @export
 * @class EMEQMPAuthService
 * @extends {EMEQMPAuthServiceBase}
 */
export default class EMEQMPAuthService extends EMEQMPAuthServiceBase {

    /**
     * Creates an instance of  EMEQMPAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}