import EMMachModelAuthServiceBase from './emmach-model-auth-service-base';


/**
 * 机型权限服务对象
 *
 * @export
 * @class EMMachModelAuthService
 * @extends {EMMachModelAuthServiceBase}
 */
export default class EMMachModelAuthService extends EMMachModelAuthServiceBase {

    /**
     * Creates an instance of  EMMachModelAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachModelAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}