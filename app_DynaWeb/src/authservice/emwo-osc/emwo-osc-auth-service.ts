import EMWO_OSCAuthServiceBase from './emwo-osc-auth-service-base';


/**
 * 外委保养工单权限服务对象
 *
 * @export
 * @class EMWO_OSCAuthService
 * @extends {EMWO_OSCAuthServiceBase}
 */
export default class EMWO_OSCAuthService extends EMWO_OSCAuthServiceBase {

    /**
     * Creates an instance of  EMWO_OSCAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_OSCAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}