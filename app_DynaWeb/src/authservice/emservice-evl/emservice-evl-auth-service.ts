import EMServiceEvlAuthServiceBase from './emservice-evl-auth-service-base';


/**
 * 服务商评估权限服务对象
 *
 * @export
 * @class EMServiceEvlAuthService
 * @extends {EMServiceEvlAuthServiceBase}
 */
export default class EMServiceEvlAuthService extends EMServiceEvlAuthServiceBase {

    /**
     * Creates an instance of  EMServiceEvlAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceEvlAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}