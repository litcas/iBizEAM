package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULESearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划时刻设置" })
@RestController("WebApi-planschedule")
@RequestMapping("")
public class PLANSCHEDULEResource {

    @Autowired
    public IPLANSCHEDULEService planscheduleService;

    @Autowired
    @Lazy
    public PLANSCHEDULEMapping planscheduleMapping;

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "新建计划时刻设置", tags = {"计划时刻设置" },  notes = "新建计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> create(@Validated @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "批量新建计划时刻设置", tags = {"计划时刻设置" },  notes = "批量新建计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        planscheduleService.createBatch(planscheduleMapping.toDomain(planscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "更新计划时刻设置", tags = {"计划时刻设置" },  notes = "更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> update(@PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
		PLANSCHEDULE domain  = planscheduleMapping.toDomain(planscheduledto);
        domain .setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain );
		PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        planscheduleService.updateBatch(planscheduleMapping.toDomain(planscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "删除计划时刻设置", tags = {"计划时刻设置" },  notes = "删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_id") String planschedule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "获取计划时刻设置", tags = {"计划时刻设置" },  notes = "获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> get(@PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "获取计划时刻设置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraft(PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划时刻设置", tags = {"计划时刻设置" },  notes = "检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "定时任务", tags = {"计划时刻设置" },  notes = "定时任务")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTask(@PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setPlanscheduleid(planschedule_id);
        domain = planscheduleService.genTask(domain);
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "批量处理[定时任务]", tags = {"计划时刻设置" },  notes = "批量处理[定时任务]")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskBatch(@RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "保存计划时刻设置", tags = {"计划时刻设置" },  notes = "保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> save(@RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        planscheduleService.saveBatch(planscheduleMapping.toDomain(planscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "获取数据集", tags = {"计划时刻设置" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchDefault(PLANSCHEDULESearchContext context) {
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "查询数据集", tags = {"计划时刻设置" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchDefault(@RequestBody PLANSCHEDULESearchContext context) {
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMPlan(@PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据总帐科目计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据总帐科目计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据总帐科目计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据总帐科目计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据总帐科目计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据总帐科目计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据总帐科目计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据总帐科目计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据总帐科目计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据总帐科目计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据总帐科目计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据总帐科目计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据总帐科目计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据总帐科目计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据总帐科目计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据总帐科目计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据总帐科目计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据设备档案计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据设备档案计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据设备档案计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据设备档案计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据设备档案计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据设备档案计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquipEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据设备档案计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据设备档案计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据设备档案计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据设备档案计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据设备档案计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据设备档案计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据设备档案计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据设备档案计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据设备档案计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据设备档案计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据设备档案计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据设备档案计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据计划模板计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据计划模板计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据计划模板计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据计划模板计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据计划模板计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据计划模板计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据计划模板计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划模板计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据计划模板计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划模板计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据计划模板计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据计划模板计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据计划模板计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据计划模板计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据计划模板计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据计划模板计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据计划模板计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据计划模板计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据服务商计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据服务商计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据服务商计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据服务商计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据服务商计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据服务商计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据服务商计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据服务商计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据服务商计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据服务商计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据服务商计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据服务商计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据服务商计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据服务商计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据服务商计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据服务商计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据服务商计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据班组计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据班组计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据班组计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据班组计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据班组计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据班组计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据班组计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组设备档案计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组设备档案计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组设备档案计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组设备档案计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组设备档案计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组设备档案计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquipEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据班组设备档案计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据班组设备档案计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据班组设备档案计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组设备档案计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组设备档案计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组设备档案计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组设备档案计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据班组设备档案计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组设备档案计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据班组设备档案计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据总帐科目计划模板计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划模板计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划模板计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据总帐科目计划模板计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据总帐科目计划模板计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据总帐科目计划模板计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据总帐科目计划模板计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据总帐科目计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据总帐科目计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据服务商计划模板计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据服务商计划模板计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据服务商计划模板计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据服务商计划模板计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据服务商计划模板计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划模板计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划模板计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据服务商计划模板计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据服务商计划模板计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据服务商计划模板计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据服务商计划模板计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据服务商计划模板计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据服务商计划模板计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据服务商计划模板计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据服务商计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据服务商计划模板计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据服务商计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组计划模板计划建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules")
    public ResponseEntity<PLANSCHEDULEDTO> createByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
		planscheduleService.create(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Create')")
    @ApiOperation(value = "根据班组计划模板计划批量建立计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划批量建立计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组计划模板计划更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> updateByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain.setPlanscheduleid(planschedule_id);
		planscheduleService.update(domain);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByEntities(this.planscheduleMapping.toDomain(#planscheduledtos)),'eam-PLANSCHEDULE-Update')")
    @ApiOperation(value = "根据班组计划模板计划批量更新计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划批量更新计划时刻设置")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        planscheduleService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planscheduleService.get(#planschedule_id),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组计划模板计划删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
		return ResponseEntity.status(HttpStatus.OK).body(planscheduleService.remove(planschedule_id));
    }

    @PreAuthorize("hasPermission(this.planscheduleService.getPlanscheduleByIds(#ids),'eam-PLANSCHEDULE-Remove')")
    @ApiOperation(value = "根据班组计划模板计划批量删除计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划批量删除计划时刻设置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        planscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planscheduleMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE-Get')")
    @ApiOperation(value = "根据班组计划模板计划获取计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划获取计划时刻设置")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}")
    public ResponseEntity<PLANSCHEDULEDTO> getByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id) {
        PLANSCHEDULE domain = planscheduleService.get(planschedule_id);
        PLANSCHEDULEDTO dto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划模板计划获取计划时刻设置草稿", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划获取计划时刻设置草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/getdraft")
    public ResponseEntity<PLANSCHEDULEDTO> getDraftByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, PLANSCHEDULEDTO dto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(planscheduleService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划模板计划检查计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划检查计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planscheduleService.checkKey(planscheduleMapping.toDomain(planscheduledto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-GenTask-all')")
    @ApiOperation(value = "根据班组计划模板计划计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/{planschedule_id}/gentask")
    public ResponseEntity<PLANSCHEDULEDTO> genTaskByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("planschedule_id") String planschedule_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        domain = planscheduleService.genTask(domain) ;
        planscheduledto = planscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduledto);
    }
    @ApiOperation(value = "批量处理[根据班组计划模板计划计划时刻设置]", tags = {"计划时刻设置" },  notes = "批量处理[根据班组计划模板计划计划时刻设置]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/gentaskbatch")
    public ResponseEntity<Boolean> genTaskByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domains = planscheduleMapping.toDomain(planscheduledtos);
        boolean result = planscheduleService.genTaskBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledto),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组计划模板计划保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/save")
    public ResponseEntity<PLANSCHEDULEDTO> saveByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULEDTO planscheduledto) {
        PLANSCHEDULE domain = planscheduleMapping.toDomain(planscheduledto);
        domain.setEmplanid(emplan_id);
        planscheduleService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planscheduleMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planscheduleMapping.toDomain(#planscheduledtos),'eam-PLANSCHEDULE-Save')")
    @ApiOperation(value = "根据班组计划模板计划批量保存计划时刻设置", tags = {"计划时刻设置" },  notes = "根据班组计划模板计划批量保存计划时刻设置")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<PLANSCHEDULEDTO> planscheduledtos) {
        List<PLANSCHEDULE> domainlist=planscheduleMapping.toDomain(planscheduledtos);
        for(PLANSCHEDULE domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        planscheduleService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组计划模板计划获取数据集", tags = {"计划时刻设置" } ,notes = "根据班组计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULEDTO>> fetchPLANSCHEDULEDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
        List<PLANSCHEDULEDTO> list = planscheduleMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE-Get')")
	@ApiOperation(value = "根据班组计划模板计划查询数据集", tags = {"计划时刻设置" } ,notes = "根据班组计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/planschedules/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULEDTO>> searchPLANSCHEDULEDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody PLANSCHEDULESearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<PLANSCHEDULE> domains = planscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

