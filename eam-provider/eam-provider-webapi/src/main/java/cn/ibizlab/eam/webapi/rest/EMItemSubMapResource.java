package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemSubMap;
import cn.ibizlab.eam.core.eam_core.service.IEMItemSubMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemSubMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物品替换件" })
@RestController("WebApi-emitemsubmap")
@RequestMapping("")
public class EMItemSubMapResource {

    @Autowired
    public IEMItemSubMapService emitemsubmapService;

    @Autowired
    @Lazy
    public EMItemSubMapMapping emitemsubmapMapping;

    @PreAuthorize("hasPermission(this.emitemsubmapMapping.toDomain(#emitemsubmapdto),'eam-EMItemSubMap-Create')")
    @ApiOperation(value = "新建物品替换件", tags = {"物品替换件" },  notes = "新建物品替换件")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemsubmaps")
    public ResponseEntity<EMItemSubMapDTO> create(@Validated @RequestBody EMItemSubMapDTO emitemsubmapdto) {
        EMItemSubMap domain = emitemsubmapMapping.toDomain(emitemsubmapdto);
		emitemsubmapService.create(domain);
        EMItemSubMapDTO dto = emitemsubmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemsubmapMapping.toDomain(#emitemsubmapdtos),'eam-EMItemSubMap-Create')")
    @ApiOperation(value = "批量新建物品替换件", tags = {"物品替换件" },  notes = "批量新建物品替换件")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemsubmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemSubMapDTO> emitemsubmapdtos) {
        emitemsubmapService.createBatch(emitemsubmapMapping.toDomain(emitemsubmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemsubmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemsubmapService.get(#emitemsubmap_id),'eam-EMItemSubMap-Update')")
    @ApiOperation(value = "更新物品替换件", tags = {"物品替换件" },  notes = "更新物品替换件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemsubmaps/{emitemsubmap_id}")
    public ResponseEntity<EMItemSubMapDTO> update(@PathVariable("emitemsubmap_id") String emitemsubmap_id, @RequestBody EMItemSubMapDTO emitemsubmapdto) {
		EMItemSubMap domain  = emitemsubmapMapping.toDomain(emitemsubmapdto);
        domain .setEmitemsubmapid(emitemsubmap_id);
		emitemsubmapService.update(domain );
		EMItemSubMapDTO dto = emitemsubmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemsubmapService.getEmitemsubmapByEntities(this.emitemsubmapMapping.toDomain(#emitemsubmapdtos)),'eam-EMItemSubMap-Update')")
    @ApiOperation(value = "批量更新物品替换件", tags = {"物品替换件" },  notes = "批量更新物品替换件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemsubmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemSubMapDTO> emitemsubmapdtos) {
        emitemsubmapService.updateBatch(emitemsubmapMapping.toDomain(emitemsubmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemsubmapService.get(#emitemsubmap_id),'eam-EMItemSubMap-Remove')")
    @ApiOperation(value = "删除物品替换件", tags = {"物品替换件" },  notes = "删除物品替换件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemsubmaps/{emitemsubmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemsubmap_id") String emitemsubmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemsubmapService.remove(emitemsubmap_id));
    }

    @PreAuthorize("hasPermission(this.emitemsubmapService.getEmitemsubmapByIds(#ids),'eam-EMItemSubMap-Remove')")
    @ApiOperation(value = "批量删除物品替换件", tags = {"物品替换件" },  notes = "批量删除物品替换件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemsubmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemsubmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemsubmapMapping.toDomain(returnObject.body),'eam-EMItemSubMap-Get')")
    @ApiOperation(value = "获取物品替换件", tags = {"物品替换件" },  notes = "获取物品替换件")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemsubmaps/{emitemsubmap_id}")
    public ResponseEntity<EMItemSubMapDTO> get(@PathVariable("emitemsubmap_id") String emitemsubmap_id) {
        EMItemSubMap domain = emitemsubmapService.get(emitemsubmap_id);
        EMItemSubMapDTO dto = emitemsubmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物品替换件草稿", tags = {"物品替换件" },  notes = "获取物品替换件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemsubmaps/getdraft")
    public ResponseEntity<EMItemSubMapDTO> getDraft(EMItemSubMapDTO dto) {
        EMItemSubMap domain = emitemsubmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemsubmapMapping.toDto(emitemsubmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查物品替换件", tags = {"物品替换件" },  notes = "检查物品替换件")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemsubmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemSubMapDTO emitemsubmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemsubmapService.checkKey(emitemsubmapMapping.toDomain(emitemsubmapdto)));
    }

    @PreAuthorize("hasPermission(this.emitemsubmapMapping.toDomain(#emitemsubmapdto),'eam-EMItemSubMap-Save')")
    @ApiOperation(value = "保存物品替换件", tags = {"物品替换件" },  notes = "保存物品替换件")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemsubmaps/save")
    public ResponseEntity<EMItemSubMapDTO> save(@RequestBody EMItemSubMapDTO emitemsubmapdto) {
        EMItemSubMap domain = emitemsubmapMapping.toDomain(emitemsubmapdto);
        emitemsubmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemsubmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemsubmapMapping.toDomain(#emitemsubmapdtos),'eam-EMItemSubMap-Save')")
    @ApiOperation(value = "批量保存物品替换件", tags = {"物品替换件" },  notes = "批量保存物品替换件")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemsubmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemSubMapDTO> emitemsubmapdtos) {
        emitemsubmapService.saveBatch(emitemsubmapMapping.toDomain(emitemsubmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemSubMap-searchDefault-all') and hasPermission(#context,'eam-EMItemSubMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"物品替换件" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemsubmaps/fetchdefault")
	public ResponseEntity<List<EMItemSubMapDTO>> fetchDefault(EMItemSubMapSearchContext context) {
        Page<EMItemSubMap> domains = emitemsubmapService.searchDefault(context) ;
        List<EMItemSubMapDTO> list = emitemsubmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemSubMap-searchDefault-all') and hasPermission(#context,'eam-EMItemSubMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"物品替换件" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemsubmaps/searchdefault")
	public ResponseEntity<Page<EMItemSubMapDTO>> searchDefault(@RequestBody EMItemSubMapSearchContext context) {
        Page<EMItemSubMap> domains = emitemsubmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemsubmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

