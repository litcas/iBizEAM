package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import cn.ibizlab.eam.core.eam_core.service.IEMEQAHService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQAHSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动历史" })
@RestController("WebApi-emeqah")
@RequestMapping("")
public class EMEQAHResource {

    @Autowired
    public IEMEQAHService emeqahService;

    @Autowired
    @Lazy
    public EMEQAHMapping emeqahMapping;

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Create')")
    @ApiOperation(value = "新建活动历史", tags = {"活动历史" },  notes = "新建活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqahs")
    public ResponseEntity<EMEQAHDTO> create(@Validated @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
		emeqahService.create(domain);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Create')")
    @ApiOperation(value = "批量新建活动历史", tags = {"活动历史" },  notes = "批量新建活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqahs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQAHDTO> emeqahdtos) {
        emeqahService.createBatch(emeqahMapping.toDomain(emeqahdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqah" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Update')")
    @ApiOperation(value = "更新活动历史", tags = {"活动历史" },  notes = "更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> update(@PathVariable("emeqah_id") String emeqah_id, @RequestBody EMEQAHDTO emeqahdto) {
		EMEQAH domain  = emeqahMapping.toDomain(emeqahdto);
        domain .setEmeqahid(emeqah_id);
		emeqahService.update(domain );
		EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByEntities(this.emeqahMapping.toDomain(#emeqahdtos)),'eam-EMEQAH-Update')")
    @ApiOperation(value = "批量更新活动历史", tags = {"活动历史" },  notes = "批量更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqahs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQAHDTO> emeqahdtos) {
        emeqahService.updateBatch(emeqahMapping.toDomain(emeqahdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "删除活动历史", tags = {"活动历史" },  notes = "删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqahs/{emeqah_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqah_id") String emeqah_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqahService.remove(emeqah_id));
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByIds(#ids),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "批量删除活动历史", tags = {"活动历史" },  notes = "批量删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqahs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqahService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqahMapping.toDomain(returnObject.body),'eam-EMEQAH-Get')")
    @ApiOperation(value = "获取活动历史", tags = {"活动历史" },  notes = "获取活动历史")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> get(@PathVariable("emeqah_id") String emeqah_id) {
        EMEQAH domain = emeqahService.get(emeqah_id);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动历史草稿", tags = {"活动历史" },  notes = "获取活动历史草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqahs/getdraft")
    public ResponseEntity<EMEQAHDTO> getDraft(EMEQAHDTO dto) {
        EMEQAH domain = emeqahMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(emeqahService.getDraft(domain)));
    }

    @ApiOperation(value = "检查活动历史", tags = {"活动历史" },  notes = "检查活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqahs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQAHDTO emeqahdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqahService.checkKey(emeqahMapping.toDomain(emeqahdto)));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Save')")
    @ApiOperation(value = "保存活动历史", tags = {"活动历史" },  notes = "保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqahs/save")
    public ResponseEntity<EMEQAHDTO> save(@RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        emeqahService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Save')")
    @ApiOperation(value = "批量保存活动历史", tags = {"活动历史" },  notes = "批量保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqahs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQAHDTO> emeqahdtos) {
        emeqahService.saveBatch(emeqahMapping.toDomain(emeqahdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"活动历史" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqahs/fetchdefault")
	public ResponseEntity<List<EMEQAHDTO>> fetchDefault(EMEQAHSearchContext context) {
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"活动历史" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqahs/searchdefault")
	public ResponseEntity<Page<EMEQAHDTO>> searchDefault(@RequestBody EMEQAHSearchContext context) {
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"活动历史" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emeqahs/fetchindexder")
	public ResponseEntity<List<EMEQAHDTO>> fetchIndexDER(EMEQAHSearchContext context) {
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"活动历史" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emeqahs/searchindexder")
	public ResponseEntity<Page<EMEQAHDTO>> searchIndexDER(@RequestBody EMEQAHSearchContext context) {
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Create')")
    @ApiOperation(value = "根据设备档案建立活动历史", tags = {"活动历史" },  notes = "根据设备档案建立活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqahs")
    public ResponseEntity<EMEQAHDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
		emeqahService.create(domain);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Create')")
    @ApiOperation(value = "根据设备档案批量建立活动历史", tags = {"活动历史" },  notes = "根据设备档案批量建立活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqahService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqah" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Update')")
    @ApiOperation(value = "根据设备档案更新活动历史", tags = {"活动历史" },  notes = "根据设备档案更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqahid(emeqah_id);
		emeqahService.update(domain);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByEntities(this.emeqahMapping.toDomain(#emeqahdtos)),'eam-EMEQAH-Update')")
    @ApiOperation(value = "根据设备档案批量更新活动历史", tags = {"活动历史" },  notes = "根据设备档案批量更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqahService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "根据设备档案删除活动历史", tags = {"活动历史" },  notes = "根据设备档案删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqahService.remove(emeqah_id));
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByIds(#ids),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "根据设备档案批量删除活动历史", tags = {"活动历史" },  notes = "根据设备档案批量删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqahService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqahMapping.toDomain(returnObject.body),'eam-EMEQAH-Get')")
    @ApiOperation(value = "根据设备档案获取活动历史", tags = {"活动历史" },  notes = "根据设备档案获取活动历史")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id) {
        EMEQAH domain = emeqahService.get(emeqah_id);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取活动历史草稿", tags = {"活动历史" },  notes = "根据设备档案获取活动历史草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqahs/getdraft")
    public ResponseEntity<EMEQAHDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQAHDTO dto) {
        EMEQAH domain = emeqahMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(emeqahService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查活动历史", tags = {"活动历史" },  notes = "根据设备档案检查活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqahs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqahService.checkKey(emeqahMapping.toDomain(emeqahdto)));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Save')")
    @ApiOperation(value = "根据设备档案保存活动历史", tags = {"活动历史" },  notes = "根据设备档案保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqahs/save")
    public ResponseEntity<EMEQAHDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
        emeqahService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Save')")
    @ApiOperation(value = "根据设备档案批量保存活动历史", tags = {"活动历史" },  notes = "根据设备档案批量保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqahs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqahService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"活动历史" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqahs/fetchdefault")
	public ResponseEntity<List<EMEQAHDTO>> fetchEMEQAHDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"活动历史" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqahs/searchdefault")
	public ResponseEntity<Page<EMEQAHDTO>> searchEMEQAHDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据设备档案获取IndexDER", tags = {"活动历史" } ,notes = "根据设备档案获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqahs/fetchindexder")
	public ResponseEntity<List<EMEQAHDTO>> fetchEMEQAHIndexDERByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据设备档案查询IndexDER", tags = {"活动历史" } ,notes = "根据设备档案查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqahs/searchindexder")
	public ResponseEntity<Page<EMEQAHDTO>> searchEMEQAHIndexDERByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Create')")
    @ApiOperation(value = "根据班组设备档案建立活动历史", tags = {"活动历史" },  notes = "根据班组设备档案建立活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs")
    public ResponseEntity<EMEQAHDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
		emeqahService.create(domain);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立活动历史", tags = {"活动历史" },  notes = "根据班组设备档案批量建立活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqahService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqah" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Update')")
    @ApiOperation(value = "根据班组设备档案更新活动历史", tags = {"活动历史" },  notes = "根据班组设备档案更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqahid(emeqah_id);
		emeqahService.update(domain);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByEntities(this.emeqahMapping.toDomain(#emeqahdtos)),'eam-EMEQAH-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新活动历史", tags = {"活动历史" },  notes = "根据班组设备档案批量更新活动历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqahService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqahService.get(#emeqah_id),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "根据班组设备档案删除活动历史", tags = {"活动历史" },  notes = "根据班组设备档案删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqahService.remove(emeqah_id));
    }

    @PreAuthorize("hasPermission(this.emeqahService.getEmeqahByIds(#ids),'eam-EMEQAH-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除活动历史", tags = {"活动历史" },  notes = "根据班组设备档案批量删除活动历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqahService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqahMapping.toDomain(returnObject.body),'eam-EMEQAH-Get')")
    @ApiOperation(value = "根据班组设备档案获取活动历史", tags = {"活动历史" },  notes = "根据班组设备档案获取活动历史")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/{emeqah_id}")
    public ResponseEntity<EMEQAHDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqah_id") String emeqah_id) {
        EMEQAH domain = emeqahService.get(emeqah_id);
        EMEQAHDTO dto = emeqahMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取活动历史草稿", tags = {"活动历史" },  notes = "根据班组设备档案获取活动历史草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/getdraft")
    public ResponseEntity<EMEQAHDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQAHDTO dto) {
        EMEQAH domain = emeqahMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(emeqahService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查活动历史", tags = {"活动历史" },  notes = "根据班组设备档案检查活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqahService.checkKey(emeqahMapping.toDomain(emeqahdto)));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdto),'eam-EMEQAH-Save')")
    @ApiOperation(value = "根据班组设备档案保存活动历史", tags = {"活动历史" },  notes = "根据班组设备档案保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/save")
    public ResponseEntity<EMEQAHDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHDTO emeqahdto) {
        EMEQAH domain = emeqahMapping.toDomain(emeqahdto);
        domain.setEquipid(emequip_id);
        emeqahService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqahMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqahMapping.toDomain(#emeqahdtos),'eam-EMEQAH-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存活动历史", tags = {"活动历史" },  notes = "根据班组设备档案批量保存活动历史")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQAHDTO> emeqahdtos) {
        List<EMEQAH> domainlist=emeqahMapping.toDomain(emeqahdtos);
        for(EMEQAH domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqahService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"活动历史" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/fetchdefault")
	public ResponseEntity<List<EMEQAHDTO>> fetchEMEQAHDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchDefault-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"活动历史" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/searchdefault")
	public ResponseEntity<Page<EMEQAHDTO>> searchEMEQAHDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据班组设备档案获取IndexDER", tags = {"活动历史" } ,notes = "根据班组设备档案获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/fetchindexder")
	public ResponseEntity<List<EMEQAHDTO>> fetchEMEQAHIndexDERByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
        List<EMEQAHDTO> list = emeqahMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQAH-searchIndexDER-all') and hasPermission(#context,'eam-EMEQAH-Get')")
	@ApiOperation(value = "根据班组设备档案查询IndexDER", tags = {"活动历史" } ,notes = "根据班组设备档案查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqahs/searchindexder")
	public ResponseEntity<Page<EMEQAHDTO>> searchEMEQAHIndexDERByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQAHSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQAH> domains = emeqahService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqahMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

