package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetHist;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetHistService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetHistSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资产历史记录" })
@RestController("WebApi-emassethist")
@RequestMapping("")
public class EMAssetHistResource {

    @Autowired
    public IEMAssetHistService emassethistService;

    @Autowired
    @Lazy
    public EMAssetHistMapping emassethistMapping;

    @PreAuthorize("hasPermission(this.emassethistMapping.toDomain(#emassethistdto),'eam-EMAssetHist-Create')")
    @ApiOperation(value = "新建资产历史记录", tags = {"资产历史记录" },  notes = "新建资产历史记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassethists")
    public ResponseEntity<EMAssetHistDTO> create(@Validated @RequestBody EMAssetHistDTO emassethistdto) {
        EMAssetHist domain = emassethistMapping.toDomain(emassethistdto);
		emassethistService.create(domain);
        EMAssetHistDTO dto = emassethistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassethistMapping.toDomain(#emassethistdtos),'eam-EMAssetHist-Create')")
    @ApiOperation(value = "批量新建资产历史记录", tags = {"资产历史记录" },  notes = "批量新建资产历史记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassethists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssetHistDTO> emassethistdtos) {
        emassethistService.createBatch(emassethistMapping.toDomain(emassethistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassethist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassethistService.get(#emassethist_id),'eam-EMAssetHist-Update')")
    @ApiOperation(value = "更新资产历史记录", tags = {"资产历史记录" },  notes = "更新资产历史记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassethists/{emassethist_id}")
    public ResponseEntity<EMAssetHistDTO> update(@PathVariable("emassethist_id") String emassethist_id, @RequestBody EMAssetHistDTO emassethistdto) {
		EMAssetHist domain  = emassethistMapping.toDomain(emassethistdto);
        domain .setEmassethistid(emassethist_id);
		emassethistService.update(domain );
		EMAssetHistDTO dto = emassethistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassethistService.getEmassethistByEntities(this.emassethistMapping.toDomain(#emassethistdtos)),'eam-EMAssetHist-Update')")
    @ApiOperation(value = "批量更新资产历史记录", tags = {"资产历史记录" },  notes = "批量更新资产历史记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassethists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssetHistDTO> emassethistdtos) {
        emassethistService.updateBatch(emassethistMapping.toDomain(emassethistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassethistService.get(#emassethist_id),'eam-EMAssetHist-Remove')")
    @ApiOperation(value = "删除资产历史记录", tags = {"资产历史记录" },  notes = "删除资产历史记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassethists/{emassethist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassethist_id") String emassethist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassethistService.remove(emassethist_id));
    }

    @PreAuthorize("hasPermission(this.emassethistService.getEmassethistByIds(#ids),'eam-EMAssetHist-Remove')")
    @ApiOperation(value = "批量删除资产历史记录", tags = {"资产历史记录" },  notes = "批量删除资产历史记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassethists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassethistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassethistMapping.toDomain(returnObject.body),'eam-EMAssetHist-Get')")
    @ApiOperation(value = "获取资产历史记录", tags = {"资产历史记录" },  notes = "获取资产历史记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emassethists/{emassethist_id}")
    public ResponseEntity<EMAssetHistDTO> get(@PathVariable("emassethist_id") String emassethist_id) {
        EMAssetHist domain = emassethistService.get(emassethist_id);
        EMAssetHistDTO dto = emassethistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资产历史记录草稿", tags = {"资产历史记录" },  notes = "获取资产历史记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassethists/getdraft")
    public ResponseEntity<EMAssetHistDTO> getDraft(EMAssetHistDTO dto) {
        EMAssetHist domain = emassethistMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassethistMapping.toDto(emassethistService.getDraft(domain)));
    }

    @ApiOperation(value = "检查资产历史记录", tags = {"资产历史记录" },  notes = "检查资产历史记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassethists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssetHistDTO emassethistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassethistService.checkKey(emassethistMapping.toDomain(emassethistdto)));
    }

    @PreAuthorize("hasPermission(this.emassethistMapping.toDomain(#emassethistdto),'eam-EMAssetHist-Save')")
    @ApiOperation(value = "保存资产历史记录", tags = {"资产历史记录" },  notes = "保存资产历史记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassethists/save")
    public ResponseEntity<EMAssetHistDTO> save(@RequestBody EMAssetHistDTO emassethistdto) {
        EMAssetHist domain = emassethistMapping.toDomain(emassethistdto);
        emassethistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassethistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassethistMapping.toDomain(#emassethistdtos),'eam-EMAssetHist-Save')")
    @ApiOperation(value = "批量保存资产历史记录", tags = {"资产历史记录" },  notes = "批量保存资产历史记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassethists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssetHistDTO> emassethistdtos) {
        emassethistService.saveBatch(emassethistMapping.toDomain(emassethistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetHist-searchDefault-all') and hasPermission(#context,'eam-EMAssetHist-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"资产历史记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassethists/fetchdefault")
	public ResponseEntity<List<EMAssetHistDTO>> fetchDefault(EMAssetHistSearchContext context) {
        Page<EMAssetHist> domains = emassethistService.searchDefault(context) ;
        List<EMAssetHistDTO> list = emassethistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetHist-searchDefault-all') and hasPermission(#context,'eam-EMAssetHist-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"资产历史记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassethists/searchdefault")
	public ResponseEntity<Page<EMAssetHistDTO>> searchDefault(@RequestBody EMAssetHistSearchContext context) {
        Page<EMAssetHist> domains = emassethistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassethistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

