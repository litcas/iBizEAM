package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.SEQUENCE;
import cn.ibizlab.eam.core.eam_core.service.ISEQUENCEService;
import cn.ibizlab.eam.core.eam_core.filter.SEQUENCESearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"序列号" })
@RestController("WebApi-sequence")
@RequestMapping("")
public class SEQUENCEResource {

    @Autowired
    public ISEQUENCEService sequenceService;

    @Autowired
    @Lazy
    public SEQUENCEMapping sequenceMapping;

    @PreAuthorize("hasPermission(this.sequenceMapping.toDomain(#sequencedto),'eam-SEQUENCE-Create')")
    @ApiOperation(value = "新建序列号", tags = {"序列号" },  notes = "新建序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/sequences")
    public ResponseEntity<SEQUENCEDTO> create(@Validated @RequestBody SEQUENCEDTO sequencedto) {
        SEQUENCE domain = sequenceMapping.toDomain(sequencedto);
		sequenceService.create(domain);
        SEQUENCEDTO dto = sequenceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sequenceMapping.toDomain(#sequencedtos),'eam-SEQUENCE-Create')")
    @ApiOperation(value = "批量新建序列号", tags = {"序列号" },  notes = "批量新建序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/sequences/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<SEQUENCEDTO> sequencedtos) {
        sequenceService.createBatch(sequenceMapping.toDomain(sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sequence" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.sequenceService.get(#sequence_id),'eam-SEQUENCE-Update')")
    @ApiOperation(value = "更新序列号", tags = {"序列号" },  notes = "更新序列号")
	@RequestMapping(method = RequestMethod.PUT, value = "/sequences/{sequence_id}")
    public ResponseEntity<SEQUENCEDTO> update(@PathVariable("sequence_id") String sequence_id, @RequestBody SEQUENCEDTO sequencedto) {
		SEQUENCE domain  = sequenceMapping.toDomain(sequencedto);
        domain .setSequenceid(sequence_id);
		sequenceService.update(domain );
		SEQUENCEDTO dto = sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sequenceService.getSequenceByEntities(this.sequenceMapping.toDomain(#sequencedtos)),'eam-SEQUENCE-Update')")
    @ApiOperation(value = "批量更新序列号", tags = {"序列号" },  notes = "批量更新序列号")
	@RequestMapping(method = RequestMethod.PUT, value = "/sequences/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<SEQUENCEDTO> sequencedtos) {
        sequenceService.updateBatch(sequenceMapping.toDomain(sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sequenceService.get(#sequence_id),'eam-SEQUENCE-Remove')")
    @ApiOperation(value = "删除序列号", tags = {"序列号" },  notes = "删除序列号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sequences/{sequence_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sequence_id") String sequence_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sequenceService.remove(sequence_id));
    }

    @PreAuthorize("hasPermission(this.sequenceService.getSequenceByIds(#ids),'eam-SEQUENCE-Remove')")
    @ApiOperation(value = "批量删除序列号", tags = {"序列号" },  notes = "批量删除序列号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sequences/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        sequenceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sequenceMapping.toDomain(returnObject.body),'eam-SEQUENCE-Get')")
    @ApiOperation(value = "获取序列号", tags = {"序列号" },  notes = "获取序列号")
	@RequestMapping(method = RequestMethod.GET, value = "/sequences/{sequence_id}")
    public ResponseEntity<SEQUENCEDTO> get(@PathVariable("sequence_id") String sequence_id) {
        SEQUENCE domain = sequenceService.get(sequence_id);
        SEQUENCEDTO dto = sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取序列号草稿", tags = {"序列号" },  notes = "获取序列号草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sequences/getdraft")
    public ResponseEntity<SEQUENCEDTO> getDraft(SEQUENCEDTO dto) {
        SEQUENCE domain = sequenceMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(sequenceMapping.toDto(sequenceService.getDraft(domain)));
    }

    @ApiOperation(value = "检查序列号", tags = {"序列号" },  notes = "检查序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/sequences/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody SEQUENCEDTO sequencedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sequenceService.checkKey(sequenceMapping.toDomain(sequencedto)));
    }

    @PreAuthorize("hasPermission(this.sequenceMapping.toDomain(#sequencedto),'eam-SEQUENCE-Save')")
    @ApiOperation(value = "保存序列号", tags = {"序列号" },  notes = "保存序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/sequences/save")
    public ResponseEntity<SEQUENCEDTO> save(@RequestBody SEQUENCEDTO sequencedto) {
        SEQUENCE domain = sequenceMapping.toDomain(sequencedto);
        sequenceService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(sequenceMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.sequenceMapping.toDomain(#sequencedtos),'eam-SEQUENCE-Save')")
    @ApiOperation(value = "批量保存序列号", tags = {"序列号" },  notes = "批量保存序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/sequences/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<SEQUENCEDTO> sequencedtos) {
        sequenceService.saveBatch(sequenceMapping.toDomain(sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-SEQUENCE-searchDefault-all') and hasPermission(#context,'eam-SEQUENCE-Get')")
	@ApiOperation(value = "获取数据集", tags = {"序列号" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sequences/fetchdefault")
	public ResponseEntity<List<SEQUENCEDTO>> fetchDefault(SEQUENCESearchContext context) {
        Page<SEQUENCE> domains = sequenceService.searchDefault(context) ;
        List<SEQUENCEDTO> list = sequenceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-SEQUENCE-searchDefault-all') and hasPermission(#context,'eam-SEQUENCE-Get')")
	@ApiOperation(value = "查询数据集", tags = {"序列号" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sequences/searchdefault")
	public ResponseEntity<Page<SEQUENCEDTO>> searchDefault(@RequestBody SEQUENCESearchContext context) {
        Page<SEQUENCE> domains = sequenceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sequenceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

