package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_D;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_DService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_DSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划_按天" })
@RestController("WebApi-planschedule_d")
@RequestMapping("")
public class PLANSCHEDULE_DResource {

    @Autowired
    public IPLANSCHEDULE_DService planschedule_dService;

    @Autowired
    @Lazy
    public PLANSCHEDULE_DMapping planschedule_dMapping;

    @PreAuthorize("hasPermission(this.planschedule_dMapping.toDomain(#planschedule_ddto),'eam-PLANSCHEDULE_D-Create')")
    @ApiOperation(value = "新建计划_按天", tags = {"计划_按天" },  notes = "新建计划_按天")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ds")
    public ResponseEntity<PLANSCHEDULE_DDTO> create(@Validated @RequestBody PLANSCHEDULE_DDTO planschedule_ddto) {
        PLANSCHEDULE_D domain = planschedule_dMapping.toDomain(planschedule_ddto);
		planschedule_dService.create(domain);
        PLANSCHEDULE_DDTO dto = planschedule_dMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_dMapping.toDomain(#planschedule_ddtos),'eam-PLANSCHEDULE_D-Create')")
    @ApiOperation(value = "批量新建计划_按天", tags = {"计划_按天" },  notes = "批量新建计划_按天")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULE_DDTO> planschedule_ddtos) {
        planschedule_dService.createBatch(planschedule_dMapping.toDomain(planschedule_ddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule_d" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planschedule_dService.get(#planschedule_d_id),'eam-PLANSCHEDULE_D-Update')")
    @ApiOperation(value = "更新计划_按天", tags = {"计划_按天" },  notes = "更新计划_按天")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ds/{planschedule_d_id}")
    public ResponseEntity<PLANSCHEDULE_DDTO> update(@PathVariable("planschedule_d_id") String planschedule_d_id, @RequestBody PLANSCHEDULE_DDTO planschedule_ddto) {
		PLANSCHEDULE_D domain  = planschedule_dMapping.toDomain(planschedule_ddto);
        domain .setPlanscheduleDid(planschedule_d_id);
		planschedule_dService.update(domain );
		PLANSCHEDULE_DDTO dto = planschedule_dMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_dService.getPlanscheduleDByEntities(this.planschedule_dMapping.toDomain(#planschedule_ddtos)),'eam-PLANSCHEDULE_D-Update')")
    @ApiOperation(value = "批量更新计划_按天", tags = {"计划_按天" },  notes = "批量更新计划_按天")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULE_DDTO> planschedule_ddtos) {
        planschedule_dService.updateBatch(planschedule_dMapping.toDomain(planschedule_ddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planschedule_dService.get(#planschedule_d_id),'eam-PLANSCHEDULE_D-Remove')")
    @ApiOperation(value = "删除计划_按天", tags = {"计划_按天" },  notes = "删除计划_按天")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ds/{planschedule_d_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_d_id") String planschedule_d_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planschedule_dService.remove(planschedule_d_id));
    }

    @PreAuthorize("hasPermission(this.planschedule_dService.getPlanscheduleDByIds(#ids),'eam-PLANSCHEDULE_D-Remove')")
    @ApiOperation(value = "批量删除计划_按天", tags = {"计划_按天" },  notes = "批量删除计划_按天")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planschedule_dService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planschedule_dMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE_D-Get')")
    @ApiOperation(value = "获取计划_按天", tags = {"计划_按天" },  notes = "获取计划_按天")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ds/{planschedule_d_id}")
    public ResponseEntity<PLANSCHEDULE_DDTO> get(@PathVariable("planschedule_d_id") String planschedule_d_id) {
        PLANSCHEDULE_D domain = planschedule_dService.get(planschedule_d_id);
        PLANSCHEDULE_DDTO dto = planschedule_dMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划_按天草稿", tags = {"计划_按天" },  notes = "获取计划_按天草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ds/getdraft")
    public ResponseEntity<PLANSCHEDULE_DDTO> getDraft(PLANSCHEDULE_DDTO dto) {
        PLANSCHEDULE_D domain = planschedule_dMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_dMapping.toDto(planschedule_dService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划_按天", tags = {"计划_按天" },  notes = "检查计划_按天")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULE_DDTO planschedule_ddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planschedule_dService.checkKey(planschedule_dMapping.toDomain(planschedule_ddto)));
    }

    @PreAuthorize("hasPermission(this.planschedule_dMapping.toDomain(#planschedule_ddto),'eam-PLANSCHEDULE_D-Save')")
    @ApiOperation(value = "保存计划_按天", tags = {"计划_按天" },  notes = "保存计划_按天")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ds/save")
    public ResponseEntity<PLANSCHEDULE_DDTO> save(@RequestBody PLANSCHEDULE_DDTO planschedule_ddto) {
        PLANSCHEDULE_D domain = planschedule_dMapping.toDomain(planschedule_ddto);
        planschedule_dService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_dMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planschedule_dMapping.toDomain(#planschedule_ddtos),'eam-PLANSCHEDULE_D-Save')")
    @ApiOperation(value = "批量保存计划_按天", tags = {"计划_按天" },  notes = "批量保存计划_按天")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULE_DDTO> planschedule_ddtos) {
        planschedule_dService.saveBatch(planschedule_dMapping.toDomain(planschedule_ddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_D-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_D-Get')")
	@ApiOperation(value = "获取数据集", tags = {"计划_按天" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedule_ds/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULE_DDTO>> fetchDefault(PLANSCHEDULE_DSearchContext context) {
        Page<PLANSCHEDULE_D> domains = planschedule_dService.searchDefault(context) ;
        List<PLANSCHEDULE_DDTO> list = planschedule_dMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_D-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_D-Get')")
	@ApiOperation(value = "查询数据集", tags = {"计划_按天" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedule_ds/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULE_DDTO>> searchDefault(@RequestBody PLANSCHEDULE_DSearchContext context) {
        Page<PLANSCHEDULE_D> domains = planschedule_dService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planschedule_dMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

