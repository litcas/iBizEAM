package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlan;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanService;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划" })
@RestController("WebApi-emplan")
@RequestMapping("")
public class EMPlanResource {

    @Autowired
    public IEMPlanService emplanService;

    @Autowired
    @Lazy
    public EMPlanMapping emplanMapping;

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "新建计划", tags = {"计划" },  notes = "新建计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans")
    public ResponseEntity<EMPlanDTO> create(@Validated @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "批量新建计划", tags = {"计划" },  notes = "批量新建计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPlanDTO> emplandtos) {
        emplanService.createBatch(emplanMapping.toDomain(emplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "更新计划", tags = {"计划" },  notes = "更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> update(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
		EMPlan domain  = emplanMapping.toDomain(emplandto);
        domain .setEmplanid(emplan_id);
		emplanService.update(domain );
		EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "批量更新计划", tags = {"计划" },  notes = "批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPlanDTO> emplandtos) {
        emplanService.updateBatch(emplanMapping.toDomain(emplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "删除计划", tags = {"计划" },  notes = "删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplan_id") String emplan_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "批量删除计划", tags = {"计划" },  notes = "批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "获取计划", tags = {"计划" },  notes = "获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> get(@PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划草稿", tags = {"计划" },  notes = "获取计划草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraft(EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划", tags = {"计划" },  notes = "检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "创建工单", tags = {"计划" },  notes = "创建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWO(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEmplanid(emplan_id);
        domain = emplanService.createWO(domain);
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "批量处理[创建工单]", tags = {"计划" },  notes = "批量处理[创建工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOBatch(@RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "设备信息表单更新", tags = {"计划" },  notes = "设备信息表单更新")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIP(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEmplanid(emplan_id);
        domain = emplanService.formUpdateByEQUIP(domain);
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "保存计划", tags = {"计划" },  notes = "保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/save")
    public ResponseEntity<EMPlanDTO> save(@RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "批量保存计划", tags = {"计划" },  notes = "批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPlanDTO> emplandtos) {
        emplanService.saveBatch(emplanMapping.toDomain(emplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchDefault(EMPlanSearchContext context) {
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchDefault(@RequestBody EMPlanSearchContext context) {
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据总帐科目建立计划", tags = {"计划" },  notes = "根据总帐科目建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setAcclassid(emacclass_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据总帐科目批量建立计划", tags = {"计划" },  notes = "根据总帐科目批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setAcclassid(emacclass_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据总帐科目更新计划", tags = {"计划" },  notes = "根据总帐科目更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setAcclassid(emacclass_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据总帐科目批量更新计划", tags = {"计划" },  notes = "根据总帐科目批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setAcclassid(emacclass_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据总帐科目删除计划", tags = {"计划" },  notes = "根据总帐科目删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据总帐科目批量删除计划", tags = {"计划" },  notes = "根据总帐科目批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClass(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据总帐科目获取计划", tags = {"计划" },  notes = "根据总帐科目获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目获取计划草稿", tags = {"计划" },  notes = "根据总帐科目获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMACClass(@PathVariable("emacclass_id") String emacclass_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setAcclassid(emacclass_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目检查计划", tags = {"计划" },  notes = "根据总帐科目检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据总帐科目计划", tags = {"计划" },  notes = "根据总帐科目计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setAcclassid(emacclass_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据总帐科目计划]", tags = {"计划" },  notes = "批量处理[根据总帐科目计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据总帐科目计划", tags = {"计划" },  notes = "根据总帐科目计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setAcclassid(emacclass_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据总帐科目保存计划", tags = {"计划" },  notes = "根据总帐科目保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setAcclassid(emacclass_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据总帐科目批量保存计划", tags = {"计划" },  notes = "根据总帐科目批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setAcclassid(emacclass_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据总帐科目获取DEFAULT", tags = {"计划" } ,notes = "根据总帐科目获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMACClass(@PathVariable("emacclass_id") String emacclass_id,EMPlanSearchContext context) {
        context.setN_acclassid_eq(emacclass_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据总帐科目查询DEFAULT", tags = {"计划" } ,notes = "根据总帐科目查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanSearchContext context) {
        context.setN_acclassid_eq(emacclass_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据设备档案建立计划", tags = {"计划" },  notes = "根据设备档案建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据设备档案批量建立计划", tags = {"计划" },  notes = "根据设备档案批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据设备档案更新计划", tags = {"计划" },  notes = "根据设备档案更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据设备档案批量更新计划", tags = {"计划" },  notes = "根据设备档案批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据设备档案删除计划", tags = {"计划" },  notes = "根据设备档案删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据设备档案批量删除计划", tags = {"计划" },  notes = "根据设备档案批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据设备档案获取计划", tags = {"计划" },  notes = "根据设备档案获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取计划草稿", tags = {"计划" },  notes = "根据设备档案获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查计划", tags = {"计划" },  notes = "根据设备档案检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据设备档案计划", tags = {"计划" },  notes = "根据设备档案计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据设备档案计划]", tags = {"计划" },  notes = "批量处理[根据设备档案计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据设备档案计划", tags = {"计划" },  notes = "根据设备档案计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据设备档案保存计划", tags = {"计划" },  notes = "根据设备档案保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据设备档案批量保存计划", tags = {"计划" },  notes = "根据设备档案批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"计划" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMPlanSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"计划" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据计划模板建立计划", tags = {"计划" },  notes = "根据计划模板建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据计划模板批量建立计划", tags = {"计划" },  notes = "根据计划模板批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据计划模板更新计划", tags = {"计划" },  notes = "根据计划模板更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据计划模板批量更新计划", tags = {"计划" },  notes = "根据计划模板批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据计划模板删除计划", tags = {"计划" },  notes = "根据计划模板删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据计划模板批量删除计划", tags = {"计划" },  notes = "根据计划模板批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlanTempl(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据计划模板获取计划", tags = {"计划" },  notes = "根据计划模板获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划模板获取计划草稿", tags = {"计划" },  notes = "根据计划模板获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setPlantemplid(emplantempl_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划模板检查计划", tags = {"计划" },  notes = "根据计划模板检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据计划模板计划", tags = {"计划" },  notes = "根据计划模板计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据计划模板计划]", tags = {"计划" },  notes = "批量处理[根据计划模板计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据计划模板计划", tags = {"计划" },  notes = "根据计划模板计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据计划模板保存计划", tags = {"计划" },  notes = "根据计划模板保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据计划模板批量保存计划", tags = {"计划" },  notes = "根据计划模板批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setPlantemplid(emplantempl_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据计划模板获取DEFAULT", tags = {"计划" } ,notes = "根据计划模板获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/{emplantempl_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id,EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据计划模板查询DEFAULT", tags = {"计划" } ,notes = "根据计划模板查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/{emplantempl_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMPlanTempl(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据服务商建立计划", tags = {"计划" },  notes = "根据服务商建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRserviceid(emservice_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据服务商批量建立计划", tags = {"计划" },  notes = "根据服务商批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setRserviceid(emservice_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据服务商更新计划", tags = {"计划" },  notes = "根据服务商更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRserviceid(emservice_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据服务商批量更新计划", tags = {"计划" },  notes = "根据服务商批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setRserviceid(emservice_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据服务商删除计划", tags = {"计划" },  notes = "根据服务商删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据服务商批量删除计划", tags = {"计划" },  notes = "根据服务商批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据服务商获取计划", tags = {"计划" },  notes = "根据服务商获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取计划草稿", tags = {"计划" },  notes = "根据服务商获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setRserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查计划", tags = {"计划" },  notes = "根据服务商检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据服务商计划", tags = {"计划" },  notes = "根据服务商计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRserviceid(emservice_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据服务商计划]", tags = {"计划" },  notes = "批量处理[根据服务商计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据服务商计划", tags = {"计划" },  notes = "根据服务商计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRserviceid(emservice_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据服务商保存计划", tags = {"计划" },  notes = "根据服务商保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRserviceid(emservice_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据服务商批量保存计划", tags = {"计划" },  notes = "根据服务商批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setRserviceid(emservice_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"计划" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMPlanSearchContext context) {
        context.setN_rserviceid_eq(emservice_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"计划" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanSearchContext context) {
        context.setN_rserviceid_eq(emservice_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组建立计划", tags = {"计划" },  notes = "根据班组建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRteamid(pfteam_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组批量建立计划", tags = {"计划" },  notes = "根据班组批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组更新计划", tags = {"计划" },  notes = "根据班组更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRteamid(pfteam_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组批量更新计划", tags = {"计划" },  notes = "根据班组批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组删除计划", tags = {"计划" },  notes = "根据班组删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组批量删除计划", tags = {"计划" },  notes = "根据班组批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeam(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据班组获取计划", tags = {"计划" },  notes = "根据班组获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组获取计划草稿", tags = {"计划" },  notes = "根据班组获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组检查计划", tags = {"计划" },  notes = "根据班组检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据班组计划", tags = {"计划" },  notes = "根据班组计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRteamid(pfteam_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据班组计划]", tags = {"计划" },  notes = "批量处理[根据班组计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据班组计划", tags = {"计划" },  notes = "根据班组计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRteamid(pfteam_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组保存计划", tags = {"计划" },  notes = "根据班组保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setRteamid(pfteam_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组批量保存计划", tags = {"计划" },  notes = "根据班组批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setRteamid(pfteam_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组获取DEFAULT", tags = {"计划" } ,notes = "根据班组获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMPlanSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组查询DEFAULT", tags = {"计划" } ,notes = "根据班组查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组设备档案建立计划", tags = {"计划" },  notes = "根据班组设备档案建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立计划", tags = {"计划" },  notes = "根据班组设备档案批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组设备档案更新计划", tags = {"计划" },  notes = "根据班组设备档案更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新计划", tags = {"计划" },  notes = "根据班组设备档案批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组设备档案删除计划", tags = {"计划" },  notes = "根据班组设备档案删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除计划", tags = {"计划" },  notes = "根据班组设备档案批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据班组设备档案获取计划", tags = {"计划" },  notes = "根据班组设备档案获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取计划草稿", tags = {"计划" },  notes = "根据班组设备档案获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查计划", tags = {"计划" },  notes = "根据班组设备档案检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据班组设备档案计划", tags = {"计划" },  notes = "根据班组设备档案计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案计划]", tags = {"计划" },  notes = "批量处理[根据班组设备档案计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据班组设备档案计划", tags = {"计划" },  notes = "根据班组设备档案计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组设备档案保存计划", tags = {"计划" },  notes = "根据班组设备档案保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setEquipid(emequip_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存计划", tags = {"计划" },  notes = "根据班组设备档案批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"计划" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMPlanSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"计划" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMPlanSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据总帐科目计划模板建立计划", tags = {"计划" },  notes = "根据总帐科目计划模板建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据总帐科目计划模板批量建立计划", tags = {"计划" },  notes = "根据总帐科目计划模板批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据总帐科目计划模板更新计划", tags = {"计划" },  notes = "根据总帐科目计划模板更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据总帐科目计划模板批量更新计划", tags = {"计划" },  notes = "根据总帐科目计划模板批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板删除计划", tags = {"计划" },  notes = "根据总帐科目计划模板删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板批量删除计划", tags = {"计划" },  notes = "根据总帐科目计划模板批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlanTempl(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据总帐科目计划模板获取计划", tags = {"计划" },  notes = "根据总帐科目计划模板获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划模板获取计划草稿", tags = {"计划" },  notes = "根据总帐科目计划模板获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setPlantemplid(emplantempl_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划模板检查计划", tags = {"计划" },  notes = "根据总帐科目计划模板检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据总帐科目计划模板计划", tags = {"计划" },  notes = "根据总帐科目计划模板计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据总帐科目计划模板计划]", tags = {"计划" },  notes = "批量处理[根据总帐科目计划模板计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据总帐科目计划模板计划", tags = {"计划" },  notes = "根据总帐科目计划模板计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据总帐科目计划模板保存计划", tags = {"计划" },  notes = "根据总帐科目计划模板保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据总帐科目计划模板批量保存计划", tags = {"计划" },  notes = "根据总帐科目计划模板批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setPlantemplid(emplantempl_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据总帐科目计划模板获取DEFAULT", tags = {"计划" } ,notes = "根据总帐科目计划模板获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id,EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据总帐科目计划模板查询DEFAULT", tags = {"计划" } ,notes = "根据总帐科目计划模板查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMACClassEMPlanTempl(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据服务商计划模板建立计划", tags = {"计划" },  notes = "根据服务商计划模板建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据服务商计划模板批量建立计划", tags = {"计划" },  notes = "根据服务商计划模板批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据服务商计划模板更新计划", tags = {"计划" },  notes = "根据服务商计划模板更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据服务商计划模板批量更新计划", tags = {"计划" },  notes = "根据服务商计划模板批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据服务商计划模板删除计划", tags = {"计划" },  notes = "根据服务商计划模板删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据服务商计划模板批量删除计划", tags = {"计划" },  notes = "根据服务商计划模板批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlanTempl(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据服务商计划模板获取计划", tags = {"计划" },  notes = "根据服务商计划模板获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划模板获取计划草稿", tags = {"计划" },  notes = "根据服务商计划模板获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setPlantemplid(emplantempl_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划模板检查计划", tags = {"计划" },  notes = "根据服务商计划模板检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据服务商计划模板计划", tags = {"计划" },  notes = "根据服务商计划模板计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据服务商计划模板计划]", tags = {"计划" },  notes = "批量处理[根据服务商计划模板计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据服务商计划模板计划", tags = {"计划" },  notes = "根据服务商计划模板计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据服务商计划模板保存计划", tags = {"计划" },  notes = "根据服务商计划模板保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据服务商计划模板批量保存计划", tags = {"计划" },  notes = "根据服务商计划模板批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setPlantemplid(emplantempl_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据服务商计划模板获取DEFAULT", tags = {"计划" } ,notes = "根据服务商计划模板获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id,EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据服务商计划模板查询DEFAULT", tags = {"计划" } ,notes = "根据服务商计划模板查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByEMServiceEMPlanTempl(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组计划模板建立计划", tags = {"计划" },  notes = "根据班组计划模板建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans")
    public ResponseEntity<EMPlanDTO> createByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
		emplanService.create(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Create')")
    @ApiOperation(value = "根据班组计划模板批量建立计划", tags = {"计划" },  notes = "根据班组计划模板批量建立计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组计划模板更新计划", tags = {"计划" },  notes = "根据班组计划模板更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> updateByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain.setEmplanid(emplan_id);
		emplanService.update(domain);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByEntities(this.emplanMapping.toDomain(#emplandtos)),'eam-EMPlan-Update')")
    @ApiOperation(value = "根据班组计划模板批量更新计划", tags = {"计划" },  notes = "根据班组计划模板批量更新计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
            domain.setPlantemplid(emplantempl_id);
        }
        emplanService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanService.get(#emplan_id),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组计划模板删除计划", tags = {"计划" },  notes = "根据班组计划模板删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanService.remove(emplan_id));
    }

    @PreAuthorize("hasPermission(this.emplanService.getEmplanByIds(#ids),'eam-EMPlan-Remove')")
    @ApiOperation(value = "根据班组计划模板批量删除计划", tags = {"计划" },  notes = "根据班组计划模板批量删除计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlanTempl(@RequestBody List<String> ids) {
        emplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanMapping.toDomain(returnObject.body),'eam-EMPlan-Get')")
    @ApiOperation(value = "根据班组计划模板获取计划", tags = {"计划" },  notes = "根据班组计划模板获取计划")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}")
    public ResponseEntity<EMPlanDTO> getByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id) {
        EMPlan domain = emplanService.get(emplan_id);
        EMPlanDTO dto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划模板获取计划草稿", tags = {"计划" },  notes = "根据班组计划模板获取计划草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/getdraft")
    public ResponseEntity<EMPlanDTO> getDraftByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, EMPlanDTO dto) {
        EMPlan domain = emplanMapping.toDomain(dto);
        domain.setPlantemplid(emplantempl_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(emplanService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划模板检查计划", tags = {"计划" },  notes = "根据班组计划模板检查计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanService.checkKey(emplanMapping.toDomain(emplandto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-CreateWO-all')")
    @ApiOperation(value = "根据班组计划模板计划", tags = {"计划" },  notes = "根据班组计划模板计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/createwo")
    public ResponseEntity<EMPlanDTO> createWOByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.createWO(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @ApiOperation(value = "批量处理[根据班组计划模板计划]", tags = {"计划" },  notes = "批量处理[根据班组计划模板计划]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/createwobatch")
    public ResponseEntity<Boolean> createWOByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domains = emplanMapping.toDomain(emplandtos);
        boolean result = emplanService.createWOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-FormUpdateByEQUIP-all')")
    @ApiOperation(value = "根据班组计划模板计划", tags = {"计划" },  notes = "根据班组计划模板计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/formupdatebyequip")
    public ResponseEntity<EMPlanDTO> formUpdateByEQUIPByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        domain = emplanService.formUpdateByEQUIP(domain) ;
        emplandto = emplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandto);
    }
    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandto),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组计划模板保存计划", tags = {"计划" },  notes = "根据班组计划模板保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/save")
    public ResponseEntity<EMPlanDTO> saveByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanDTO emplandto) {
        EMPlan domain = emplanMapping.toDomain(emplandto);
        domain.setPlantemplid(emplantempl_id);
        emplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanMapping.toDomain(#emplandtos),'eam-EMPlan-Save')")
    @ApiOperation(value = "根据班组计划模板批量保存计划", tags = {"计划" },  notes = "根据班组计划模板批量保存计划")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody List<EMPlanDTO> emplandtos) {
        List<EMPlan> domainlist=emplanMapping.toDomain(emplandtos);
        for(EMPlan domain:domainlist){
             domain.setPlantemplid(emplantempl_id);
        }
        emplanService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组计划模板获取DEFAULT", tags = {"计划" } ,notes = "根据班组计划模板获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/fetchdefault")
	public ResponseEntity<List<EMPlanDTO>> fetchEMPlanDefaultByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id,EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
        List<EMPlanDTO> list = emplanMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlan-searchDefault-all') and hasPermission(#context,'eam-EMPlan-Get')")
	@ApiOperation(value = "根据班组计划模板查询DEFAULT", tags = {"计划" } ,notes = "根据班组计划模板查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/searchdefault")
	public ResponseEntity<Page<EMPlanDTO>> searchEMPlanDefaultByPFTeamEMPlanTempl(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanSearchContext context) {
        context.setN_plantemplid_eq(emplantempl_id);
        Page<EMPlan> domains = emplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

