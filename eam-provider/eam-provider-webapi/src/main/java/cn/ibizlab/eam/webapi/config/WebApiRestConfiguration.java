package cn.ibizlab.eam.webapi.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.eam.webapi")
public class WebApiRestConfiguration {

}
