package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFOAC;
import cn.ibizlab.eam.core.eam_core.service.IEMRFOACService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOACSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"方案" })
@RestController("WebApi-emrfoac")
@RequestMapping("")
public class EMRFOACResource {

    @Autowired
    public IEMRFOACService emrfoacService;

    @Autowired
    @Lazy
    public EMRFOACMapping emrfoacMapping;

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "新建方案", tags = {"方案" },  notes = "新建方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfoacs")
    public ResponseEntity<EMRFOACDTO> create(@Validated @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
		emrfoacService.create(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "批量新建方案", tags = {"方案" },  notes = "批量新建方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfoacs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFOACDTO> emrfoacdtos) {
        emrfoacService.createBatch(emrfoacMapping.toDomain(emrfoacdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoac" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "更新方案", tags = {"方案" },  notes = "更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> update(@PathVariable("emrfoac_id") String emrfoac_id, @RequestBody EMRFOACDTO emrfoacdto) {
		EMRFOAC domain  = emrfoacMapping.toDomain(emrfoacdto);
        domain .setEmrfoacid(emrfoac_id);
		emrfoacService.update(domain );
		EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByEntities(this.emrfoacMapping.toDomain(#emrfoacdtos)),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "批量更新方案", tags = {"方案" },  notes = "批量更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfoacs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFOACDTO> emrfoacdtos) {
        emrfoacService.updateBatch(emrfoacMapping.toDomain(emrfoacdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "删除方案", tags = {"方案" },  notes = "删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfoacs/{emrfoac_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfoac_id") String emrfoac_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfoacService.remove(emrfoac_id));
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByIds(#ids),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "批量删除方案", tags = {"方案" },  notes = "批量删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfoacs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfoacService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfoacMapping.toDomain(returnObject.body),'eam-EMRFOAC-Get')")
    @ApiOperation(value = "获取方案", tags = {"方案" },  notes = "获取方案")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> get(@PathVariable("emrfoac_id") String emrfoac_id) {
        EMRFOAC domain = emrfoacService.get(emrfoac_id);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取方案草稿", tags = {"方案" },  notes = "获取方案草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfoacs/getdraft")
    public ResponseEntity<EMRFOACDTO> getDraft(EMRFOACDTO dto) {
        EMRFOAC domain = emrfoacMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(emrfoacService.getDraft(domain)));
    }

    @ApiOperation(value = "检查方案", tags = {"方案" },  notes = "检查方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfoacs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFOACDTO emrfoacdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfoacService.checkKey(emrfoacMapping.toDomain(emrfoacdto)));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "保存方案", tags = {"方案" },  notes = "保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfoacs/save")
    public ResponseEntity<EMRFOACDTO> save(@RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        emrfoacService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "批量保存方案", tags = {"方案" },  notes = "批量保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfoacs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFOACDTO> emrfoacdtos) {
        emrfoacService.saveBatch(emrfoacMapping.toDomain(emrfoacdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"方案" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfoacs/fetchdefault")
	public ResponseEntity<List<EMRFOACDTO>> fetchDefault(EMRFOACSearchContext context) {
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
        List<EMRFOACDTO> list = emrfoacMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"方案" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfoacs/searchdefault")
	public ResponseEntity<Page<EMRFOACDTO>> searchDefault(@RequestBody EMRFOACSearchContext context) {
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfoacMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据现象建立方案", tags = {"方案" },  notes = "根据现象建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfoacs")
    public ResponseEntity<EMRFOACDTO> createByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfodeid(emrfode_id);
		emrfoacService.create(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据现象批量建立方案", tags = {"方案" },  notes = "根据现象批量建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfoacService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoac" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据现象更新方案", tags = {"方案" },  notes = "根据现象更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> updateByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoac_id") String emrfoac_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfodeid(emrfode_id);
        domain.setEmrfoacid(emrfoac_id);
		emrfoacService.update(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByEntities(this.emrfoacMapping.toDomain(#emrfoacdtos)),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据现象批量更新方案", tags = {"方案" },  notes = "根据现象批量更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfoacService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据现象删除方案", tags = {"方案" },  notes = "根据现象删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<Boolean> removeByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoac_id") String emrfoac_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfoacService.remove(emrfoac_id));
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByIds(#ids),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据现象批量删除方案", tags = {"方案" },  notes = "根据现象批量删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODE(@RequestBody List<String> ids) {
        emrfoacService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfoacMapping.toDomain(returnObject.body),'eam-EMRFOAC-Get')")
    @ApiOperation(value = "根据现象获取方案", tags = {"方案" },  notes = "根据现象获取方案")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> getByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoac_id") String emrfoac_id) {
        EMRFOAC domain = emrfoacService.get(emrfoac_id);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象获取方案草稿", tags = {"方案" },  notes = "根据现象获取方案草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfoacs/getdraft")
    public ResponseEntity<EMRFOACDTO> getDraftByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, EMRFOACDTO dto) {
        EMRFOAC domain = emrfoacMapping.toDomain(dto);
        domain.setRfodeid(emrfode_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(emrfoacService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象检查方案", tags = {"方案" },  notes = "根据现象检查方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfoacs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOACDTO emrfoacdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfoacService.checkKey(emrfoacMapping.toDomain(emrfoacdto)));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据现象保存方案", tags = {"方案" },  notes = "根据现象保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfoacs/save")
    public ResponseEntity<EMRFOACDTO> saveByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfodeid(emrfode_id);
        emrfoacService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据现象批量保存方案", tags = {"方案" },  notes = "根据现象批量保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfoacs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
             domain.setRfodeid(emrfode_id);
        }
        emrfoacService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据现象获取DEFAULT", tags = {"方案" } ,notes = "根据现象获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfoacs/fetchdefault")
	public ResponseEntity<List<EMRFOACDTO>> fetchEMRFOACDefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id,EMRFOACSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
        List<EMRFOACDTO> list = emrfoacMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据现象查询DEFAULT", tags = {"方案" } ,notes = "根据现象查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfoacs/searchdefault")
	public ResponseEntity<Page<EMRFOACDTO>> searchEMRFOACDefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOACSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfoacMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据模式建立方案", tags = {"方案" },  notes = "根据模式建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfoacs")
    public ResponseEntity<EMRFOACDTO> createByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
		emrfoacService.create(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据模式批量建立方案", tags = {"方案" },  notes = "根据模式批量建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> createBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoac" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据模式更新方案", tags = {"方案" },  notes = "根据模式更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> updateByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
        domain.setEmrfoacid(emrfoac_id);
		emrfoacService.update(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByEntities(this.emrfoacMapping.toDomain(#emrfoacdtos)),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据模式批量更新方案", tags = {"方案" },  notes = "根据模式批量更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据模式删除方案", tags = {"方案" },  notes = "根据模式删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<Boolean> removeByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfoacService.remove(emrfoac_id));
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByIds(#ids),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据模式批量删除方案", tags = {"方案" },  notes = "根据模式批量删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFOMO(@RequestBody List<String> ids) {
        emrfoacService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfoacMapping.toDomain(returnObject.body),'eam-EMRFOAC-Get')")
    @ApiOperation(value = "根据模式获取方案", tags = {"方案" },  notes = "根据模式获取方案")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> getByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id) {
        EMRFOAC domain = emrfoacService.get(emrfoac_id);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据模式获取方案草稿", tags = {"方案" },  notes = "根据模式获取方案草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfomos/{emrfomo_id}/emrfoacs/getdraft")
    public ResponseEntity<EMRFOACDTO> getDraftByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, EMRFOACDTO dto) {
        EMRFOAC domain = emrfoacMapping.toDomain(dto);
        domain.setRfomoid(emrfomo_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(emrfoacService.getDraft(domain)));
    }

    @ApiOperation(value = "根据模式检查方案", tags = {"方案" },  notes = "根据模式检查方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfoacs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfoacService.checkKey(emrfoacMapping.toDomain(emrfoacdto)));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据模式保存方案", tags = {"方案" },  notes = "根据模式保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfoacs/save")
    public ResponseEntity<EMRFOACDTO> saveByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
        emrfoacService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据模式批量保存方案", tags = {"方案" },  notes = "根据模式批量保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfoacs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
             domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据模式获取DEFAULT", tags = {"方案" } ,notes = "根据模式获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfomos/{emrfomo_id}/emrfoacs/fetchdefault")
	public ResponseEntity<List<EMRFOACDTO>> fetchEMRFOACDefaultByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id,EMRFOACSearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
        List<EMRFOACDTO> list = emrfoacMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据模式查询DEFAULT", tags = {"方案" } ,notes = "根据模式查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfomos/{emrfomo_id}/emrfoacs/searchdefault")
	public ResponseEntity<Page<EMRFOACDTO>> searchEMRFOACDefaultByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACSearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfoacMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据现象模式建立方案", tags = {"方案" },  notes = "根据现象模式建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs")
    public ResponseEntity<EMRFOACDTO> createByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
		emrfoacService.create(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Create')")
    @ApiOperation(value = "根据现象模式批量建立方案", tags = {"方案" },  notes = "根据现象模式批量建立方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoac" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据现象模式更新方案", tags = {"方案" },  notes = "根据现象模式更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> updateByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
        domain.setEmrfoacid(emrfoac_id);
		emrfoacService.update(domain);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByEntities(this.emrfoacMapping.toDomain(#emrfoacdtos)),'eam-EMRFOAC-Update')")
    @ApiOperation(value = "根据现象模式批量更新方案", tags = {"方案" },  notes = "根据现象模式批量更新方案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfoacService.get(#emrfoac_id),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据现象模式删除方案", tags = {"方案" },  notes = "根据现象模式删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<Boolean> removeByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfoacService.remove(emrfoac_id));
    }

    @PreAuthorize("hasPermission(this.emrfoacService.getEmrfoacByIds(#ids),'eam-EMRFOAC-Remove')")
    @ApiOperation(value = "根据现象模式批量删除方案", tags = {"方案" },  notes = "根据现象模式批量删除方案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODEEMRFOMO(@RequestBody List<String> ids) {
        emrfoacService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfoacMapping.toDomain(returnObject.body),'eam-EMRFOAC-Get')")
    @ApiOperation(value = "根据现象模式获取方案", tags = {"方案" },  notes = "根据现象模式获取方案")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/{emrfoac_id}")
    public ResponseEntity<EMRFOACDTO> getByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoac_id") String emrfoac_id) {
        EMRFOAC domain = emrfoacService.get(emrfoac_id);
        EMRFOACDTO dto = emrfoacMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象模式获取方案草稿", tags = {"方案" },  notes = "根据现象模式获取方案草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/getdraft")
    public ResponseEntity<EMRFOACDTO> getDraftByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, EMRFOACDTO dto) {
        EMRFOAC domain = emrfoacMapping.toDomain(dto);
        domain.setRfomoid(emrfomo_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(emrfoacService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象模式检查方案", tags = {"方案" },  notes = "根据现象模式检查方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfoacService.checkKey(emrfoacMapping.toDomain(emrfoacdto)));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdto),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据现象模式保存方案", tags = {"方案" },  notes = "根据现象模式保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/save")
    public ResponseEntity<EMRFOACDTO> saveByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACDTO emrfoacdto) {
        EMRFOAC domain = emrfoacMapping.toDomain(emrfoacdto);
        domain.setRfomoid(emrfomo_id);
        emrfoacService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfoacMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfoacMapping.toDomain(#emrfoacdtos),'eam-EMRFOAC-Save')")
    @ApiOperation(value = "根据现象模式批量保存方案", tags = {"方案" },  notes = "根据现象模式批量保存方案")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOACDTO> emrfoacdtos) {
        List<EMRFOAC> domainlist=emrfoacMapping.toDomain(emrfoacdtos);
        for(EMRFOAC domain:domainlist){
             domain.setRfomoid(emrfomo_id);
        }
        emrfoacService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据现象模式获取DEFAULT", tags = {"方案" } ,notes = "根据现象模式获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/fetchdefault")
	public ResponseEntity<List<EMRFOACDTO>> fetchEMRFOACDefaultByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id,EMRFOACSearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
        List<EMRFOACDTO> list = emrfoacMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOAC-searchDefault-all') and hasPermission(#context,'eam-EMRFOAC-Get')")
	@ApiOperation(value = "根据现象模式查询DEFAULT", tags = {"方案" } ,notes = "根据现象模式查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfoacs/searchdefault")
	public ResponseEntity<Page<EMRFOACDTO>> searchEMRFOACDefaultByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOACSearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOAC> domains = emrfoacService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfoacMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

