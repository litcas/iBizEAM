package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMResEmp;
import cn.ibizlab.eam.core.eam_core.service.IEMResEmpService;
import cn.ibizlab.eam.core.eam_core.filter.EMResEmpSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"员工资源" })
@RestController("WebApi-emresemp")
@RequestMapping("")
public class EMResEmpResource {

    @Autowired
    public IEMResEmpService emresempService;

    @Autowired
    @Lazy
    public EMResEmpMapping emresempMapping;

    @PreAuthorize("hasPermission(this.emresempMapping.toDomain(#emresempdto),'eam-EMResEmp-Create')")
    @ApiOperation(value = "新建员工资源", tags = {"员工资源" },  notes = "新建员工资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresemps")
    public ResponseEntity<EMResEmpDTO> create(@Validated @RequestBody EMResEmpDTO emresempdto) {
        EMResEmp domain = emresempMapping.toDomain(emresempdto);
		emresempService.create(domain);
        EMResEmpDTO dto = emresempMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresempMapping.toDomain(#emresempdtos),'eam-EMResEmp-Create')")
    @ApiOperation(value = "批量新建员工资源", tags = {"员工资源" },  notes = "批量新建员工资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresemps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMResEmpDTO> emresempdtos) {
        emresempService.createBatch(emresempMapping.toDomain(emresempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emresemp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emresempService.get(#emresemp_id),'eam-EMResEmp-Update')")
    @ApiOperation(value = "更新员工资源", tags = {"员工资源" },  notes = "更新员工资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresemps/{emresemp_id}")
    public ResponseEntity<EMResEmpDTO> update(@PathVariable("emresemp_id") String emresemp_id, @RequestBody EMResEmpDTO emresempdto) {
		EMResEmp domain  = emresempMapping.toDomain(emresempdto);
        domain .setEmresempid(emresemp_id);
		emresempService.update(domain );
		EMResEmpDTO dto = emresempMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresempService.getEmresempByEntities(this.emresempMapping.toDomain(#emresempdtos)),'eam-EMResEmp-Update')")
    @ApiOperation(value = "批量更新员工资源", tags = {"员工资源" },  notes = "批量更新员工资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresemps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMResEmpDTO> emresempdtos) {
        emresempService.updateBatch(emresempMapping.toDomain(emresempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emresempService.get(#emresemp_id),'eam-EMResEmp-Remove')")
    @ApiOperation(value = "删除员工资源", tags = {"员工资源" },  notes = "删除员工资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresemps/{emresemp_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emresemp_id") String emresemp_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emresempService.remove(emresemp_id));
    }

    @PreAuthorize("hasPermission(this.emresempService.getEmresempByIds(#ids),'eam-EMResEmp-Remove')")
    @ApiOperation(value = "批量删除员工资源", tags = {"员工资源" },  notes = "批量删除员工资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresemps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emresempService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emresempMapping.toDomain(returnObject.body),'eam-EMResEmp-Get')")
    @ApiOperation(value = "获取员工资源", tags = {"员工资源" },  notes = "获取员工资源")
	@RequestMapping(method = RequestMethod.GET, value = "/emresemps/{emresemp_id}")
    public ResponseEntity<EMResEmpDTO> get(@PathVariable("emresemp_id") String emresemp_id) {
        EMResEmp domain = emresempService.get(emresemp_id);
        EMResEmpDTO dto = emresempMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取员工资源草稿", tags = {"员工资源" },  notes = "获取员工资源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emresemps/getdraft")
    public ResponseEntity<EMResEmpDTO> getDraft(EMResEmpDTO dto) {
        EMResEmp domain = emresempMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emresempMapping.toDto(emresempService.getDraft(domain)));
    }

    @ApiOperation(value = "检查员工资源", tags = {"员工资源" },  notes = "检查员工资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresemps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMResEmpDTO emresempdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emresempService.checkKey(emresempMapping.toDomain(emresempdto)));
    }

    @PreAuthorize("hasPermission(this.emresempMapping.toDomain(#emresempdto),'eam-EMResEmp-Save')")
    @ApiOperation(value = "保存员工资源", tags = {"员工资源" },  notes = "保存员工资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresemps/save")
    public ResponseEntity<EMResEmpDTO> save(@RequestBody EMResEmpDTO emresempdto) {
        EMResEmp domain = emresempMapping.toDomain(emresempdto);
        emresempService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emresempMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emresempMapping.toDomain(#emresempdtos),'eam-EMResEmp-Save')")
    @ApiOperation(value = "批量保存员工资源", tags = {"员工资源" },  notes = "批量保存员工资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresemps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMResEmpDTO> emresempdtos) {
        emresempService.saveBatch(emresempMapping.toDomain(emresempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResEmp-searchDefault-all') and hasPermission(#context,'eam-EMResEmp-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"员工资源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emresemps/fetchdefault")
	public ResponseEntity<List<EMResEmpDTO>> fetchDefault(EMResEmpSearchContext context) {
        Page<EMResEmp> domains = emresempService.searchDefault(context) ;
        List<EMResEmpDTO> list = emresempMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResEmp-searchDefault-all') and hasPermission(#context,'eam-EMResEmp-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"员工资源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emresemps/searchdefault")
	public ResponseEntity<Page<EMResEmpDTO>> searchDefault(@RequestBody EMResEmpSearchContext context) {
        Page<EMResEmp> domains = emresempService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresempMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

