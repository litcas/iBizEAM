package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import cn.ibizlab.eam.webapi.dto.EMWPListCostDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMWPListCostMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMWPListCostMapping extends MappingBase<EMWPListCostDTO, EMWPListCost> {


}

