package cn.ibizlab.eam.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ibizlab.eam.util.domain.IBZUSER;

public interface IBZUSERMapper extends BaseMapper<IBZUSER>{

}