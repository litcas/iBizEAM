package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
/**
 * 关系型数据实体[EMItemType] 查询条件对象
 */
@Slf4j
@Data
public class EMItemTypeSearchContext extends QueryWrapperContext<EMItemType> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emitemtypename_like;//[物品类型名称]
	public void setN_emitemtypename_like(String n_emitemtypename_like) {
        this.n_emitemtypename_like = n_emitemtypename_like;
        if(!ObjectUtils.isEmpty(this.n_emitemtypename_like)){
            this.getSearchCond().like("emitemtypename", n_emitemtypename_like);
        }
    }
	private String n_itemtypeinfo_like;//[物品类型信息]
	public void setN_itemtypeinfo_like(String n_itemtypeinfo_like) {
        this.n_itemtypeinfo_like = n_itemtypeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_itemtypeinfo_like)){
            this.getSearchCond().like("itemtypeinfo", n_itemtypeinfo_like);
        }
    }
	private String n_itembtypename_eq;//[一级类]
	public void setN_itembtypename_eq(String n_itembtypename_eq) {
        this.n_itembtypename_eq = n_itembtypename_eq;
        if(!ObjectUtils.isEmpty(this.n_itembtypename_eq)){
            this.getSearchCond().eq("itembtypename", n_itembtypename_eq);
        }
    }
	private String n_itembtypename_like;//[一级类]
	public void setN_itembtypename_like(String n_itembtypename_like) {
        this.n_itembtypename_like = n_itembtypename_like;
        if(!ObjectUtils.isEmpty(this.n_itembtypename_like)){
            this.getSearchCond().like("itembtypename", n_itembtypename_like);
        }
    }
	private String n_itemmtypename_eq;//[二级类]
	public void setN_itemmtypename_eq(String n_itemmtypename_eq) {
        this.n_itemmtypename_eq = n_itemmtypename_eq;
        if(!ObjectUtils.isEmpty(this.n_itemmtypename_eq)){
            this.getSearchCond().eq("itemmtypename", n_itemmtypename_eq);
        }
    }
	private String n_itemmtypename_like;//[二级类]
	public void setN_itemmtypename_like(String n_itemmtypename_like) {
        this.n_itemmtypename_like = n_itemmtypename_like;
        if(!ObjectUtils.isEmpty(this.n_itemmtypename_like)){
            this.getSearchCond().like("itemmtypename", n_itemmtypename_like);
        }
    }
	private String n_itemtypepname_eq;//[上级类型]
	public void setN_itemtypepname_eq(String n_itemtypepname_eq) {
        this.n_itemtypepname_eq = n_itemtypepname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypepname_eq)){
            this.getSearchCond().eq("itemtypepname", n_itemtypepname_eq);
        }
    }
	private String n_itemtypepname_like;//[上级类型]
	public void setN_itemtypepname_like(String n_itemtypepname_like) {
        this.n_itemtypepname_like = n_itemtypepname_like;
        if(!ObjectUtils.isEmpty(this.n_itemtypepname_like)){
            this.getSearchCond().like("itemtypepname", n_itemtypepname_like);
        }
    }
	private String n_itemtypepid_eq;//[上级类型]
	public void setN_itemtypepid_eq(String n_itemtypepid_eq) {
        this.n_itemtypepid_eq = n_itemtypepid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypepid_eq)){
            this.getSearchCond().eq("itemtypepid", n_itemtypepid_eq);
        }
    }
	private String n_itemmtypeid_eq;//[二级类]
	public void setN_itemmtypeid_eq(String n_itemmtypeid_eq) {
        this.n_itemmtypeid_eq = n_itemmtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemmtypeid_eq)){
            this.getSearchCond().eq("itemmtypeid", n_itemmtypeid_eq);
        }
    }
	private String n_itembtypeid_eq;//[一级类]
	public void setN_itembtypeid_eq(String n_itembtypeid_eq) {
        this.n_itembtypeid_eq = n_itembtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_itembtypeid_eq)){
            this.getSearchCond().eq("itembtypeid", n_itembtypeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emitemtypename", query)
            );
		 }
	}
}



