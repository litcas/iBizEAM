package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPRCD;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPRCDSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPRCDService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQKPRCDMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[关键点记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEQKPRCDServiceImpl")
public class EMEQKPRCDServiceImpl extends ServiceImpl<EMEQKPRCDMapper, EMEQKPRCD> implements IEMEQKPRCDService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKPService emeqkpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQKPRCD et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqkprcdid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQKPRCD> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQKPRCD et) {
        fillParentData(et);
        emdprctService.update(emeqkprcdInheritMapping.toEmdprct(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqkprcdid", et.getEmeqkprcdid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqkprcdid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQKPRCD> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQKPRCD get(String key) {
        EMEQKPRCD et = getById(key);
        if(et == null){
            et = new EMEQKPRCD();
            et.setEmeqkprcdid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQKPRCD getDraft(EMEQKPRCD et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQKPRCD et) {
        return (!ObjectUtils.isEmpty(et.getEmeqkprcdid())) && (!Objects.isNull(this.getById(et.getEmeqkprcdid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQKPRCD et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQKPRCD et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQKPRCD> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQKPRCD> create = new ArrayList<>();
        List<EMEQKPRCD> update = new ArrayList<>();
        for (EMEQKPRCD et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqkprcdid()) || ObjectUtils.isEmpty(getById(et.getEmeqkprcdid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQKPRCD> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQKPRCD> create = new ArrayList<>();
        List<EMEQKPRCD> update = new ArrayList<>();
        for (EMEQKPRCD et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqkprcdid()) || ObjectUtils.isEmpty(getById(et.getEmeqkprcdid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQKPRCD> selectByKpid(String emeqkpid) {
        return baseMapper.selectByKpid(emeqkpid);
    }
    @Override
    public void removeByKpid(String emeqkpid) {
        this.remove(new QueryWrapper<EMEQKPRCD>().eq("kpid",emeqkpid));
    }

	@Override
    public List<EMEQKPRCD> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQKPRCD>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQKPRCD> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQKPRCD>().eq("objid",emobjectid));
    }

	@Override
    public List<EMEQKPRCD> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }
    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQKPRCD>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQKPRCD> searchDefault(EMEQKPRCDSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQKPRCD> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQKPRCD>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQKPRCD et){
        //实体关系[DER1N_EMEQKPRCD_EMEQKP_KPID]
        if(!ObjectUtils.isEmpty(et.getKpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQKP kp=et.getKp();
            if(ObjectUtils.isEmpty(kp)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQKP majorEntity=emeqkpService.get(et.getKpid());
                et.setKp(majorEntity);
                kp=majorEntity;
            }
            et.setKpname(kp.getEmeqkpname());
        }
        //实体关系[DER1N_EMEQKPRCD_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQKPRCD_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMEQKPRCD_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQKPRCDInheritMapping emeqkprcdInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQKPRCD et){
        if(ObjectUtils.isEmpty(et.getEmeqkprcdid()))
            et.setEmeqkprcdid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emeqkprcdInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","EQKPRCT");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQKPRCD> getEmeqkprcdByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQKPRCD> getEmeqkprcdByEntities(List<EMEQKPRCD> entities) {
        List ids =new ArrayList();
        for(EMEQKPRCD entity : entities){
            Serializable id=entity.getEmeqkprcdid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQKPRCDService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



