package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClass;
/**
 * 关系型数据实体[EMAssetClass] 查询条件对象
 */
@Slf4j
@Data
public class EMAssetClassSearchContext extends QueryWrapperContext<EMAssetClass> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emassetclassname_like;//[资产类别名称]
	public void setN_emassetclassname_like(String n_emassetclassname_like) {
        this.n_emassetclassname_like = n_emassetclassname_like;
        if(!ObjectUtils.isEmpty(this.n_emassetclassname_like)){
            this.getSearchCond().like("emassetclassname", n_emassetclassname_like);
        }
    }
	private String n_assetclassgroup_eq;//[固定资产科目分类]
	public void setN_assetclassgroup_eq(String n_assetclassgroup_eq) {
        this.n_assetclassgroup_eq = n_assetclassgroup_eq;
        if(!ObjectUtils.isEmpty(this.n_assetclassgroup_eq)){
            this.getSearchCond().eq("assetclassgroup", n_assetclassgroup_eq);
        }
    }
	private String n_assetclassinfo_like;//[资产科目信息]
	public void setN_assetclassinfo_like(String n_assetclassinfo_like) {
        this.n_assetclassinfo_like = n_assetclassinfo_like;
        if(!ObjectUtils.isEmpty(this.n_assetclassinfo_like)){
            this.getSearchCond().like("assetclassinfo", n_assetclassinfo_like);
        }
    }
	private String n_assetclasspname_eq;//[上级科目]
	public void setN_assetclasspname_eq(String n_assetclasspname_eq) {
        this.n_assetclasspname_eq = n_assetclasspname_eq;
        if(!ObjectUtils.isEmpty(this.n_assetclasspname_eq)){
            this.getSearchCond().eq("assetclasspname", n_assetclasspname_eq);
        }
    }
	private String n_assetclasspname_like;//[上级科目]
	public void setN_assetclasspname_like(String n_assetclasspname_like) {
        this.n_assetclasspname_like = n_assetclasspname_like;
        if(!ObjectUtils.isEmpty(this.n_assetclasspname_like)){
            this.getSearchCond().like("assetclasspname", n_assetclasspname_like);
        }
    }
	private String n_assetclasspid_eq;//[上级科目编号]
	public void setN_assetclasspid_eq(String n_assetclasspid_eq) {
        this.n_assetclasspid_eq = n_assetclasspid_eq;
        if(!ObjectUtils.isEmpty(this.n_assetclasspid_eq)){
            this.getSearchCond().eq("assetclasspid", n_assetclasspid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassetclassname", query)
            );
		 }
	}
}



