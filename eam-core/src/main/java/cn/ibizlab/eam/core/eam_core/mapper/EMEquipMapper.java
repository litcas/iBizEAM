package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
import cn.ibizlab.eam.core.eam_core.filter.EMEquipSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEquipMapper extends BaseMapper<EMEquip> {

    Page<EMEquip> searchDefault(IPage page, @Param("srf") EMEquipSearchContext context, @Param("ew") Wrapper<EMEquip> wrapper);
    Page<EMEquip> searchEQTypeTree(IPage page, @Param("srf") EMEquipSearchContext context, @Param("ew") Wrapper<EMEquip> wrapper);
    Page<Map> searchTypeEQNum(IPage page, @Param("srf") EMEquipSearchContext context, @Param("ew") Wrapper<EMEquip> wrapper);
    @Override
    EMEquip selectById(Serializable id);
    @Override
    int insert(EMEquip entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEquip entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEquip entity, @Param("ew") Wrapper<EMEquip> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEquip> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMEquip> selectByAssetid(@Param("emassetid") Serializable emassetid);

    List<EMEquip> selectByEmberthid(@Param("emberthid") Serializable emberthid);

    List<EMEquip> selectByEmbrandid(@Param("embrandid") Serializable embrandid);

    List<EMEquip> selectByEqlocationid(@Param("emeqlocationid") Serializable emeqlocationid);

    List<EMEquip> selectByEqtypeid(@Param("emeqtypeid") Serializable emeqtypeid);

    List<EMEquip> selectByEquippid(@Param("emequipid") Serializable emequipid);

    List<EMEquip> selectByEmmachinecategoryid(@Param("emmachinecategoryid") Serializable emmachinecategoryid);

    List<EMEquip> selectByEmmachmodelid(@Param("emmachmodelid") Serializable emmachmodelid);

    List<EMEquip> selectByLabserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEquip> selectByMserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEquip> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEquip> selectByContractid(@Param("pfcontractid") Serializable pfcontractid);

    List<EMEquip> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
