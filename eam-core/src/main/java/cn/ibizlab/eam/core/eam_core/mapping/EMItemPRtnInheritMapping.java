

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemPRtnInheritMapping {

    @Mappings({
        @Mapping(source ="emitemprtnid",target = "emitemtradeid"),
        @Mapping(source ="itemname",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemTrade toEmitemtrade(EMItemPRtn minorEntity);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitemprtnid"),
        @Mapping(source ="emitemtradename" ,target = "itemname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemPRtn toEmitemprtn(EMItemTrade majorEntity);

    List<EMItemTrade> toEmitemtrade(List<EMItemPRtn> minorEntities);

    List<EMItemPRtn> toEmitemprtn(List<EMItemTrade> majorEntities);

}


