package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
/**
 * 关系型数据实体[EMEQLCTMap] 查询条件对象
 */
@Slf4j
@Data
public class EMEQLCTMapSearchContext extends QueryWrapperContext<EMEQLCTMap> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emeqlctmapname_like;//[位置关系]
	public void setN_emeqlctmapname_like(String n_emeqlctmapname_like) {
        this.n_emeqlctmapname_like = n_emeqlctmapname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqlctmapname_like)){
            this.getSearchCond().like("emeqlctmapname", n_emeqlctmapname_like);
        }
    }
	private String n_eqlocationpname_eq;//[上级位置]
	public void setN_eqlocationpname_eq(String n_eqlocationpname_eq) {
        this.n_eqlocationpname_eq = n_eqlocationpname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationpname_eq)){
            this.getSearchCond().eq("eqlocationpname", n_eqlocationpname_eq);
        }
    }
	private String n_eqlocationpname_like;//[上级位置]
	public void setN_eqlocationpname_like(String n_eqlocationpname_like) {
        this.n_eqlocationpname_like = n_eqlocationpname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationpname_like)){
            this.getSearchCond().like("eqlocationpname", n_eqlocationpname_like);
        }
    }
	private String n_eqlocationname_eq;//[位置]
	public void setN_eqlocationname_eq(String n_eqlocationname_eq) {
        this.n_eqlocationname_eq = n_eqlocationname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_eq)){
            this.getSearchCond().eq("eqlocationname", n_eqlocationname_eq);
        }
    }
	private String n_eqlocationname_like;//[位置]
	public void setN_eqlocationname_like(String n_eqlocationname_like) {
        this.n_eqlocationname_like = n_eqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_like)){
            this.getSearchCond().like("eqlocationname", n_eqlocationname_like);
        }
    }
	private String n_eqlocationid_eq;//[位置]
	public void setN_eqlocationid_eq(String n_eqlocationid_eq) {
        this.n_eqlocationid_eq = n_eqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationid_eq)){
            this.getSearchCond().eq("eqlocationid", n_eqlocationid_eq);
        }
    }
	private String n_eqlocationpid_eq;//[上级位置]
	public void setN_eqlocationpid_eq(String n_eqlocationpid_eq) {
        this.n_eqlocationpid_eq = n_eqlocationpid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationpid_eq)){
            this.getSearchCond().eq("eqlocationpid", n_eqlocationpid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqlctmapname", query)
            );
		 }
	}
}



