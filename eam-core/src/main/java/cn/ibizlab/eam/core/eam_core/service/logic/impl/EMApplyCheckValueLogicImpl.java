package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMApplyCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMApply;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMApplyCheckValueLogicImpl implements IEMApplyCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMApplyService emapplyservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMApplyService getEmapplyService() {
        return this.emapplyservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMApplyService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMApplyService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMApply et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emapplycheckvaluedefault", et);
            kieSession.setGlobal("emapplyservice", emapplyservice);
            kieSession.setGlobal("iBzSysEmapplyDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emapplycheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[新建时检查值]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
