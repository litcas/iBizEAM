package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySetupSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEIBatterySetup] 服务对象接口
 */
public interface IEMEIBatterySetupService extends IService<EMEIBatterySetup> {

    boolean create(EMEIBatterySetup et);
    void createBatch(List<EMEIBatterySetup> list);
    boolean update(EMEIBatterySetup et);
    void updateBatch(List<EMEIBatterySetup> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEIBatterySetup get(String key);
    EMEIBatterySetup getDraft(EMEIBatterySetup et);
    boolean checkKey(EMEIBatterySetup et);
    boolean save(EMEIBatterySetup et);
    void saveBatch(List<EMEIBatterySetup> list);
    Page<EMEIBatterySetup> searchDefault(EMEIBatterySetupSearchContext context);
    List<EMEIBatterySetup> selectByEiobjid(String emeibatteryid);
    void removeByEiobjid(String emeibatteryid);
    List<EMEIBatterySetup> selectByEiobjnid(String emeibatteryid);
    void removeByEiobjnid(String emeibatteryid);
    List<EMEIBatterySetup> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEIBatterySetup> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEIBatterySetup> getEmeibatterysetupByIds(List<String> ids);
    List<EMEIBatterySetup> getEmeibatterysetupByEntities(List<EMEIBatterySetup> entities);
}


