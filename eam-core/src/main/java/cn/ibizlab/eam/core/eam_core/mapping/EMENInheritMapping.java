

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEN;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMENInheritMapping {

    @Mappings({
        @Mapping(source ="emenid",target = "emobjectid"),
        @Mapping(source ="emenname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="energycode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEN minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emenid"),
        @Mapping(source ="emobjectname" ,target = "emenname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "energycode"),
    })
    EMEN toEmen(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEN> minorEntities);

    List<EMEN> toEmen(List<EMObject> majorEntities);

}


