package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPurPlan;
import cn.ibizlab.eam.core.eam_core.filter.EMPurPlanSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPurPlanService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPurPlanMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划修理] 服务对象接口实现
 */
@Slf4j
@Service("EMPurPlanServiceImpl")
public class EMPurPlanServiceImpl extends ServiceImpl<EMPurPlanMapper, EMPurPlan> implements IEMPurPlanService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMBidinquiryService embidinquiryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMProjectTrackService emprojecttrackService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTypeService emitemtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFUnitService pfunitService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPurPlan et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpurplanid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPurPlan> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPurPlan et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("empurplanid", et.getEmpurplanid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpurplanid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPurPlan> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPurPlan get(String key) {
        EMPurPlan et = getById(key);
        if(et == null){
            et = new EMPurPlan();
            et.setEmpurplanid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPurPlan getDraft(EMPurPlan et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPurPlan et) {
        return (!ObjectUtils.isEmpty(et.getEmpurplanid())) && (!Objects.isNull(this.getById(et.getEmpurplanid())));
    }
    @Override
    @Transactional
    public boolean save(EMPurPlan et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPurPlan et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPurPlan> list) {
        list.forEach(item->fillParentData(item));
        List<EMPurPlan> create = new ArrayList<>();
        List<EMPurPlan> update = new ArrayList<>();
        for (EMPurPlan et : list) {
            if (ObjectUtils.isEmpty(et.getEmpurplanid()) || ObjectUtils.isEmpty(getById(et.getEmpurplanid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPurPlan> list) {
        list.forEach(item->fillParentData(item));
        List<EMPurPlan> create = new ArrayList<>();
        List<EMPurPlan> update = new ArrayList<>();
        for (EMPurPlan et : list) {
            if (ObjectUtils.isEmpty(et.getEmpurplanid()) || ObjectUtils.isEmpty(getById(et.getEmpurplanid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPurPlan> selectByEmbidinquiryid(String embidinquiryid) {
        return baseMapper.selectByEmbidinquiryid(embidinquiryid);
    }
    @Override
    public void removeByEmbidinquiryid(String embidinquiryid) {
        this.remove(new QueryWrapper<EMPurPlan>().eq("embidinquiryid",embidinquiryid));
    }

	@Override
    public List<EMPurPlan> selectByItemtypeid(String emitemtypeid) {
        return baseMapper.selectByItemtypeid(emitemtypeid);
    }
    @Override
    public void removeByItemtypeid(String emitemtypeid) {
        this.remove(new QueryWrapper<EMPurPlan>().eq("itemtypeid",emitemtypeid));
    }

	@Override
    public List<EMPurPlan> selectByUnitid(String pfunitid) {
        return baseMapper.selectByUnitid(pfunitid);
    }
    @Override
    public void removeByUnitid(String pfunitid) {
        this.remove(new QueryWrapper<EMPurPlan>().eq("unitid",pfunitid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPurPlan> searchDefault(EMPurPlanSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPurPlan> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPurPlan>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPurPlan et){
        //实体关系[DER1N_EMPURPLAN_EMBIDINQUIRY_EMBIDINQUIRYID]
        if(!ObjectUtils.isEmpty(et.getEmbidinquiryid())){
            cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry embidinquiry=et.getEmbidinquiry();
            if(ObjectUtils.isEmpty(embidinquiry)){
                cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry majorEntity=embidinquiryService.get(et.getEmbidinquiryid());
                et.setEmbidinquiry(majorEntity);
                embidinquiry=majorEntity;
            }
            et.setEmbidinquiryname(embidinquiry.getEmbidinquiryname());
        }
        //实体关系[DER1N_EMPURPLAN_EMITEMTYPE_ITEMTYPEID]
        if(!ObjectUtils.isEmpty(et.getItemtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemType itemtype=et.getItemtype();
            if(ObjectUtils.isEmpty(itemtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemType majorEntity=emitemtypeService.get(et.getItemtypeid());
                et.setItemtype(majorEntity);
                itemtype=majorEntity;
            }
            et.setItemtypename(itemtype.getItemtypeinfo());
        }
        //实体关系[DER1N_EMPURPLAN_PFUNIT_UNITID]
        if(!ObjectUtils.isEmpty(et.getUnitid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit=et.getUnit();
            if(ObjectUtils.isEmpty(unit)){
                cn.ibizlab.eam.core.eam_pf.domain.PFUnit majorEntity=pfunitService.get(et.getUnitid());
                et.setUnit(majorEntity);
                unit=majorEntity;
            }
            et.setUnitname(unit.getPfunitname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPurPlan> getEmpurplanByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPurPlan> getEmpurplanByEntities(List<EMPurPlan> entities) {
        List ids =new ArrayList();
        for(EMPurPlan entity : entities){
            Serializable id=entity.getEmpurplanid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPurPlanService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



