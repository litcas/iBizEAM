package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMBidding;
import cn.ibizlab.eam.core.eam_core.filter.EMBiddingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMBidding] 服务对象接口
 */
public interface IEMBiddingService extends IService<EMBidding> {

    boolean create(EMBidding et);
    void createBatch(List<EMBidding> list);
    boolean update(EMBidding et);
    void updateBatch(List<EMBidding> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMBidding get(String key);
    EMBidding getDraft(EMBidding et);
    boolean checkKey(EMBidding et);
    boolean save(EMBidding et);
    void saveBatch(List<EMBidding> list);
    Page<EMBidding> searchDefault(EMBiddingSearchContext context);
    List<EMBidding> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMBidding> getEmbiddingByIds(List<String> ids);
    List<EMBidding> getEmbiddingByEntities(List<EMBidding> entities);
}


