package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
import cn.ibizlab.eam.core.eam_core.filter.EMItemROutSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemROutMapper extends BaseMapper<EMItemROut> {

    Page<EMItemROut> searchConfirmed(IPage page, @Param("srf") EMItemROutSearchContext context, @Param("ew") Wrapper<EMItemROut> wrapper);
    Page<EMItemROut> searchDefault(IPage page, @Param("srf") EMItemROutSearchContext context, @Param("ew") Wrapper<EMItemROut> wrapper);
    Page<EMItemROut> searchDraft(IPage page, @Param("srf") EMItemROutSearchContext context, @Param("ew") Wrapper<EMItemROut> wrapper);
    Page<EMItemROut> searchToConfirm(IPage page, @Param("srf") EMItemROutSearchContext context, @Param("ew") Wrapper<EMItemROut> wrapper);
    @Override
    EMItemROut selectById(Serializable id);
    @Override
    int insert(EMItemROut entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemROut entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemROut entity, @Param("ew") Wrapper<EMItemROut> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemROut> selectByRid(@Param("emitemrinid") Serializable emitemrinid);

    List<EMItemROut> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemROut> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemROut> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemROut> selectBySempid(@Param("pfempid") Serializable pfempid);

}
