package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemFormUpdateByIdLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItem;

/**
 * 关系型数据实体[FormUpdateById] 对象
 */
@Slf4j
@Service
public class EMItemFormUpdateByIdLogicImpl implements IEMItemFormUpdateByIdLogic {

    @Autowired
    private KieContainer kieContainer;


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItem et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitemformupdatebyiddefault", et);
            kieSession.setGlobal("iBzSysEmitemDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitemformupdatebyid");

        } catch (Exception e) {
            throw new RuntimeException("执行[FormUpdateById]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
