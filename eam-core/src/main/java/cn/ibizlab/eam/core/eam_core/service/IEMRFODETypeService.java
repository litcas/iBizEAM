package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRFODEType;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODETypeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRFODEType] 服务对象接口
 */
public interface IEMRFODETypeService extends IService<EMRFODEType> {

    boolean create(EMRFODEType et);
    void createBatch(List<EMRFODEType> list);
    boolean update(EMRFODEType et);
    void updateBatch(List<EMRFODEType> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMRFODEType get(String key);
    EMRFODEType getDraft(EMRFODEType et);
    boolean checkKey(EMRFODEType et);
    boolean save(EMRFODEType et);
    void saveBatch(List<EMRFODEType> list);
    Page<EMRFODEType> searchDefault(EMRFODETypeSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRFODEType> getEmrfodetypeByIds(List<String> ids);
    List<EMRFODEType> getEmrfodetypeByEntities(List<EMRFODEType> entities);
}


