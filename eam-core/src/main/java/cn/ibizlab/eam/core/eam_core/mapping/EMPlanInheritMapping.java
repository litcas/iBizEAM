

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlan;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMPlanInheritMapping {

    @Mappings({
        @Mapping(source ="emplanid",target = "emresrefobjid"),
        @Mapping(source ="emplanname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMResRefObj toEmresrefobj(EMPlan minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emplanid"),
        @Mapping(source ="emresrefobjname" ,target = "emplanname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMPlan toEmplan(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMPlan> minorEntities);

    List<EMPlan> toEmplan(List<EMResRefObj> majorEntities);

}


