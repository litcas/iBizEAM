package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQMaintance;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMaintanceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQMaintance] 服务对象接口
 */
public interface IEMEQMaintanceService extends IService<EMEQMaintance> {

    boolean create(EMEQMaintance et);
    void createBatch(List<EMEQMaintance> list);
    boolean update(EMEQMaintance et);
    void updateBatch(List<EMEQMaintance> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQMaintance get(String key);
    EMEQMaintance getDraft(EMEQMaintance et);
    boolean checkKey(EMEQMaintance et);
    boolean save(EMEQMaintance et);
    void saveBatch(List<EMEQMaintance> list);
    Page<EMEQMaintance> searchCalendar(EMEQMaintanceSearchContext context);
    Page<EMEQMaintance> searchDefault(EMEQMaintanceSearchContext context);
    List<EMEQMaintance> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQMaintance> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQMaintance> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQMaintance> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQMaintance> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQMaintance> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQMaintance> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQMaintance> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQMaintance> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQMaintance> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQMaintance> getEmeqmaintanceByIds(List<String> ids);
    List<EMEQMaintance> getEmeqmaintanceByEntities(List<EMEQMaintance> entities);
}


