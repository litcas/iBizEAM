package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWOORI;
import cn.ibizlab.eam.core.eam_core.filter.EMWOORISearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWOORIService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWOORIMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[工单来源] 服务对象接口实现
 */
@Slf4j
@Service("EMWOORIServiceImpl")
public class EMWOORIServiceImpl extends ServiceImpl<EMWOORIMapper, EMWOORI> implements IEMWOORIService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService emwoPtService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWOORI et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwooriid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWOORI> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWOORI et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emwooriid", et.getEmwooriid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwooriid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWOORI> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWOORI get(String key) {
        EMWOORI et = getById(key);
        if(et == null){
            et = new EMWOORI();
            et.setEmwooriid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMWOORI getDraft(EMWOORI et) {
        return et;
    }

    @Override
    public boolean checkKey(EMWOORI et) {
        return (!ObjectUtils.isEmpty(et.getEmwooriid())) && (!Objects.isNull(this.getById(et.getEmwooriid())));
    }
    @Override
    @Transactional
    public boolean save(EMWOORI et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWOORI et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWOORI> list) {
        List<EMWOORI> create = new ArrayList<>();
        List<EMWOORI> update = new ArrayList<>();
        for (EMWOORI et : list) {
            if (ObjectUtils.isEmpty(et.getEmwooriid()) || ObjectUtils.isEmpty(getById(et.getEmwooriid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWOORI> list) {
        List<EMWOORI> create = new ArrayList<>();
        List<EMWOORI> update = new ArrayList<>();
        for (EMWOORI et : list) {
            if (ObjectUtils.isEmpty(et.getEmwooriid()) || ObjectUtils.isEmpty(getById(et.getEmwooriid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWOORI> searchDefault(EMWOORISearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWOORI> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWOORI>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 IndexDER
     */
    @Override
    public Page<EMWOORI> searchIndexDER(EMWOORISearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWOORI> pages=baseMapper.searchIndexDER(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWOORI>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWOORI> getEmwooriByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWOORI> getEmwooriByEntities(List<EMWOORI> entities) {
        List ids =new ArrayList();
        for(EMWOORI entity : entities){
            Serializable id=entity.getEmwooriid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMWOORIService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



