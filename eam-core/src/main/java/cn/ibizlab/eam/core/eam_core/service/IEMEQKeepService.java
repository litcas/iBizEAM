package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQKeep;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKeepSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQKeep] 服务对象接口
 */
public interface IEMEQKeepService extends IService<EMEQKeep> {

    boolean create(EMEQKeep et);
    void createBatch(List<EMEQKeep> list);
    boolean update(EMEQKeep et);
    void updateBatch(List<EMEQKeep> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQKeep get(String key);
    EMEQKeep getDraft(EMEQKeep et);
    boolean checkKey(EMEQKeep et);
    boolean save(EMEQKeep et);
    void saveBatch(List<EMEQKeep> list);
    Page<EMEQKeep> searchCalendar(EMEQKeepSearchContext context);
    Page<EMEQKeep> searchDefault(EMEQKeepSearchContext context);
    List<EMEQKeep> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQKeep> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQKeep> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQKeep> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQKeep> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQKeep> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQKeep> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQKeep> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQKeep> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQKeep> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQKeep> getEmeqkeepByIds(List<String> ids);
    List<EMEQKeep> getEmeqkeepByEntities(List<EMEQKeep> entities);
}


