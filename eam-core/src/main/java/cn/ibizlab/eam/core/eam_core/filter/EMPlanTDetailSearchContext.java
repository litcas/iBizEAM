package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
/**
 * 关系型数据实体[EMPlanTDetail] 查询条件对象
 */
@Slf4j
@Data
public class EMPlanTDetailSearchContext extends QueryWrapperContext<EMPlanTDetail> {

	private String n_recvpersonid_eq;//[接收人]
	public void setN_recvpersonid_eq(String n_recvpersonid_eq) {
        this.n_recvpersonid_eq = n_recvpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonid_eq)){
            this.getSearchCond().eq("recvpersonid", n_recvpersonid_eq);
        }
    }
	private String n_emplantdetailname_like;//[计划模板步骤名称]
	public void setN_emplantdetailname_like(String n_emplantdetailname_like) {
        this.n_emplantdetailname_like = n_emplantdetailname_like;
        if(!ObjectUtils.isEmpty(this.n_emplantdetailname_like)){
            this.getSearchCond().like("emplantdetailname", n_emplantdetailname_like);
        }
    }
	private String n_emwotype_eq;//[生成工单种类]
	public void setN_emwotype_eq(String n_emwotype_eq) {
        this.n_emwotype_eq = n_emwotype_eq;
        if(!ObjectUtils.isEmpty(this.n_emwotype_eq)){
            this.getSearchCond().eq("emwotype", n_emwotype_eq);
        }
    }
	private String n_recvpersonname_eq;//[接收人]
	public void setN_recvpersonname_eq(String n_recvpersonname_eq) {
        this.n_recvpersonname_eq = n_recvpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_eq)){
            this.getSearchCond().eq("recvpersonname", n_recvpersonname_eq);
        }
    }
	private String n_recvpersonname_like;//[接收人]
	public void setN_recvpersonname_like(String n_recvpersonname_like) {
        this.n_recvpersonname_like = n_recvpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_like)){
            this.getSearchCond().like("recvpersonname", n_recvpersonname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_plantemplname_eq;//[计划模板]
	public void setN_plantemplname_eq(String n_plantemplname_eq) {
        this.n_plantemplname_eq = n_plantemplname_eq;
        if(!ObjectUtils.isEmpty(this.n_plantemplname_eq)){
            this.getSearchCond().eq("plantemplname", n_plantemplname_eq);
        }
    }
	private String n_plantemplname_like;//[计划模板]
	public void setN_plantemplname_like(String n_plantemplname_like) {
        this.n_plantemplname_like = n_plantemplname_like;
        if(!ObjectUtils.isEmpty(this.n_plantemplname_like)){
            this.getSearchCond().like("plantemplname", n_plantemplname_like);
        }
    }
	private String n_plantemplid_eq;//[计划模板]
	public void setN_plantemplid_eq(String n_plantemplid_eq) {
        this.n_plantemplid_eq = n_plantemplid_eq;
        if(!ObjectUtils.isEmpty(this.n_plantemplid_eq)){
            this.getSearchCond().eq("plantemplid", n_plantemplid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emplantdetailname", query)
            );
		 }
	}
}



