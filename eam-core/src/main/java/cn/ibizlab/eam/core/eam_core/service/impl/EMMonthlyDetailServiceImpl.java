package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthlyDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMMonthlyDetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMMonthlyDetailService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMMonthlyDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[维修中心月度计划明细] 服务对象接口实现
 */
@Slf4j
@Service("EMMonthlyDetailServiceImpl")
public class EMMonthlyDetailServiceImpl extends ServiceImpl<EMMonthlyDetailMapper, EMMonthlyDetail> implements IEMMonthlyDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMMonthlyService emmonthlyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMMonthlyDetail et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmmonthlydetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMMonthlyDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMMonthlyDetail et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emmonthlydetailid", et.getEmmonthlydetailid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmmonthlydetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMMonthlyDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMMonthlyDetail get(String key) {
        EMMonthlyDetail et = getById(key);
        if(et == null){
            et = new EMMonthlyDetail();
            et.setEmmonthlydetailid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMMonthlyDetail getDraft(EMMonthlyDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMMonthlyDetail et) {
        return (!ObjectUtils.isEmpty(et.getEmmonthlydetailid())) && (!Objects.isNull(this.getById(et.getEmmonthlydetailid())));
    }
    @Override
    @Transactional
    public boolean save(EMMonthlyDetail et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMMonthlyDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMMonthlyDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMMonthlyDetail> create = new ArrayList<>();
        List<EMMonthlyDetail> update = new ArrayList<>();
        for (EMMonthlyDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmmonthlydetailid()) || ObjectUtils.isEmpty(getById(et.getEmmonthlydetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMMonthlyDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMMonthlyDetail> create = new ArrayList<>();
        List<EMMonthlyDetail> update = new ArrayList<>();
        for (EMMonthlyDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmmonthlydetailid()) || ObjectUtils.isEmpty(getById(et.getEmmonthlydetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMMonthlyDetail> selectByEmmonthlyid(String emmonthlyid) {
        return baseMapper.selectByEmmonthlyid(emmonthlyid);
    }
    @Override
    public void removeByEmmonthlyid(String emmonthlyid) {
        this.remove(new QueryWrapper<EMMonthlyDetail>().eq("emmonthlyid",emmonthlyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMMonthlyDetail> searchDefault(EMMonthlyDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMMonthlyDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMMonthlyDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMMonthlyDetail et){
        //实体关系[DER1N_EMMONTHLYDETAIL_EMMONTHLY_EMMONTHLYID]
        if(!ObjectUtils.isEmpty(et.getEmmonthlyid())){
            cn.ibizlab.eam.core.eam_core.domain.EMMonthly emmonthly=et.getEmmonthly();
            if(ObjectUtils.isEmpty(emmonthly)){
                cn.ibizlab.eam.core.eam_core.domain.EMMonthly majorEntity=emmonthlyService.get(et.getEmmonthlyid());
                et.setEmmonthly(majorEntity);
                emmonthly=majorEntity;
            }
            et.setEmmonthlyname(emmonthly.getEmmonthlyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMMonthlyDetail> getEmmonthlydetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMMonthlyDetail> getEmmonthlydetailByEntities(List<EMMonthlyDetail> entities) {
        List ids =new ArrayList();
        for(EMMonthlyDetail entity : entities){
            Serializable id=entity.getEmmonthlydetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMMonthlyDetailService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



