package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPurchangeHis;
import cn.ibizlab.eam.core.eam_core.filter.EMPurchangeHisSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPurchangeHisService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPurchangeHisMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[领料单换料记录] 服务对象接口实现
 */
@Slf4j
@Service("EMPurchangeHisServiceImpl")
public class EMPurchangeHisServiceImpl extends ServiceImpl<EMPurchangeHisMapper, EMPurchangeHis> implements IEMPurchangeHisService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPurchangeHis et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpurchangehisid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPurchangeHis> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPurchangeHis et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("empurchangehisid", et.getEmpurchangehisid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpurchangehisid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPurchangeHis> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPurchangeHis get(String key) {
        EMPurchangeHis et = getById(key);
        if(et == null){
            et = new EMPurchangeHis();
            et.setEmpurchangehisid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPurchangeHis getDraft(EMPurchangeHis et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPurchangeHis et) {
        return (!ObjectUtils.isEmpty(et.getEmpurchangehisid())) && (!Objects.isNull(this.getById(et.getEmpurchangehisid())));
    }
    @Override
    @Transactional
    public boolean save(EMPurchangeHis et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPurchangeHis et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPurchangeHis> list) {
        list.forEach(item->fillParentData(item));
        List<EMPurchangeHis> create = new ArrayList<>();
        List<EMPurchangeHis> update = new ArrayList<>();
        for (EMPurchangeHis et : list) {
            if (ObjectUtils.isEmpty(et.getEmpurchangehisid()) || ObjectUtils.isEmpty(getById(et.getEmpurchangehisid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPurchangeHis> list) {
        list.forEach(item->fillParentData(item));
        List<EMPurchangeHis> create = new ArrayList<>();
        List<EMPurchangeHis> update = new ArrayList<>();
        for (EMPurchangeHis et : list) {
            if (ObjectUtils.isEmpty(et.getEmpurchangehisid()) || ObjectUtils.isEmpty(getById(et.getEmpurchangehisid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPurchangeHis> selectByEmitempuseid(String emitempuseid) {
        return baseMapper.selectByEmitempuseid(emitempuseid);
    }
    @Override
    public void removeByEmitempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMPurchangeHis>().eq("emitempuseid",emitempuseid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPurchangeHis> searchDefault(EMPurchangeHisSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPurchangeHis> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPurchangeHis>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPurchangeHis et){
        //实体关系[DER1N_EMPURCHANGEHIS_EMITEMPUSE_EMITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getEmitempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse emitempuse=et.getEmitempuse();
            if(ObjectUtils.isEmpty(emitempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getEmitempuseid());
                et.setEmitempuse(majorEntity);
                emitempuse=majorEntity;
            }
            et.setUnitname(emitempuse.getUnitname());
            et.setItemid(emitempuse.getItemid());
            et.setEmitempusename(emitempuse.getEmitempusename());
            et.setPsum(emitempuse.getPsum());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPurchangeHis> getEmpurchangehisByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPurchangeHis> getEmpurchangehisByEntities(List<EMPurchangeHis> entities) {
        List ids =new ArrayList();
        for(EMPurchangeHis entity : entities){
            Serializable id=entity.getEmpurchangehisid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPurchangeHisService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



