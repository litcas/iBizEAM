package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEMap;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODEMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMRFODEMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMRFODEMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[现象引用] 服务对象接口实现
 */
@Slf4j
@Service("EMRFODEMapServiceImpl")
public class EMRFODEMapServiceImpl extends ServiceImpl<EMRFODEMapMapper, EMRFODEMap> implements IEMRFODEMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMRFODEMap et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrfodemapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMRFODEMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMRFODEMap et) {
        fillParentData(et);
        emobjmapService.update(emrfodemapInheritMapping.toEmobjmap(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emrfodemapid", et.getEmrfodemapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrfodemapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMRFODEMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMRFODEMap get(String key) {
        EMRFODEMap et = getById(key);
        if(et == null){
            et = new EMRFODEMap();
            et.setEmrfodemapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMRFODEMap getDraft(EMRFODEMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMRFODEMap et) {
        return (!ObjectUtils.isEmpty(et.getEmrfodemapid())) && (!Objects.isNull(this.getById(et.getEmrfodemapid())));
    }
    @Override
    @Transactional
    public boolean save(EMRFODEMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMRFODEMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMRFODEMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMRFODEMap> create = new ArrayList<>();
        List<EMRFODEMap> update = new ArrayList<>();
        for (EMRFODEMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmrfodemapid()) || ObjectUtils.isEmpty(getById(et.getEmrfodemapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMRFODEMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMRFODEMap> create = new ArrayList<>();
        List<EMRFODEMap> update = new ArrayList<>();
        for (EMRFODEMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmrfodemapid()) || ObjectUtils.isEmpty(getById(et.getEmrfodemapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMRFODEMap> selectByRefobjid(String emobjectid) {
        return baseMapper.selectByRefobjid(emobjectid);
    }
    @Override
    public void removeByRefobjid(String emobjectid) {
        this.remove(new QueryWrapper<EMRFODEMap>().eq("refobjid",emobjectid));
    }

	@Override
    public List<EMRFODEMap> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }
    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMRFODEMap>().eq("rfodeid",emrfodeid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMRFODEMap> searchDefault(EMRFODEMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMRFODEMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMRFODEMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMRFODEMap et){
        //实体关系[DER1N_EMRFODEMAP_EMOBJECT_REFOBJID]
        if(!ObjectUtils.isEmpty(et.getRefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject refobj=et.getRefobj();
            if(ObjectUtils.isEmpty(refobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getRefobjid());
                et.setRefobj(majorEntity);
                refobj=majorEntity;
            }
            et.setRefobjname(refobj.getEmobjectname());
        }
        //实体关系[DER1N_EMRFODEMAP_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMRFODEMapInheritMapping emrfodemapInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMRFODEMap et){
        if(ObjectUtils.isEmpty(et.getEmrfodemapid()))
            et.setEmrfodemapid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emrfodemapInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","RFODEMAP");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMRFODEMap> getEmrfodemapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMRFODEMap> getEmrfodemapByEntities(List<EMRFODEMap> entities) {
        List ids =new ArrayList();
        for(EMRFODEMap entity : entities){
            Serializable id=entity.getEmrfodemapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMRFODEMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



