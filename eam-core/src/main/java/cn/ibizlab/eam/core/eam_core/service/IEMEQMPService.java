package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQMP;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMPSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQMP] 服务对象接口
 */
public interface IEMEQMPService extends IService<EMEQMP> {

    boolean create(EMEQMP et);
    void createBatch(List<EMEQMP> list);
    boolean update(EMEQMP et);
    void updateBatch(List<EMEQMP> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQMP get(String key);
    EMEQMP getDraft(EMEQMP et);
    boolean checkKey(EMEQMP et);
    boolean save(EMEQMP et);
    void saveBatch(List<EMEQMP> list);
    Page<EMEQMP> searchDefault(EMEQMPSearchContext context);
    List<EMEQMP> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQMP> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQMP> getEmeqmpByIds(List<String> ids);
    List<EMEQMP> getEmeqmpByEntities(List<EMEQMP> entities);
}


