package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMaintance;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMaintanceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEQMaintanceMapper extends BaseMapper<EMEQMaintance> {

    Page<EMEQMaintance> searchCalendar(IPage page, @Param("srf") EMEQMaintanceSearchContext context, @Param("ew") Wrapper<EMEQMaintance> wrapper);
    Page<EMEQMaintance> searchDefault(IPage page, @Param("srf") EMEQMaintanceSearchContext context, @Param("ew") Wrapper<EMEQMaintance> wrapper);
    @Override
    EMEQMaintance selectById(Serializable id);
    @Override
    int insert(EMEQMaintance entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEQMaintance entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEQMaintance entity, @Param("ew") Wrapper<EMEQMaintance> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEQMaintance> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMEQMaintance> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMEQMaintance> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMEQMaintance> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid);

    List<EMEQMaintance> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid);

    List<EMEQMaintance> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid);

    List<EMEQMaintance> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid);

    List<EMEQMaintance> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEQMaintance> selectByWoid(@Param("emwoid") Serializable emwoid);

    List<EMEQMaintance> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
