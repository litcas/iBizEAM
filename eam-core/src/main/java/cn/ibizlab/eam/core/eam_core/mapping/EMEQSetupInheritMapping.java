

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSetup;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQSetupInheritMapping {

    @Mappings({
        @Mapping(source ="emeqsetupid",target = "emeqahid"),
        @Mapping(source ="emeqsetupname",target = "emeqahname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQAH toEmeqah(EMEQSetup minorEntity);

    @Mappings({
        @Mapping(source ="emeqahid" ,target = "emeqsetupid"),
        @Mapping(source ="emeqahname" ,target = "emeqsetupname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQSetup toEmeqsetup(EMEQAH majorEntity);

    List<EMEQAH> toEmeqah(List<EMEQSetup> minorEntities);

    List<EMEQSetup> toEmeqsetup(List<EMEQAH> majorEntities);

}


