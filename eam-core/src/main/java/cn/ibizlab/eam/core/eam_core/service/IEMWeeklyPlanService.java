package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWeeklyPlan;
import cn.ibizlab.eam.core.eam_core.filter.EMWeeklyPlanSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWeeklyPlan] 服务对象接口
 */
public interface IEMWeeklyPlanService extends IService<EMWeeklyPlan> {

    boolean create(EMWeeklyPlan et);
    void createBatch(List<EMWeeklyPlan> list);
    boolean update(EMWeeklyPlan et);
    void updateBatch(List<EMWeeklyPlan> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWeeklyPlan get(String key);
    EMWeeklyPlan getDraft(EMWeeklyPlan et);
    boolean checkKey(EMWeeklyPlan et);
    boolean save(EMWeeklyPlan et);
    void saveBatch(List<EMWeeklyPlan> list);
    Page<EMWeeklyPlan> searchDefault(EMWeeklyPlanSearchContext context);
    List<EMWeeklyPlan> selectByPfteamid(String pfteamid);
    void removeByPfteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWeeklyPlan> getEmweeklyplanByIds(List<String> ids);
    List<EMWeeklyPlan> getEmweeklyplanByEntities(List<EMWeeklyPlan> entities);
}


