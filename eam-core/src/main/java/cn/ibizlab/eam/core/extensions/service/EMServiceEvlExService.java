package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMServiceEvlServiceImpl;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

/**
 * 实体[服务商评估] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMServiceEvlExService")
public class EMServiceEvlExService extends EMServiceEvlServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Confirm:确认] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMServiceEvl confirm(EMServiceEvl et) {
        EMServiceEvl emServiceEvl = this.get(et.getEmserviceevlid());
        if(emServiceEvl == null){
            throw new RuntimeException("未找到对应的服务商评估信息，无法完成操作");
        }

        emServiceEvl.setServiceevlstate(StaticDict.EMSERVICEEVLSTATE.ITEM_20.getValue());
        Aops.getSelf(this).update(emServiceEvl);
        return super.confirm(et);
    }
}

