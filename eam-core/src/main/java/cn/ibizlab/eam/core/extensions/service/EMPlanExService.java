package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.*;
import cn.ibizlab.eam.core.eam_core.service.*;
import cn.ibizlab.eam.core.eam_core.service.impl.EMPlanServiceImpl;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

import java.sql.Timestamp;

/**
 * 实体[计划] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMPlanExService")
public class EMPlanExService extends EMPlanServiceImpl {

    @Autowired
    private IEMWO_ENService iemwoEnService;

    @Autowired
    private IEMWO_DPService iemwoDpService;

    @Autowired
    private IEMWO_INNERService iemwoInnerService;

    @Autowired
    private IEMWO_OSCService iemwoOscService;

    @Autowired
    private IEMPLANRECORDService iemplanrecordService;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [CreateWO:创建工单] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPlan createWO(EMPlan et) {
        // 计划生成对应的工单
        if(et.getEmplanid() == null){
            throw new RuntimeException("计划不存在");
        }

        et = this.get(et.getEmplanid());

        EMPlanDetail planDetail = emplandetailService.get(et.getEmplanid());

//         根据计划生成的工单类型选择生成那种工单
        if(StringUtils.compare(et.getEmwotype(), StaticDict.EMWOKIND.DP.getValue()) == 0){
            // 生成定检工单
            EMWO_DP dp = new EMWO_DP();
            BeanCopier copier = BeanCopier.create(EMPlan.class, EMWO_DP.class, false);
            copier.copy(et, dp, null);
            dp.setEmwoDpname(et.getEmplanname());
            dp.setWogroup(StaticDict.EMWOGROUP.PLAN.getValue());
            // TODO:工单来源
//            dp.setWooriid(planDetail.getEmplandetailid());
            dp.setWodate(new Timestamp(System.currentTimeMillis()));// 安排日期
            dp.setRegionbegindate(new Timestamp(System.currentTimeMillis())); // 实际点检时间
            dp.setWpersonid(et.getRempid()); // 点检人
            dp.setRecvpersonid(et.getRempid()); // 指派点检人

            iemwoDpService.create(dp);
        }
        if(StringUtils.compare(et.getEmwotype(), StaticDict.EMWOKIND.EN.getValue()) == 0){
            // 生成能耗登记工单
            EMWO_EN en = new EMWO_EN();
            BeanCopier copier = BeanCopier.create(EMPlan.class, EMWO_EN.class, false);
            copier.copy(et, en, null);
            en.setEmwoEnname(et.getEmplanname());
            en.setWogroup(StaticDict.EMWOGROUP.PLAN.getValue());
            // TODO:工单来源
//            en.setWooriid(planDetail.getEmplandetailid());
            en.setWodate(new Timestamp(System.currentTimeMillis())); // 安排日期
            en.setWpersonid(et.getRempid()); // 抄表人
//            en.setBdate(); // 上次采集时间
            en.setRegionbegindate(new Timestamp(System.currentTimeMillis())); // 实际抄表时间
//            en.setLastval(); // 上次记录值
            en.setRecvpersonid(et.getRempid()); // 指定抄表人


            iemwoEnService.create(en);
        }
        if(StringUtils.compare(et.getEmwotype(), StaticDict.EMWOKIND.INNER.getValue()) == 0){
            // 生成内部工单
            EMWO_INNER inner = new EMWO_INNER();
            BeanCopier copier = BeanCopier.create(EMPlan.class, EMWO_INNER.class, false);
            copier.copy(et, inner, null);
            inner.setEmwoInnername(et.getEmplanname());
            inner.setWogroup(StaticDict.EMWOGROUP.PLAN.getValue());
            // TODO:工单来源
//            inner.setWooriid(planDetail.getEmplandetailid());
            inner.setWodate(new Timestamp(System.currentTimeMillis())); // 安排执行日期
            inner.setWpersonid(et.getRempid()); // 执行人

            iemwoInnerService.create(inner);
        }
        if(StringUtils.compare(et.getEmwotype(), StaticDict.EMWOKIND.OSC.getValue()) == 0){
            // 生成外委工单
            EMWO_OSC osc = new EMWO_OSC();
            BeanCopier copier = BeanCopier.create(EMPlan.class, EMWO_OSC.class, false);
            copier.copy(et, osc, null);
            osc.setEmwoOscname(et.getEmplanname());
            osc.setWogroup(StaticDict.EMWOGROUP.PLAN.getValue());
            // TODO:工单来源
//            osc.setWooriid(planDetail.getEmplandetailid());
            osc.setWotype(StaticDict.EMWOTYPE.KEEP.getValue());
            osc.setWodate(new Timestamp(System.currentTimeMillis()));
            osc.setWpersonid(et.getRempid());

            iemwoOscService.create(osc);
        }

        EMPLANRECORD emplanrecord = new EMPLANRECORD();

        emplanrecord.setEmplanid(et.getEmplanid());
        emplanrecord.setEmplanname(et.getEmplanname());
        emplanrecord.setTriggerdate(et.getMdate());
        iemplanrecordService.create(emplanrecord);

        return super.createWO(et);
    }

}

