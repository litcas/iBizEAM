

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMApply;
import cn.ibizlab.eam.core.eam_core.domain.EMWOORI;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMApplyInheritMapping {

    @Mappings({
        @Mapping(source ="emapplyid",target = "emwooriid"),
        @Mapping(source ="emapplyname",target = "emwooriname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMWOORI toEmwoori(EMApply minorEntity);

    @Mappings({
        @Mapping(source ="emwooriid" ,target = "emapplyid"),
        @Mapping(source ="emwooriname" ,target = "emapplyname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMApply toEmapply(EMWOORI majorEntity);

    List<EMWOORI> toEmwoori(List<EMApply> minorEntities);

    List<EMApply> toEmapply(List<EMWOORI> majorEntities);

}


