package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQType;
/**
 * 关系型数据实体[EMEQType] 查询条件对象
 */
@Slf4j
@Data
public class EMEQTypeSearchContext extends QueryWrapperContext<EMEQType> {

	private String n_eqtypegroup_eq;//[类型分组]
	public void setN_eqtypegroup_eq(String n_eqtypegroup_eq) {
        this.n_eqtypegroup_eq = n_eqtypegroup_eq;
        if(!ObjectUtils.isEmpty(this.n_eqtypegroup_eq)){
            this.getSearchCond().eq("eqtypegroup", n_eqtypegroup_eq);
        }
    }
	private String n_eqtypecode_like;//[类型代码]
	public void setN_eqtypecode_like(String n_eqtypecode_like) {
        this.n_eqtypecode_like = n_eqtypecode_like;
        if(!ObjectUtils.isEmpty(this.n_eqtypecode_like)){
            this.getSearchCond().like("eqtypecode", n_eqtypecode_like);
        }
    }
	private String n_eqtypeinfo_like;//[设备类型信息]
	public void setN_eqtypeinfo_like(String n_eqtypeinfo_like) {
        this.n_eqtypeinfo_like = n_eqtypeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_eqtypeinfo_like)){
            this.getSearchCond().like("eqtypeinfo", n_eqtypeinfo_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emeqtypename_like;//[设备类型名称]
	public void setN_emeqtypename_like(String n_emeqtypename_like) {
        this.n_emeqtypename_like = n_emeqtypename_like;
        if(!ObjectUtils.isEmpty(this.n_emeqtypename_like)){
            this.getSearchCond().like("emeqtypename", n_emeqtypename_like);
        }
    }
	private String n_eqtypepname_eq;//[上级设备类型]
	public void setN_eqtypepname_eq(String n_eqtypepname_eq) {
        this.n_eqtypepname_eq = n_eqtypepname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqtypepname_eq)){
            this.getSearchCond().eq("eqtypepname", n_eqtypepname_eq);
        }
    }
	private String n_eqtypepname_like;//[上级设备类型]
	public void setN_eqtypepname_like(String n_eqtypepname_like) {
        this.n_eqtypepname_like = n_eqtypepname_like;
        if(!ObjectUtils.isEmpty(this.n_eqtypepname_like)){
            this.getSearchCond().like("eqtypepname", n_eqtypepname_like);
        }
    }
	private String n_eqtypepid_eq;//[上级设备类型]
	public void setN_eqtypepid_eq(String n_eqtypepid_eq) {
        this.n_eqtypepid_eq = n_eqtypepid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqtypepid_eq)){
            this.getSearchCond().eq("eqtypepid", n_eqtypepid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqtypename", query)
            );
		 }
	}
}



