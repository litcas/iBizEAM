package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMService;
/**
 * 关系型数据实体[EMService] 查询条件对象
 */
@Slf4j
@Data
public class EMServiceSearchContext extends QueryWrapperContext<EMService> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_payway_eq;//[付款方式]
	public void setN_payway_eq(String n_payway_eq) {
        this.n_payway_eq = n_payway_eq;
        if(!ObjectUtils.isEmpty(this.n_payway_eq)){
            this.getSearchCond().eq("payway", n_payway_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_serviceinfo_like;//[服务商信息]
	public void setN_serviceinfo_like(String n_serviceinfo_like) {
        this.n_serviceinfo_like = n_serviceinfo_like;
        if(!ObjectUtils.isEmpty(this.n_serviceinfo_like)){
            this.getSearchCond().like("serviceinfo", n_serviceinfo_like);
        }
    }
	private String n_labservicelevelid_eq;//[级别]
	public void setN_labservicelevelid_eq(String n_labservicelevelid_eq) {
        this.n_labservicelevelid_eq = n_labservicelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicelevelid_eq)){
            this.getSearchCond().eq("labservicelevelid", n_labservicelevelid_eq);
        }
    }
	private String n_labservicetypeid_eq;//[服务商类型]
	public void setN_labservicetypeid_eq(String n_labservicetypeid_eq) {
        this.n_labservicetypeid_eq = n_labservicetypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicetypeid_eq)){
            this.getSearchCond().eq("labservicetypeid", n_labservicetypeid_eq);
        }
    }
	private Integer n_range_eq;//[服务商归属]
	public void setN_range_eq(Integer n_range_eq) {
        this.n_range_eq = n_range_eq;
        if(!ObjectUtils.isEmpty(this.n_range_eq)){
            this.getSearchCond().eq("range", n_range_eq);
        }
    }
	private String n_emservicename_like;//[服务商名称]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_taxtypeid_eq;//[税类型]
	public void setN_taxtypeid_eq(String n_taxtypeid_eq) {
        this.n_taxtypeid_eq = n_taxtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_taxtypeid_eq)){
            this.getSearchCond().eq("taxtypeid", n_taxtypeid_eq);
        }
    }
	private String n_servicestate_eq;//[服务商状态]
	public void setN_servicestate_eq(String n_servicestate_eq) {
        this.n_servicestate_eq = n_servicestate_eq;
        if(!ObjectUtils.isEmpty(this.n_servicestate_eq)){
            this.getSearchCond().eq("servicestate", n_servicestate_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emservicename", query)
            );
		 }
	}
}



