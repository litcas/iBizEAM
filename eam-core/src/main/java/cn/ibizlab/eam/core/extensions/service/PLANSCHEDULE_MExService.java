package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.PLANSCHEDULE_MServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Primary;

/**
 * 实体[计划_按月] 自定义服务对象
 */
@Slf4j
@Primary
@Service("PLANSCHEDULE_MExService")
public class PLANSCHEDULE_MExService extends PLANSCHEDULE_MServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

}

