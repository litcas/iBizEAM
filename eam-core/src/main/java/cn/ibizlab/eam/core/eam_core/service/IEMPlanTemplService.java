package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTemplSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPlanTempl] 服务对象接口
 */
public interface IEMPlanTemplService extends IService<EMPlanTempl> {

    boolean create(EMPlanTempl et);
    void createBatch(List<EMPlanTempl> list);
    boolean update(EMPlanTempl et);
    void updateBatch(List<EMPlanTempl> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPlanTempl get(String key);
    EMPlanTempl getDraft(EMPlanTempl et);
    boolean checkKey(EMPlanTempl et);
    boolean save(EMPlanTempl et);
    void saveBatch(List<EMPlanTempl> list);
    Page<EMPlanTempl> searchDefault(EMPlanTemplSearchContext context);
    List<EMPlanTempl> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMPlanTempl> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMPlanTempl> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPlanTempl> getEmplantemplByIds(List<String> ids);
    List<EMPlanTempl> getEmplantemplByEntities(List<EMPlanTempl> entities);
}


