package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWork;
import cn.ibizlab.eam.core.eam_core.filter.EMWorkSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWorkService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWorkMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[加班工单] 服务对象接口实现
 */
@Slf4j
@Service("EMWorkServiceImpl")
public class EMWorkServiceImpl extends ServiceImpl<EMWorkMapper, EMWork> implements IEMWorkService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWork et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmworkid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWork> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWork et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emworkid", et.getEmworkid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmworkid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWork> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWork get(String key) {
        EMWork et = getById(key);
        if(et == null){
            et = new EMWork();
            et.setEmworkid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMWork getDraft(EMWork et) {
        return et;
    }

    @Override
    public boolean checkKey(EMWork et) {
        return (!ObjectUtils.isEmpty(et.getEmworkid())) && (!Objects.isNull(this.getById(et.getEmworkid())));
    }
    @Override
    @Transactional
    public boolean save(EMWork et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWork et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWork> list) {
        List<EMWork> create = new ArrayList<>();
        List<EMWork> update = new ArrayList<>();
        for (EMWork et : list) {
            if (ObjectUtils.isEmpty(et.getEmworkid()) || ObjectUtils.isEmpty(getById(et.getEmworkid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWork> list) {
        List<EMWork> create = new ArrayList<>();
        List<EMWork> update = new ArrayList<>();
        for (EMWork et : list) {
            if (ObjectUtils.isEmpty(et.getEmworkid()) || ObjectUtils.isEmpty(getById(et.getEmworkid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWork> searchDefault(EMWorkSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWork> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWork>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWork> getEmworkByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWork> getEmworkByEntities(List<EMWork> entities) {
        List ids =new ArrayList();
        for(EMWork entity : entities){
            Serializable id=entity.getEmworkid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMWorkService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



