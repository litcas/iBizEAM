package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMSubList;
import cn.ibizlab.eam.core.eam_core.filter.EMSubListSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMSubList] 服务对象接口
 */
public interface IEMSubListService extends IService<EMSubList> {

    boolean create(EMSubList et);
    void createBatch(List<EMSubList> list);
    boolean update(EMSubList et);
    void updateBatch(List<EMSubList> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMSubList get(String key);
    EMSubList getDraft(EMSubList et);
    boolean checkKey(EMSubList et);
    boolean save(EMSubList et);
    void saveBatch(List<EMSubList> list);
    Page<EMSubList> searchDefault(EMSubListSearchContext context);
    List<EMSubList> selectByPfteamid(String pfteamid);
    void removeByPfteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMSubList> getEmsublistByIds(List<String> ids);
    List<EMSubList> getEmsublistByEntities(List<EMSubList> entities);
}


