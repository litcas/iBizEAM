package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTRHY;
/**
 * 关系型数据实体[EMEQLCTRHY] 查询条件对象
 */
@Slf4j
@Data
public class EMEQLCTRHYSearchContext extends QueryWrapperContext<EMEQLCTRHY> {

	private String n_rhytype_eq;//[润滑油类型]
	public void setN_rhytype_eq(String n_rhytype_eq) {
        this.n_rhytype_eq = n_rhytype_eq;
        if(!ObjectUtils.isEmpty(this.n_rhytype_eq)){
            this.getSearchCond().eq("rhytype", n_rhytype_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_eqlocationinfo_eq;//[润滑油信息]
	public void setN_eqlocationinfo_eq(String n_eqlocationinfo_eq) {
        this.n_eqlocationinfo_eq = n_eqlocationinfo_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationinfo_eq)){
            this.getSearchCond().eq("eqlocationinfo", n_eqlocationinfo_eq);
        }
    }
	private String n_eqlocationinfo_like;//[润滑油信息]
	public void setN_eqlocationinfo_like(String n_eqlocationinfo_like) {
        this.n_eqlocationinfo_like = n_eqlocationinfo_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationinfo_like)){
            this.getSearchCond().like("eqlocationinfo", n_eqlocationinfo_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_emeqlocationid_eq;//[位置标识]
	public void setN_emeqlocationid_eq(String n_emeqlocationid_eq) {
        this.n_emeqlocationid_eq = n_emeqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_emeqlocationid_eq)){
            this.getSearchCond().eq("emeqlocationid", n_emeqlocationid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("eqlocationinfo", query)
            );
		 }
	}
}



