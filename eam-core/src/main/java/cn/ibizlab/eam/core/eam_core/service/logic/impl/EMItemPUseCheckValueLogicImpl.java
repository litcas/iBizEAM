package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemPUseCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMItemPUseCheckValueLogicImpl implements IEMItemPUseCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService getEmitempuseService() {
        return this.emitempuseservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItemPUse et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitempusecheckvaluedefault", et);
            kieSession.setGlobal("emitempuseservice", emitempuseservice);
            kieSession.setGlobal("iBzSysEmitempuseDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitempusecheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[CheckValue]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
