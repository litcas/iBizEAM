package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
/**
 * 关系型数据实体[EMItemTrade] 查询条件对象
 */
@Slf4j
@Data
public class EMItemTradeSearchContext extends QueryWrapperContext<EMItemTrade> {

	private String n_emitemtradename_like;//[物品交易名称]
	public void setN_emitemtradename_like(String n_emitemtradename_like) {
        this.n_emitemtradename_like = n_emitemtradename_like;
        if(!ObjectUtils.isEmpty(this.n_emitemtradename_like)){
            this.getSearchCond().like("emitemtradename", n_emitemtradename_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private Integer n_tradestate_eq;//[交易状态]
	public void setN_tradestate_eq(Integer n_tradestate_eq) {
        this.n_tradestate_eq = n_tradestate_eq;
        if(!ObjectUtils.isEmpty(this.n_tradestate_eq)){
            this.getSearchCond().eq("tradestate", n_tradestate_eq);
        }
    }
	private String n_pusetype_eq;//[领料分类]
	public void setN_pusetype_eq(String n_pusetype_eq) {
        this.n_pusetype_eq = n_pusetype_eq;
        if(!ObjectUtils.isEmpty(this.n_pusetype_eq)){
            this.getSearchCond().eq("pusetype", n_pusetype_eq);
        }
    }
	private Double n_shf_gt;//[税费]
	public void setN_shf_gt(Double n_shf_gt) {
        this.n_shf_gt = n_shf_gt;
        if(!ObjectUtils.isEmpty(this.n_shf_gt)){
            this.getSearchCond().gt("shf", n_shf_gt);
        }
    }
	private String n_emitemtradetype_eq;//[交易分组]
	public void setN_emitemtradetype_eq(String n_emitemtradetype_eq) {
        this.n_emitemtradetype_eq = n_emitemtradetype_eq;
        if(!ObjectUtils.isEmpty(this.n_emitemtradetype_eq)){
            this.getSearchCond().eq("emitemtradetype", n_emitemtradetype_eq);
        }
    }
	private String n_storename_eq;//[仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_rname_eq;//[关联单]
	public void setN_rname_eq(String n_rname_eq) {
        this.n_rname_eq = n_rname_eq;
        if(!ObjectUtils.isEmpty(this.n_rname_eq)){
            this.getSearchCond().eq("rname", n_rname_eq);
        }
    }
	private String n_rname_like;//[关联单]
	public void setN_rname_like(String n_rname_like) {
        this.n_rname_like = n_rname_like;
        if(!ObjectUtils.isEmpty(this.n_rname_like)){
            this.getSearchCond().like("rname", n_rname_like);
        }
    }
	private String n_storepartname_eq;//[库位]
	public void setN_storepartname_eq(String n_storepartname_eq) {
        this.n_storepartname_eq = n_storepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartname_eq)){
            this.getSearchCond().eq("storepartname", n_storepartname_eq);
        }
    }
	private String n_storepartname_like;//[库位]
	public void setN_storepartname_like(String n_storepartname_like) {
        this.n_storepartname_like = n_storepartname_like;
        if(!ObjectUtils.isEmpty(this.n_storepartname_like)){
            this.getSearchCond().like("storepartname", n_storepartname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_teamname_eq;//[班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_labservicename_eq;//[供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_labserviceid_eq;//[供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_teamid_eq;//[班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }
	private String n_rid_eq;//[关联单]
	public void setN_rid_eq(String n_rid_eq) {
        this.n_rid_eq = n_rid_eq;
        if(!ObjectUtils.isEmpty(this.n_rid_eq)){
            this.getSearchCond().eq("rid", n_rid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_storeid_eq;//[仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }
	private String n_storepartid_eq;//[库位]
	public void setN_storepartid_eq(String n_storepartid_eq) {
        this.n_storepartid_eq = n_storepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartid_eq)){
            this.getSearchCond().eq("storepartid", n_storepartid_eq);
        }
    }
	private String n_aempid_eq;//[申请人]
	public void setN_aempid_eq(String n_aempid_eq) {
        this.n_aempid_eq = n_aempid_eq;
        if(!ObjectUtils.isEmpty(this.n_aempid_eq)){
            this.getSearchCond().eq("aempid", n_aempid_eq);
        }
    }
	private String n_aempname_eq;//[申请人]
	public void setN_aempname_eq(String n_aempname_eq) {
        this.n_aempname_eq = n_aempname_eq;
        if(!ObjectUtils.isEmpty(this.n_aempname_eq)){
            this.getSearchCond().eq("aempname", n_aempname_eq);
        }
    }
	private String n_aempname_like;//[申请人]
	public void setN_aempname_like(String n_aempname_like) {
        this.n_aempname_like = n_aempname_like;
        if(!ObjectUtils.isEmpty(this.n_aempname_like)){
            this.getSearchCond().like("aempname", n_aempname_like);
        }
    }
	private String n_deptid_eq;//[部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_deptname_eq;//[部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_sempid_eq;//[库管员]
	public void setN_sempid_eq(String n_sempid_eq) {
        this.n_sempid_eq = n_sempid_eq;
        if(!ObjectUtils.isEmpty(this.n_sempid_eq)){
            this.getSearchCond().eq("sempid", n_sempid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emitemtradename", query)
            );
		 }
	}
}



