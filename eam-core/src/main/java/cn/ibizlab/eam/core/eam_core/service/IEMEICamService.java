package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICam;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICam] 服务对象接口
 */
public interface IEMEICamService extends IService<EMEICam> {

    boolean create(EMEICam et);
    void createBatch(List<EMEICam> list);
    boolean update(EMEICam et);
    void updateBatch(List<EMEICam> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICam get(String key);
    EMEICam getDraft(EMEICam et);
    boolean checkKey(EMEICam et);
    boolean save(EMEICam et);
    void saveBatch(List<EMEICam> list);
    Page<EMEICam> searchDefault(EMEICamSearchContext context);
    List<EMEICam> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEICam> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEICam> selectByItempuseid(String emitempuseid);
    void removeByItempuseid(String emitempuseid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICam> getEmeicamByIds(List<String> ids);
    List<EMEICam> getEmeicamByEntities(List<EMEICam> entities);
}


