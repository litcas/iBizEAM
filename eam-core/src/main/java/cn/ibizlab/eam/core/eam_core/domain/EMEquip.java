package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备档案]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQUIP_BASE", resultMap = "EMEquipResultMap")
@ApiModel("设备档案")
public class EMEquip extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图片
     */
    @TableField(value = "pic9")
    @JSONField(name = "pic9")
    @JsonProperty("pic9")
    @ApiModelProperty("图片")
    private String pic9;
    /**
     * 强检周期(年)
     */
    @TableField(value = "efcheck")
    @JSONField(name = "efcheck")
    @JsonProperty("efcheck")
    @ApiModelProperty("强检周期(年)")
    private Double efcheck;
    /**
     * 下次检测日期
     */
    @TableField(value = "efcheckndate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "efcheckndate", format = "yyyy-MM-dd")
    @JsonProperty("efcheckndate")
    @ApiModelProperty("下次检测日期")
    private Timestamp efcheckndate;
    /**
     * 更换价格
     */
    @TableField(value = "replacecost")
    @JSONField(name = "replacecost")
    @JsonProperty("replacecost")
    @ApiModelProperty("更换价格")
    private String replacecost;
    /**
     * 设备优先级
     */
    @TableField(value = "eqpriority")
    @JSONField(name = "eqpriority")
    @JsonProperty("eqpriority")
    @ApiModelProperty("设备优先级")
    private Double eqpriority;
    /**
     * 利用率(当年)
     */
    @DEField(name = "efficiency_y")
    @TableField(value = "efficiency_y")
    @JSONField(name = "efficiency_y")
    @JsonProperty("efficiency_y")
    @ApiModelProperty("利用率(当年)")
    private Double efficiencyY;
    /**
     * 完好率(当年)
     */
    @DEField(name = "intactrate_y")
    @TableField(value = "intactrate_y")
    @JSONField(name = "intactrate_y")
    @JsonProperty("intactrate_y")
    @ApiModelProperty("完好率(当年)")
    private Double intactrateY;
    /**
     * 完好率(当季度)
     */
    @DEField(name = "intactrate_q")
    @TableField(value = "intactrate_q")
    @JSONField(name = "intactrate_q")
    @JsonProperty("intactrate_q")
    @ApiModelProperty("完好率(当季度)")
    private Double intactrateQ;
    /**
     * 利用率(当月)
     */
    @DEField(name = "efficiency_m")
    @TableField(value = "efficiency_m")
    @JSONField(name = "efficiency_m")
    @JsonProperty("efficiency_m")
    @ApiModelProperty("利用率(当月)")
    private Double efficiencyM;
    /**
     * 设备名称
     */
    @TableField(value = "emequipname")
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @ApiModelProperty("设备名称")
    private String emequipname;
    /**
     * 设备标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emequipid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @ApiModelProperty("设备标识")
    private String emequipid;
    /**
     * 强检备注
     */
    @TableField(value = "efcheckdesc")
    @JSONField(name = "efcheckdesc")
    @JsonProperty("efcheckdesc")
    @ApiModelProperty("强检备注")
    private String efcheckdesc;
    /**
     * 故障率(当月)
     */
    @DEField(name = "failurerate_m")
    @TableField(value = "failurerate_m")
    @JSONField(name = "failurerate_m")
    @JsonProperty("failurerate_m")
    @ApiModelProperty("故障率(当月)")
    private Double failurerateM;
    /**
     * 专责部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @ApiModelProperty("专责部门")
    private String deptid;
    /**
     * 图片
     */
    @TableField(value = "pic6")
    @JSONField(name = "pic6")
    @JsonProperty("pic6")
    @ApiModelProperty("图片")
    private String pic6;
    /**
     * 材料成本
     */
    @TableField(value = "materialcost")
    @JSONField(name = "materialcost")
    @JsonProperty("materialcost")
    @ApiModelProperty("材料成本")
    private String materialcost;
    /**
     * 停机类型
     */
    @TableField(value = "haltstate")
    @JSONField(name = "haltstate")
    @JsonProperty("haltstate")
    @ApiModelProperty("停机类型")
    private String haltstate;
    /**
     * 完好率(当月)
     */
    @DEField(name = "intactrate_m")
    @TableField(value = "intactrate_m")
    @JSONField(name = "intactrate_m")
    @JsonProperty("intactrate_m")
    @ApiModelProperty("完好率(当月)")
    private Double intactrateM;
    /**
     * 图片
     */
    @TableField(value = "pic8")
    @JSONField(name = "pic8")
    @JsonProperty("pic8")
    @ApiModelProperty("图片")
    private String pic8;
    /**
     * 购买日期
     */
    @TableField(value = "purchdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "purchdate", format = "yyyy-MM-dd")
    @JsonProperty("purchdate")
    @ApiModelProperty("购买日期")
    private Timestamp purchdate;
    /**
     * 停机原因
     */
    @TableField(value = "haltcause")
    @JSONField(name = "haltcause")
    @JsonProperty("haltcause")
    @ApiModelProperty("停机原因")
    private String haltcause;
    /**
     * 生产属性
     */
    @TableField(value = "productparam")
    @JSONField(name = "productparam")
    @JsonProperty("productparam")
    @ApiModelProperty("生产属性")
    private String productparam;
    /**
     * 图片
     */
    @TableField(value = "pic4")
    @JSONField(name = "pic4")
    @JsonProperty("pic4")
    @ApiModelProperty("图片")
    private String pic4;
    /**
     * 集团设备编号
     */
    @DEField(name = "equip_bh")
    @TableField(value = "equip_bh")
    @JSONField(name = "equip_bh")
    @JsonProperty("equip_bh")
    @ApiModelProperty("集团设备编号")
    private String equipBh;
    /**
     * 维护成本累计
     */
    @TableField(value = "maintenancecost")
    @JSONField(name = "maintenancecost")
    @JsonProperty("maintenancecost")
    @ApiModelProperty("维护成本累计")
    private String maintenancecost;
    /**
     * 设备状态
     */
    @DEField(defaultValue = "1")
    @TableField(value = "eqstate")
    @JSONField(name = "eqstate")
    @JsonProperty("eqstate")
    @ApiModelProperty("设备状态")
    private String eqstate;
    /**
     * 寿命
     */
    @TableField(value = "eqlife")
    @JSONField(name = "eqlife")
    @JsonProperty("eqlife")
    @ApiModelProperty("寿命")
    private Double eqlife;
    /**
     * 初始价格
     */
    @TableField(value = "originalcost")
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @ApiModelProperty("初始价格")
    private String originalcost;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 基本参数
     */
    @TableField(value = "params")
    @JSONField(name = "params")
    @JsonProperty("params")
    @ApiModelProperty("基本参数")
    private String params;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 箱量操作量(当季度)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "outputrct_dj")
    @JSONField(name = "outputrct_dj")
    @JsonProperty("outputrct_dj")
    @ApiModelProperty("箱量操作量(当季度)")
    private Double outputrctDj;
    /**
     * 产品系列号
     */
    @TableField(value = "eqserialcode")
    @JSONField(name = "eqserialcode")
    @JsonProperty("eqserialcode")
    @ApiModelProperty("产品系列号")
    private String eqserialcode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 图片
     */
    @TableField(value = "pic2")
    @JSONField(name = "pic2")
    @JsonProperty("pic2")
    @ApiModelProperty("图片")
    private String pic2;
    /**
     * 图片
     */
    @TableField(value = "pic3")
    @JSONField(name = "pic3")
    @JsonProperty("pic3")
    @ApiModelProperty("图片")
    private String pic3;
    /**
     * 设备代码
     */
    @TableField(value = "equipcode")
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    @ApiModelProperty("设备代码")
    private String equipcode;
    /**
     * 箱量操作量(当月)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "outputrct_dy")
    @JSONField(name = "outputrct_dy")
    @JsonProperty("outputrct_dy")
    @ApiModelProperty("箱量操作量(当月)")
    private Double outputrctDy;
    /**
     * 成本中心
     */
    @TableField(value = "costcenterid")
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @ApiModelProperty("成本中心")
    private String costcenterid;
    /**
     * 投运日期
     */
    @TableField(value = "eqstartdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "eqstartdate", format = "yyyy-MM-dd")
    @JsonProperty("eqstartdate")
    @ApiModelProperty("投运日期")
    private Timestamp eqstartdate;
    /**
     * 保修终止日期
     */
    @TableField(value = "warrantydate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "warrantydate", format = "yyyy-MM-dd")
    @JsonProperty("warrantydate")
    @ApiModelProperty("保修终止日期")
    private Timestamp warrantydate;
    /**
     * 统计大型设备
     */
    @TableField(value = "eqisservice1")
    @JSONField(name = "eqisservice1")
    @JsonProperty("eqisservice1")
    @ApiModelProperty("统计大型设备")
    private Integer eqisservice1;
    /**
     * 故障率(当季度)
     */
    @DEField(name = "failurerate_q")
    @TableField(value = "failurerate_q")
    @JSONField(name = "failurerate_q")
    @JsonProperty("failurerate_q")
    @ApiModelProperty("故障率(当季度)")
    private Double failurerateQ;
    /**
     * 图片
     */
    @TableField(value = "pic7")
    @JSONField(name = "pic7")
    @JsonProperty("pic7")
    @ApiModelProperty("图片")
    private String pic7;
    /**
     * 图片
     */
    @TableField(value = "pic")
    @JSONField(name = "pic")
    @JsonProperty("pic")
    @ApiModelProperty("图片")
    private String pic;
    /**
     * 所属系统备注
     */
    @TableField(value = "blsystemdesc")
    @JSONField(name = "blsystemdesc")
    @JsonProperty("blsystemdesc")
    @ApiModelProperty("所属系统备注")
    private String blsystemdesc;
    /**
     * 利用率(当季度)
     */
    @DEField(name = "efficiency_q")
    @TableField(value = "efficiency_q")
    @JSONField(name = "efficiency_q")
    @JsonProperty("efficiency_q")
    @ApiModelProperty("利用率(当季度)")
    private Double efficiencyQ;
    /**
     * 产品型号
     */
    @TableField(value = "eqmodelcode")
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @ApiModelProperty("产品型号")
    private String eqmodelcode;
    /**
     * 图片
     */
    @TableField(value = "pic5")
    @JSONField(name = "pic5")
    @JsonProperty("pic5")
    @ApiModelProperty("图片")
    private String pic5;
    /**
     * 设备备注
     */
    @TableField(value = "equipdesc")
    @JSONField(name = "equipdesc")
    @JsonProperty("equipdesc")
    @ApiModelProperty("设备备注")
    private String equipdesc;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 人工成本
     */
    @TableField(value = "innerlaborcost")
    @JSONField(name = "innerlaborcost")
    @JsonProperty("innerlaborcost")
    @ApiModelProperty("人工成本")
    private String innerlaborcost;
    /**
     * 关键属性参数
     */
    @TableField(value = "keyattparam")
    @JSONField(name = "keyattparam")
    @JsonProperty("keyattparam")
    @ApiModelProperty("关键属性参数")
    private String keyattparam;
    /**
     * 专责人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @ApiModelProperty("专责人")
    private String empid;
    /**
     * 工艺代码
     */
    @TableField(value = "techcode")
    @JSONField(name = "techcode")
    @JsonProperty("techcode")
    @ApiModelProperty("工艺代码")
    private String techcode;
    /**
     * 箱量操作量(当年)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "outputrct_dn")
    @JSONField(name = "outputrct_dn")
    @JsonProperty("outputrct_dn")
    @ApiModelProperty("箱量操作量(当年)")
    private Double outputrctDn;
    /**
     * 是否在工作
     */
    @TableField(value = "eqisservice")
    @JSONField(name = "eqisservice")
    @JsonProperty("eqisservice")
    @ApiModelProperty("是否在工作")
    private Integer eqisservice;
    /**
     * 故障率(当年)
     */
    @DEField(name = "failurerate_y")
    @TableField(value = "failurerate_y")
    @JSONField(name = "failurerate_y")
    @JsonProperty("failurerate_y")
    @ApiModelProperty("故障率(当年)")
    private Double failurerateY;
    /**
     * 本次检测日期
     */
    @TableField(value = "efcheckdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "efcheckdate", format = "yyyy-MM-dd")
    @JsonProperty("efcheckdate")
    @ApiModelProperty("本次检测日期")
    private Timestamp efcheckdate;
    /**
     * 服务成本
     */
    @TableField(value = "foreignlaborcost")
    @JSONField(name = "foreignlaborcost")
    @JsonProperty("foreignlaborcost")
    @ApiModelProperty("服务成本")
    private String foreignlaborcost;
    /**
     * 设备分组
     */
    @DEField(defaultValue = "1")
    @TableField(value = "equipgroup")
    @JSONField(name = "equipgroup")
    @JsonProperty("equipgroup")
    @ApiModelProperty("设备分组")
    private Integer equipgroup;
    /**
     * 专责部门
     */
    @TableField(value = "deptname")
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @ApiModelProperty("专责部门")
    private String deptname;
    /**
     * 设备状态情况
     */
    @TableField(exist = false)
    @JSONField(name = "haltstateinfo")
    @JsonProperty("haltstateinfo")
    @ApiModelProperty("设备状态情况")
    private String haltstateinfo;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 本次发证日期
     */
    @TableField(value = "efcheckcdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "efcheckcdate", format = "yyyy-MM-dd")
    @JsonProperty("efcheckcdate")
    @ApiModelProperty("本次发证日期")
    private Timestamp efcheckcdate;
    /**
     * 设备信息
     */
    @TableField(exist = false)
    @JSONField(name = "equipinfo")
    @JsonProperty("equipinfo")
    @ApiModelProperty("设备信息")
    private String equipinfo;
    /**
     * 总停机时间
     */
    @TableField(value = "eqsumstoptime")
    @JSONField(name = "eqsumstoptime")
    @JsonProperty("eqsumstoptime")
    @ApiModelProperty("总停机时间")
    private Double eqsumstoptime;
    /**
     * 专责人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @ApiModelProperty("专责人")
    private String empname;
    /**
     * 泊位编码
     */
    @TableField(exist = false)
    @JSONField(name = "emberthcode")
    @JsonProperty("emberthcode")
    @ApiModelProperty("泊位编码")
    private String emberthcode;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @ApiModelProperty("责任班组")
    private String rteamname;
    /**
     * 机种编号1
     */
    @TableField(exist = false)
    @JSONField(name = "jzbh1")
    @JsonProperty("jzbh1")
    @ApiModelProperty("机种编号1")
    private String jzbh1;
    /**
     * 机种
     */
    @TableField(exist = false)
    @JSONField(name = "emmachinecategoryname")
    @JsonProperty("emmachinecategoryname")
    @ApiModelProperty("机种")
    private String emmachinecategoryname;
    /**
     * 统计归口类型分组
     */
    @TableField(exist = false)
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @ApiModelProperty("统计归口类型分组")
    private String stype;
    /**
     * 制造商
     */
    @TableField(exist = false)
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @ApiModelProperty("制造商")
    private String mservicename;
    /**
     * 设备类型代码
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypecode")
    @JsonProperty("eqtypecode")
    @ApiModelProperty("设备类型代码")
    private String eqtypecode;
    /**
     * 资产科目代码
     */
    @TableField(exist = false)
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    @ApiModelProperty("资产科目代码")
    private String assetclasscode;
    /**
     * 上级设备代码
     */
    @TableField(exist = false)
    @JSONField(name = "equippcode")
    @JsonProperty("equippcode")
    @ApiModelProperty("上级设备代码")
    private String equippcode;
    /**
     * 设备类型
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypename")
    @JsonProperty("eqtypename")
    @ApiModelProperty("设备类型")
    private String eqtypename;
    /**
     * 机型
     */
    @TableField(exist = false)
    @JSONField(name = "emmachmodelname")
    @JsonProperty("emmachmodelname")
    @ApiModelProperty("机型")
    private String emmachmodelname;
    /**
     * 品牌编码
     */
    @TableField(exist = false)
    @JSONField(name = "embrandcode")
    @JsonProperty("embrandcode")
    @ApiModelProperty("品牌编码")
    private String embrandcode;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @ApiModelProperty("总帐科目")
    private String acclassname;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @ApiModelProperty("产品供应商")
    private String labservicename;
    /**
     * 资产
     */
    @TableField(exist = false)
    @JSONField(name = "assetname")
    @JsonProperty("assetname")
    @ApiModelProperty("资产")
    private String assetname;
    /**
     * 资产科目
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @ApiModelProperty("资产科目")
    private String assetclassname;
    /**
     * 品牌
     */
    @TableField(exist = false)
    @JSONField(name = "embrandname")
    @JsonProperty("embrandname")
    @ApiModelProperty("品牌")
    private String embrandname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @ApiModelProperty("位置")
    private String eqlocationname;
    /**
     * 上级设备
     */
    @TableField(exist = false)
    @JSONField(name = "equippname")
    @JsonProperty("equippname")
    @ApiModelProperty("上级设备")
    private String equippname;
    /**
     * 资产科目
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @ApiModelProperty("资产科目")
    private String assetclassid;
    /**
     * 合同
     */
    @TableField(exist = false)
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    @ApiModelProperty("合同")
    private String contractname;
    /**
     * 泊位
     */
    @TableField(exist = false)
    @JSONField(name = "emberthname")
    @JsonProperty("emberthname")
    @ApiModelProperty("泊位")
    private String emberthname;
    /**
     * 资产代码
     */
    @TableField(exist = false)
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @ApiModelProperty("资产代码")
    private String assetcode;
    /**
     * 服务提供商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @ApiModelProperty("服务提供商")
    private String rservicename;
    /**
     * 位置代码
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationcode")
    @JsonProperty("eqlocationcode")
    @ApiModelProperty("位置代码")
    private String eqlocationcode;
    /**
     * 机种编码
     */
    @TableField(exist = false)
    @JSONField(name = "machtypecode")
    @JsonProperty("machtypecode")
    @ApiModelProperty("机种编码")
    private String machtypecode;
    /**
     * 上级设备类型
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypepid")
    @JsonProperty("eqtypepid")
    @ApiModelProperty("上级设备类型")
    private String eqtypepid;
    /**
     * 统计归口类型
     */
    @TableField(exist = false)
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @ApiModelProperty("统计归口类型")
    private String sname;
    /**
     * 位置
     */
    @TableField(value = "eqlocationid")
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @ApiModelProperty("位置")
    private String eqlocationid;
    /**
     * 机种
     */
    @TableField(value = "emmachinecategoryid")
    @JSONField(name = "emmachinecategoryid")
    @JsonProperty("emmachinecategoryid")
    @ApiModelProperty("机种")
    private String emmachinecategoryid;
    /**
     * 泊位
     */
    @TableField(value = "emberthid")
    @JSONField(name = "emberthid")
    @JsonProperty("emberthid")
    @ApiModelProperty("泊位")
    private String emberthid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @ApiModelProperty("责任班组")
    private String rteamid;
    /**
     * 品牌
     */
    @TableField(value = "embrandid")
    @JSONField(name = "embrandid")
    @JsonProperty("embrandid")
    @ApiModelProperty("品牌")
    private String embrandid;
    /**
     * 上级设备
     */
    @TableField(value = "equippid")
    @JSONField(name = "equippid")
    @JsonProperty("equippid")
    @ApiModelProperty("上级设备")
    private String equippid;
    /**
     * 产品供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @ApiModelProperty("产品供应商")
    private String labserviceid;
    /**
     * 服务提供商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @ApiModelProperty("服务提供商")
    private String rserviceid;
    /**
     * 机型
     */
    @TableField(value = "emmachmodelid")
    @JSONField(name = "emmachmodelid")
    @JsonProperty("emmachmodelid")
    @ApiModelProperty("机型")
    private String emmachmodelid;
    /**
     * 合同
     */
    @TableField(value = "contractid")
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    @ApiModelProperty("合同")
    private String contractid;
    /**
     * 设备类型
     */
    @TableField(value = "eqtypeid")
    @JSONField(name = "eqtypeid")
    @JsonProperty("eqtypeid")
    @ApiModelProperty("设备类型")
    private String eqtypeid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @ApiModelProperty("总帐科目")
    private String acclassid;
    /**
     * 资产
     */
    @TableField(value = "assetid")
    @JSONField(name = "assetid")
    @JsonProperty("assetid")
    @ApiModelProperty("资产")
    private String assetid;
    /**
     * 制造商
     */
    @TableField(value = "mserviceid")
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @ApiModelProperty("制造商")
    private String mserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAsset asset;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMBerth emberth;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMBrand embrand;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQType eqtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equipp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory emmachinecategory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMMachModel emmachmodel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService mservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFContract contract;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [图片]
     */
    public void setPic9(String pic9) {
        this.pic9 = pic9;
        this.modify("pic9", pic9);
    }

    /**
     * 设置 [强检周期(年)]
     */
    public void setEfcheck(Double efcheck) {
        this.efcheck = efcheck;
        this.modify("efcheck", efcheck);
    }

    /**
     * 设置 [下次检测日期]
     */
    public void setEfcheckndate(Timestamp efcheckndate) {
        this.efcheckndate = efcheckndate;
        this.modify("efcheckndate", efcheckndate);
    }

    /**
     * 格式化日期 [下次检测日期]
     */
    public String formatEfcheckndate() {
        if (this.efcheckndate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(efcheckndate);
    }
    /**
     * 设置 [更换价格]
     */
    public void setReplacecost(String replacecost) {
        this.replacecost = replacecost;
        this.modify("replacecost", replacecost);
    }

    /**
     * 设置 [设备优先级]
     */
    public void setEqpriority(Double eqpriority) {
        this.eqpriority = eqpriority;
        this.modify("eqpriority", eqpriority);
    }

    /**
     * 设置 [利用率(当年)]
     */
    public void setEfficiencyY(Double efficiencyY) {
        this.efficiencyY = efficiencyY;
        this.modify("efficiency_y", efficiencyY);
    }

    /**
     * 设置 [完好率(当年)]
     */
    public void setIntactrateY(Double intactrateY) {
        this.intactrateY = intactrateY;
        this.modify("intactrate_y", intactrateY);
    }

    /**
     * 设置 [完好率(当季度)]
     */
    public void setIntactrateQ(Double intactrateQ) {
        this.intactrateQ = intactrateQ;
        this.modify("intactrate_q", intactrateQ);
    }

    /**
     * 设置 [利用率(当月)]
     */
    public void setEfficiencyM(Double efficiencyM) {
        this.efficiencyM = efficiencyM;
        this.modify("efficiency_m", efficiencyM);
    }

    /**
     * 设置 [设备名称]
     */
    public void setEmequipname(String emequipname) {
        this.emequipname = emequipname;
        this.modify("emequipname", emequipname);
    }

    /**
     * 设置 [强检备注]
     */
    public void setEfcheckdesc(String efcheckdesc) {
        this.efcheckdesc = efcheckdesc;
        this.modify("efcheckdesc", efcheckdesc);
    }

    /**
     * 设置 [故障率(当月)]
     */
    public void setFailurerateM(Double failurerateM) {
        this.failurerateM = failurerateM;
        this.modify("failurerate_m", failurerateM);
    }

    /**
     * 设置 [专责部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [图片]
     */
    public void setPic6(String pic6) {
        this.pic6 = pic6;
        this.modify("pic6", pic6);
    }

    /**
     * 设置 [材料成本]
     */
    public void setMaterialcost(String materialcost) {
        this.materialcost = materialcost;
        this.modify("materialcost", materialcost);
    }

    /**
     * 设置 [停机类型]
     */
    public void setHaltstate(String haltstate) {
        this.haltstate = haltstate;
        this.modify("haltstate", haltstate);
    }

    /**
     * 设置 [完好率(当月)]
     */
    public void setIntactrateM(Double intactrateM) {
        this.intactrateM = intactrateM;
        this.modify("intactrate_m", intactrateM);
    }

    /**
     * 设置 [图片]
     */
    public void setPic8(String pic8) {
        this.pic8 = pic8;
        this.modify("pic8", pic8);
    }

    /**
     * 设置 [购买日期]
     */
    public void setPurchdate(Timestamp purchdate) {
        this.purchdate = purchdate;
        this.modify("purchdate", purchdate);
    }

    /**
     * 格式化日期 [购买日期]
     */
    public String formatPurchdate() {
        if (this.purchdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(purchdate);
    }
    /**
     * 设置 [停机原因]
     */
    public void setHaltcause(String haltcause) {
        this.haltcause = haltcause;
        this.modify("haltcause", haltcause);
    }

    /**
     * 设置 [生产属性]
     */
    public void setProductparam(String productparam) {
        this.productparam = productparam;
        this.modify("productparam", productparam);
    }

    /**
     * 设置 [图片]
     */
    public void setPic4(String pic4) {
        this.pic4 = pic4;
        this.modify("pic4", pic4);
    }

    /**
     * 设置 [集团设备编号]
     */
    public void setEquipBh(String equipBh) {
        this.equipBh = equipBh;
        this.modify("equip_bh", equipBh);
    }

    /**
     * 设置 [维护成本累计]
     */
    public void setMaintenancecost(String maintenancecost) {
        this.maintenancecost = maintenancecost;
        this.modify("maintenancecost", maintenancecost);
    }

    /**
     * 设置 [设备状态]
     */
    public void setEqstate(String eqstate) {
        this.eqstate = eqstate;
        this.modify("eqstate", eqstate);
    }

    /**
     * 设置 [寿命]
     */
    public void setEqlife(Double eqlife) {
        this.eqlife = eqlife;
        this.modify("eqlife", eqlife);
    }

    /**
     * 设置 [初始价格]
     */
    public void setOriginalcost(String originalcost) {
        this.originalcost = originalcost;
        this.modify("originalcost", originalcost);
    }

    /**
     * 设置 [基本参数]
     */
    public void setParams(String params) {
        this.params = params;
        this.modify("params", params);
    }

    /**
     * 设置 [箱量操作量(当季度)]
     */
    public void setOutputrctDj(Double outputrctDj) {
        this.outputrctDj = outputrctDj;
        this.modify("outputrct_dj", outputrctDj);
    }

    /**
     * 设置 [产品系列号]
     */
    public void setEqserialcode(String eqserialcode) {
        this.eqserialcode = eqserialcode;
        this.modify("eqserialcode", eqserialcode);
    }

    /**
     * 设置 [图片]
     */
    public void setPic2(String pic2) {
        this.pic2 = pic2;
        this.modify("pic2", pic2);
    }

    /**
     * 设置 [图片]
     */
    public void setPic3(String pic3) {
        this.pic3 = pic3;
        this.modify("pic3", pic3);
    }

    /**
     * 设置 [设备代码]
     */
    public void setEquipcode(String equipcode) {
        this.equipcode = equipcode;
        this.modify("equipcode", equipcode);
    }

    /**
     * 设置 [箱量操作量(当月)]
     */
    public void setOutputrctDy(Double outputrctDy) {
        this.outputrctDy = outputrctDy;
        this.modify("outputrct_dy", outputrctDy);
    }

    /**
     * 设置 [成本中心]
     */
    public void setCostcenterid(String costcenterid) {
        this.costcenterid = costcenterid;
        this.modify("costcenterid", costcenterid);
    }

    /**
     * 设置 [投运日期]
     */
    public void setEqstartdate(Timestamp eqstartdate) {
        this.eqstartdate = eqstartdate;
        this.modify("eqstartdate", eqstartdate);
    }

    /**
     * 格式化日期 [投运日期]
     */
    public String formatEqstartdate() {
        if (this.eqstartdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(eqstartdate);
    }
    /**
     * 设置 [保修终止日期]
     */
    public void setWarrantydate(Timestamp warrantydate) {
        this.warrantydate = warrantydate;
        this.modify("warrantydate", warrantydate);
    }

    /**
     * 格式化日期 [保修终止日期]
     */
    public String formatWarrantydate() {
        if (this.warrantydate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(warrantydate);
    }
    /**
     * 设置 [统计大型设备]
     */
    public void setEqisservice1(Integer eqisservice1) {
        this.eqisservice1 = eqisservice1;
        this.modify("eqisservice1", eqisservice1);
    }

    /**
     * 设置 [故障率(当季度)]
     */
    public void setFailurerateQ(Double failurerateQ) {
        this.failurerateQ = failurerateQ;
        this.modify("failurerate_q", failurerateQ);
    }

    /**
     * 设置 [图片]
     */
    public void setPic7(String pic7) {
        this.pic7 = pic7;
        this.modify("pic7", pic7);
    }

    /**
     * 设置 [图片]
     */
    public void setPic(String pic) {
        this.pic = pic;
        this.modify("pic", pic);
    }

    /**
     * 设置 [所属系统备注]
     */
    public void setBlsystemdesc(String blsystemdesc) {
        this.blsystemdesc = blsystemdesc;
        this.modify("blsystemdesc", blsystemdesc);
    }

    /**
     * 设置 [利用率(当季度)]
     */
    public void setEfficiencyQ(Double efficiencyQ) {
        this.efficiencyQ = efficiencyQ;
        this.modify("efficiency_q", efficiencyQ);
    }

    /**
     * 设置 [产品型号]
     */
    public void setEqmodelcode(String eqmodelcode) {
        this.eqmodelcode = eqmodelcode;
        this.modify("eqmodelcode", eqmodelcode);
    }

    /**
     * 设置 [图片]
     */
    public void setPic5(String pic5) {
        this.pic5 = pic5;
        this.modify("pic5", pic5);
    }

    /**
     * 设置 [设备备注]
     */
    public void setEquipdesc(String equipdesc) {
        this.equipdesc = equipdesc;
        this.modify("equipdesc", equipdesc);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [人工成本]
     */
    public void setInnerlaborcost(String innerlaborcost) {
        this.innerlaborcost = innerlaborcost;
        this.modify("innerlaborcost", innerlaborcost);
    }

    /**
     * 设置 [关键属性参数]
     */
    public void setKeyattparam(String keyattparam) {
        this.keyattparam = keyattparam;
        this.modify("keyattparam", keyattparam);
    }

    /**
     * 设置 [专责人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [工艺代码]
     */
    public void setTechcode(String techcode) {
        this.techcode = techcode;
        this.modify("techcode", techcode);
    }

    /**
     * 设置 [箱量操作量(当年)]
     */
    public void setOutputrctDn(Double outputrctDn) {
        this.outputrctDn = outputrctDn;
        this.modify("outputrct_dn", outputrctDn);
    }

    /**
     * 设置 [是否在工作]
     */
    public void setEqisservice(Integer eqisservice) {
        this.eqisservice = eqisservice;
        this.modify("eqisservice", eqisservice);
    }

    /**
     * 设置 [故障率(当年)]
     */
    public void setFailurerateY(Double failurerateY) {
        this.failurerateY = failurerateY;
        this.modify("failurerate_y", failurerateY);
    }

    /**
     * 设置 [本次检测日期]
     */
    public void setEfcheckdate(Timestamp efcheckdate) {
        this.efcheckdate = efcheckdate;
        this.modify("efcheckdate", efcheckdate);
    }

    /**
     * 格式化日期 [本次检测日期]
     */
    public String formatEfcheckdate() {
        if (this.efcheckdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(efcheckdate);
    }
    /**
     * 设置 [服务成本]
     */
    public void setForeignlaborcost(String foreignlaborcost) {
        this.foreignlaborcost = foreignlaborcost;
        this.modify("foreignlaborcost", foreignlaborcost);
    }

    /**
     * 设置 [设备分组]
     */
    public void setEquipgroup(Integer equipgroup) {
        this.equipgroup = equipgroup;
        this.modify("equipgroup", equipgroup);
    }

    /**
     * 设置 [专责部门]
     */
    public void setDeptname(String deptname) {
        this.deptname = deptname;
        this.modify("deptname", deptname);
    }

    /**
     * 设置 [本次发证日期]
     */
    public void setEfcheckcdate(Timestamp efcheckcdate) {
        this.efcheckcdate = efcheckcdate;
        this.modify("efcheckcdate", efcheckcdate);
    }

    /**
     * 格式化日期 [本次发证日期]
     */
    public String formatEfcheckcdate() {
        if (this.efcheckcdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(efcheckcdate);
    }
    /**
     * 设置 [总停机时间]
     */
    public void setEqsumstoptime(Double eqsumstoptime) {
        this.eqsumstoptime = eqsumstoptime;
        this.modify("eqsumstoptime", eqsumstoptime);
    }

    /**
     * 设置 [专责人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [位置]
     */
    public void setEqlocationid(String eqlocationid) {
        this.eqlocationid = eqlocationid;
        this.modify("eqlocationid", eqlocationid);
    }

    /**
     * 设置 [机种]
     */
    public void setEmmachinecategoryid(String emmachinecategoryid) {
        this.emmachinecategoryid = emmachinecategoryid;
        this.modify("emmachinecategoryid", emmachinecategoryid);
    }

    /**
     * 设置 [泊位]
     */
    public void setEmberthid(String emberthid) {
        this.emberthid = emberthid;
        this.modify("emberthid", emberthid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [品牌]
     */
    public void setEmbrandid(String embrandid) {
        this.embrandid = embrandid;
        this.modify("embrandid", embrandid);
    }

    /**
     * 设置 [上级设备]
     */
    public void setEquippid(String equippid) {
        this.equippid = equippid;
        this.modify("equippid", equippid);
    }

    /**
     * 设置 [产品供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [服务提供商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [机型]
     */
    public void setEmmachmodelid(String emmachmodelid) {
        this.emmachmodelid = emmachmodelid;
        this.modify("emmachmodelid", emmachmodelid);
    }

    /**
     * 设置 [合同]
     */
    public void setContractid(String contractid) {
        this.contractid = contractid;
        this.modify("contractid", contractid);
    }

    /**
     * 设置 [设备类型]
     */
    public void setEqtypeid(String eqtypeid) {
        this.eqtypeid = eqtypeid;
        this.modify("eqtypeid", eqtypeid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [资产]
     */
    public void setAssetid(String assetid) {
        this.assetid = assetid;
        this.modify("assetid", assetid);
    }

    /**
     * 设置 [制造商]
     */
    public void setMserviceid(String mserviceid) {
        this.mserviceid = mserviceid;
        this.modify("mserviceid", mserviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emequipid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


