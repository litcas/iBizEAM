package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPODetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.impl.EMPOServiceImpl;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

import java.util.*;

/**
 * 实体[订单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMPOExService")
public class EMPOExService extends EMPOServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Arrival:到货] 行为扩展
     *
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPO arrival(EMPO et) {
        // 更新订单状态
        // 根据订单id获取当前订单信息
        et = this.get(et.getEmpoid());
        // 修改订单流程步骤为已关闭
        et.setWfstep(null);
        // 修改订单状态为已关闭(20:已关闭)
        et.setPostate(StaticDict.EMPOSTATE.ITEM_20.getValue());
        Aops.getSelf(this).update(et);

        // 根据订单编号查找当前订单中的订单条目
        EMPODetailSearchContext ctx = new EMPODetailSearchContext();
        ctx.setN_poid_eq(et.getEmpoid());
        List<EMPODetail> empoDetailList = empodetailService.searchDefault(ctx).getContent();
        // 遍历订单条目，修改相关状态信息
        for (EMPODetail empoDetail : empoDetailList) {
            // 修改订单中订单条目的状态(10:验收中)
            empoDetail.setPodetailstate(StaticDict.EMPODETAILSTATE.ITEM_10.getValue());
            // 修改平台内置流程状态(1:流程中)
            empoDetail.setWfstep(StaticDict.EMPODETAILWFSTEP.ITEM_10.getValue());
            empodetailService.update(empoDetail);
        }
        return super.arrival(et);
    }

}

