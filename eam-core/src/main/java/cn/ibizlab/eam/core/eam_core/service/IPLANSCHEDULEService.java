package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULESearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PLANSCHEDULE] 服务对象接口
 */
public interface IPLANSCHEDULEService extends IService<PLANSCHEDULE> {

    boolean create(PLANSCHEDULE et);
    void createBatch(List<PLANSCHEDULE> list);
    boolean update(PLANSCHEDULE et);
    void updateBatch(List<PLANSCHEDULE> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PLANSCHEDULE get(String key);
    PLANSCHEDULE getDraft(PLANSCHEDULE et);
    boolean checkKey(PLANSCHEDULE et);
    PLANSCHEDULE genTask(PLANSCHEDULE et);
    boolean genTaskBatch(List<PLANSCHEDULE> etList);
    boolean save(PLANSCHEDULE et);
    void saveBatch(List<PLANSCHEDULE> list);
    Page<PLANSCHEDULE> searchDefault(PLANSCHEDULESearchContext context);
    Page<PLANSCHEDULE> searchIndexDER(PLANSCHEDULESearchContext context);
    List<PLANSCHEDULE> selectByEmplanid(String emplanid);
    void removeByEmplanid(String emplanid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PLANSCHEDULE> getPlanscheduleByIds(List<String> ids);
    List<PLANSCHEDULE> getPlanscheduleByEntities(List<PLANSCHEDULE> entities);
}


