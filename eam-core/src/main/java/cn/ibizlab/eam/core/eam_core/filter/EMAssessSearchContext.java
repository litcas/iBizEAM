package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssess;
/**
 * 关系型数据实体[EMAssess] 查询条件对象
 */
@Slf4j
@Data
public class EMAssessSearchContext extends QueryWrapperContext<EMAssess> {

	private String n_emassessname_like;//[设备故障考核名称]
	public void setN_emassessname_like(String n_emassessname_like) {
        this.n_emassessname_like = n_emassessname_like;
        if(!ObjectUtils.isEmpty(this.n_emassessname_like)){
            this.getSearchCond().like("emassessname", n_emassessname_like);
        }
    }
	private String n_pfempname_eq;//[填写人]
	public void setN_pfempname_eq(String n_pfempname_eq) {
        this.n_pfempname_eq = n_pfempname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempname_eq)){
            this.getSearchCond().eq("pfempname", n_pfempname_eq);
        }
    }
	private String n_pfempname_like;//[填写人]
	public void setN_pfempname_like(String n_pfempname_like) {
        this.n_pfempname_like = n_pfempname_like;
        if(!ObjectUtils.isEmpty(this.n_pfempname_like)){
            this.getSearchCond().like("pfempname", n_pfempname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_pfempid_eq;//[填写人]
	public void setN_pfempid_eq(String n_pfempid_eq) {
        this.n_pfempid_eq = n_pfempid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempid_eq)){
            this.getSearchCond().eq("pfempid", n_pfempid_eq);
        }
    }
	private String n_pfteamname_eq;//[设备班组]
	public void setN_pfteamname_eq(String n_pfteamname_eq) {
        this.n_pfteamname_eq = n_pfteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_eq)){
            this.getSearchCond().eq("pfteamname", n_pfteamname_eq);
        }
    }
	private String n_pfteamname_like;//[设备班组]
	public void setN_pfteamname_like(String n_pfteamname_like) {
        this.n_pfteamname_like = n_pfteamname_like;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_like)){
            this.getSearchCond().like("pfteamname", n_pfteamname_like);
        }
    }
	private String n_pfteamid_eq;//[设备班组]
	public void setN_pfteamid_eq(String n_pfteamid_eq) {
        this.n_pfteamid_eq = n_pfteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamid_eq)){
            this.getSearchCond().eq("pfteamid", n_pfteamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassessname", query)
            );
		 }
	}
}



