package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPlan;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPlanMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划] 服务对象接口实现
 */
@Slf4j
@Service("EMPlanServiceImpl")
public class EMPlanServiceImpl extends ServiceImpl<EMPlanMapper, EMPlan> implements IEMPlanService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanCDTService emplancdtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanDetailService emplandetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPLANRECORDService emplanrecordService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService planscheduleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanTemplService emplantemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFDeptService pfdeptService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPlan et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplanid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPlan> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPlan et) {
        fillParentData(et);
        emresrefobjService.update(emplanInheritMapping.toEmresrefobj(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emplanid", et.getEmplanid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplanid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPlan> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emresrefobjService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPlan get(String key) {
        EMPlan et = getById(key);
        if(et == null){
            et = new EMPlan();
            et.setEmplanid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPlan getDraft(EMPlan et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPlan et) {
        return (!ObjectUtils.isEmpty(et.getEmplanid())) && (!Objects.isNull(this.getById(et.getEmplanid())));
    }
    @Override
    @Transactional
    public EMPlan createWO(EMPlan et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean createWOBatch(List<EMPlan> etList) {
        for(EMPlan et : etList) {
            createWO(et);
        }
        return true;
    }

    @Override
    @Transactional
    public EMPlan formUpdateByEQUIP(EMPlan et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMPlan et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPlan et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPlan> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlan> create = new ArrayList<>();
        List<EMPlan> update = new ArrayList<>();
        for (EMPlan et : list) {
            if (ObjectUtils.isEmpty(et.getEmplanid()) || ObjectUtils.isEmpty(getById(et.getEmplanid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPlan> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlan> create = new ArrayList<>();
        List<EMPlan> update = new ArrayList<>();
        for (EMPlan et : list) {
            if (ObjectUtils.isEmpty(et.getEmplanid()) || ObjectUtils.isEmpty(getById(et.getEmplanid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPlan> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }
    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMPlan>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMPlan> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMPlan>().eq("equipid",emequipid));
    }

	@Override
    public List<EMPlan> selectByDpid(String emobjectid) {
        return baseMapper.selectByDpid(emobjectid);
    }
    @Override
    public void removeByDpid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlan>().eq("dpid",emobjectid));
    }

	@Override
    public List<EMPlan> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlan>().eq("objid",emobjectid));
    }

	@Override
    public List<EMPlan> selectByPlantemplid(String emplantemplid) {
        return baseMapper.selectByPlantemplid(emplantemplid);
    }
    @Override
    public void removeByPlantemplid(String emplantemplid) {
        this.remove(new QueryWrapper<EMPlan>().eq("plantemplid",emplantemplid));
    }

	@Override
    public List<EMPlan> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }
    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMPlan>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMPlan> selectByRdeptid(String pfdeptid) {
        return baseMapper.selectByRdeptid(pfdeptid);
    }
    @Override
    public void removeByRdeptid(String pfdeptid) {
        this.remove(new QueryWrapper<EMPlan>().eq("rdeptid",pfdeptid));
    }

	@Override
    public List<EMPlan> selectByMpersonid(String pfempid) {
        return baseMapper.selectByMpersonid(pfempid);
    }
    @Override
    public void removeByMpersonid(String pfempid) {
        this.remove(new QueryWrapper<EMPlan>().eq("mpersonid",pfempid));
    }

	@Override
    public List<EMPlan> selectByRempid(String pfempid) {
        return baseMapper.selectByRempid(pfempid);
    }
    @Override
    public void removeByRempid(String pfempid) {
        this.remove(new QueryWrapper<EMPlan>().eq("rempid",pfempid));
    }

	@Override
    public List<EMPlan> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }
    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMPlan>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPlan> searchDefault(EMPlanSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPlan> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPlan>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPlan et){
        //实体关系[DER1N_EMPLAN_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMPLAN_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEmequipname());
        }
        //实体关系[DER1N_EMPLAN_EMOBJECT_DPID]
        if(!ObjectUtils.isEmpty(et.getDpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject dp=et.getDp();
            if(ObjectUtils.isEmpty(dp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getDpid());
                et.setDp(majorEntity);
                dp=majorEntity;
            }
            et.setDpname(dp.getEmobjectname());
            et.setDptype(dp.getEmobjecttype());
        }
        //实体关系[DER1N_EMPLAN_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMPLAN_EMPLANTEMPL_PLANTEMPLID]
        if(!ObjectUtils.isEmpty(et.getPlantemplid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl plantempl=et.getPlantempl();
            if(ObjectUtils.isEmpty(plantempl)){
                cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl majorEntity=emplantemplService.get(et.getPlantemplid());
                et.setPlantempl(majorEntity);
                plantempl=majorEntity;
            }
            et.setPlantemplname(plantempl.getEmplantemplname());
        }
        //实体关系[DER1N_EMPLAN_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMPLAN_PFDEPT_RDEPTID]
        if(!ObjectUtils.isEmpty(et.getRdeptid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid=et.getPfdeptid();
            if(ObjectUtils.isEmpty(pfdeptid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFDept majorEntity=pfdeptService.get(et.getRdeptid());
                et.setPfdeptid(majorEntity);
                pfdeptid=majorEntity;
            }
            et.setRdeptname(pfdeptid.getDeptinfo());
        }
        //实体关系[DER1N_EMPLAN_PFEMP_MPERSONID]
        if(!ObjectUtils.isEmpty(et.getMpersonid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempersonid=et.getPfempersonid();
            if(ObjectUtils.isEmpty(pfempersonid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getMpersonid());
                et.setPfempersonid(majorEntity);
                pfempersonid=majorEntity;
            }
            et.setMpersonname(pfempersonid.getEmpinfo());
        }
        //实体关系[DER1N_EMPLAN_PFEMP_REMPID]
        if(!ObjectUtils.isEmpty(et.getRempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid=et.getPfempid();
            if(ObjectUtils.isEmpty(pfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getRempid());
                et.setPfempid(majorEntity);
                pfempid=majorEntity;
            }
            et.setRempname(pfempid.getEmpinfo());
        }
        //实体关系[DER1N_EMPLAN_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMPlanInheritMapping emplanInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMPlan et){
        if(ObjectUtils.isEmpty(et.getEmplanid()))
            et.setEmplanid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMResRefObj emresrefobj =emplanInheritMapping.toEmresrefobj(et);
        emresrefobj.set("emresrefobjtype","PLAN");
        emresrefobjService.create(emresrefobj);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPlan> getEmplanByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPlan> getEmplanByEntities(List<EMPlan> entities) {
        List ids =new ArrayList();
        for(EMPlan entity : entities){
            Serializable id=entity.getEmplanid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPlanService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



