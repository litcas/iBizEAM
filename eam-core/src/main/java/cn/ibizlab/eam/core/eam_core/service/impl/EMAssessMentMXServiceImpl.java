package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMentMX;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMentMXSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMentMXService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMAssessMentMXMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划及项目进程考核明细] 服务对象接口实现
 */
@Slf4j
@Service("EMAssessMentMXServiceImpl")
public class EMAssessMentMXServiceImpl extends ServiceImpl<EMAssessMentMXMapper, EMAssessMentMX> implements IEMAssessMentMXService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessMentService emassessmentService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMAssessMentMX et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassessmentmxid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMAssessMentMX> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMAssessMentMX et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emassessmentmxid", et.getEmassessmentmxid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassessmentmxid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMAssessMentMX> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMAssessMentMX get(String key) {
        EMAssessMentMX et = getById(key);
        if(et == null){
            et = new EMAssessMentMX();
            et.setEmassessmentmxid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMAssessMentMX getDraft(EMAssessMentMX et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMAssessMentMX et) {
        return (!ObjectUtils.isEmpty(et.getEmassessmentmxid())) && (!Objects.isNull(this.getById(et.getEmassessmentmxid())));
    }
    @Override
    @Transactional
    public boolean save(EMAssessMentMX et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMAssessMentMX et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMAssessMentMX> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssessMentMX> create = new ArrayList<>();
        List<EMAssessMentMX> update = new ArrayList<>();
        for (EMAssessMentMX et : list) {
            if (ObjectUtils.isEmpty(et.getEmassessmentmxid()) || ObjectUtils.isEmpty(getById(et.getEmassessmentmxid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMAssessMentMX> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssessMentMX> create = new ArrayList<>();
        List<EMAssessMentMX> update = new ArrayList<>();
        for (EMAssessMentMX et : list) {
            if (ObjectUtils.isEmpty(et.getEmassessmentmxid()) || ObjectUtils.isEmpty(getById(et.getEmassessmentmxid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMAssessMentMX> selectByEmassessmentid(String emassessmentid) {
        return baseMapper.selectByEmassessmentid(emassessmentid);
    }
    @Override
    public void removeByEmassessmentid(String emassessmentid) {
        this.remove(new QueryWrapper<EMAssessMentMX>().eq("emassessmentid",emassessmentid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMAssessMentMX> searchDefault(EMAssessMentMXSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMAssessMentMX> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMAssessMentMX>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMAssessMentMX et){
        //实体关系[DER1N_EMASSESSMENTMX_EMASSESSMENT_EMASSESSMENTID]
        if(!ObjectUtils.isEmpty(et.getEmassessmentid())){
            cn.ibizlab.eam.core.eam_core.domain.EMAssessMent emassessment=et.getEmassessment();
            if(ObjectUtils.isEmpty(emassessment)){
                cn.ibizlab.eam.core.eam_core.domain.EMAssessMent majorEntity=emassessmentService.get(et.getEmassessmentid());
                et.setEmassessment(majorEntity);
                emassessment=majorEntity;
            }
            et.setEmassessmentname(emassessment.getEmassessmentname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMAssessMentMX> getEmassessmentmxByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMAssessMentMX> getEmassessmentmxByEntities(List<EMAssessMentMX> entities) {
        List ids =new ArrayList();
        for(EMAssessMentMX entity : entities){
            Serializable id=entity.getEmassessmentmxid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMAssessMentMXService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



