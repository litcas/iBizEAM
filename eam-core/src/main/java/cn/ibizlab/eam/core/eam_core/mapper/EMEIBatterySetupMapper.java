package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySetupSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEIBatterySetupMapper extends BaseMapper<EMEIBatterySetup> {

    Page<EMEIBatterySetup> searchDefault(IPage page, @Param("srf") EMEIBatterySetupSearchContext context, @Param("ew") Wrapper<EMEIBatterySetup> wrapper);
    @Override
    EMEIBatterySetup selectById(Serializable id);
    @Override
    int insert(EMEIBatterySetup entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEIBatterySetup entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEIBatterySetup entity, @Param("ew") Wrapper<EMEIBatterySetup> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEIBatterySetup> selectByEiobjid(@Param("emeibatteryid") Serializable emeibatteryid);

    List<EMEIBatterySetup> selectByEiobjnid(@Param("emeibatteryid") Serializable emeibatteryid);

    List<EMEIBatterySetup> selectByEqlocationid(@Param("emeqlocationid") Serializable emeqlocationid);

    List<EMEIBatterySetup> selectByEquipid(@Param("emequipid") Serializable emequipid);

}
