package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceEvlSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMServiceEvl] 服务对象接口
 */
public interface IEMServiceEvlService extends IService<EMServiceEvl> {

    boolean create(EMServiceEvl et);
    void createBatch(List<EMServiceEvl> list);
    boolean update(EMServiceEvl et);
    void updateBatch(List<EMServiceEvl> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMServiceEvl get(String key);
    EMServiceEvl getDraft(EMServiceEvl et);
    boolean checkKey(EMServiceEvl et);
    EMServiceEvl confirm(EMServiceEvl et);
    boolean confirmBatch(List<EMServiceEvl> etList);
    EMServiceEvl rejected(EMServiceEvl et);
    boolean save(EMServiceEvl et);
    void saveBatch(List<EMServiceEvl> list);
    EMServiceEvl submit(EMServiceEvl et);
    Page<EMServiceEvl> searchConfirmed(EMServiceEvlSearchContext context);
    Page<EMServiceEvl> searchDefault(EMServiceEvlSearchContext context);
    Page<EMServiceEvl> searchDraft(EMServiceEvlSearchContext context);
    Page<Map> searchEvaluateTop5(EMServiceEvlSearchContext context);
    Page<Map> searchOverallEVL(EMServiceEvlSearchContext context);
    Page<EMServiceEvl> searchToConfirm(EMServiceEvlSearchContext context);
    List<EMServiceEvl> selectByServiceid(String emserviceid);
    void removeByServiceid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMServiceEvl> getEmserviceevlByIds(List<String> ids);
    List<EMServiceEvl> getEmserviceevlByEntities(List<EMServiceEvl> entities);
}


