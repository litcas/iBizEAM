package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFEmpPostMap;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpPostMapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFEmpPostMap] 服务对象接口
 */
public interface IPFEmpPostMapService extends IService<PFEmpPostMap> {

    boolean create(PFEmpPostMap et);
    void createBatch(List<PFEmpPostMap> list);
    boolean update(PFEmpPostMap et);
    void updateBatch(List<PFEmpPostMap> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFEmpPostMap get(String key);
    PFEmpPostMap getDraft(PFEmpPostMap et);
    boolean checkKey(PFEmpPostMap et);
    boolean save(PFEmpPostMap et);
    void saveBatch(List<PFEmpPostMap> list);
    Page<PFEmpPostMap> searchDefault(PFEmpPostMapSearchContext context);
    List<PFEmpPostMap> selectByPostid(String pfpostid);
    void removeByPostid(String pfpostid);
    List<PFEmpPostMap> selectByTeamid(String pfteamid);
    void removeByTeamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFEmpPostMap> getPfemppostmapByIds(List<String> ids);
    List<PFEmpPostMap> getPfemppostmapByEntities(List<PFEmpPostMap> entities);
}


