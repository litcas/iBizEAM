package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMDisableAssess;
import cn.ibizlab.eam.core.eam_core.filter.EMDisableAssessSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMDisableAssessService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMDisableAssessMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备停用考核] 服务对象接口实现
 */
@Slf4j
@Service("EMDisableAssessServiceImpl")
public class EMDisableAssessServiceImpl extends ServiceImpl<EMDisableAssessMapper, EMDisableAssess> implements IEMDisableAssessService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessMXService emassessmxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMDisableAssess et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdisableassessid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMDisableAssess> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMDisableAssess et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emdisableassessid", et.getEmdisableassessid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdisableassessid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMDisableAssess> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMDisableAssess get(String key) {
        EMDisableAssess et = getById(key);
        if(et == null){
            et = new EMDisableAssess();
            et.setEmdisableassessid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMDisableAssess getDraft(EMDisableAssess et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMDisableAssess et) {
        return (!ObjectUtils.isEmpty(et.getEmdisableassessid())) && (!Objects.isNull(this.getById(et.getEmdisableassessid())));
    }
    @Override
    @Transactional
    public boolean save(EMDisableAssess et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMDisableAssess et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMDisableAssess> list) {
        list.forEach(item->fillParentData(item));
        List<EMDisableAssess> create = new ArrayList<>();
        List<EMDisableAssess> update = new ArrayList<>();
        for (EMDisableAssess et : list) {
            if (ObjectUtils.isEmpty(et.getEmdisableassessid()) || ObjectUtils.isEmpty(getById(et.getEmdisableassessid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMDisableAssess> list) {
        list.forEach(item->fillParentData(item));
        List<EMDisableAssess> create = new ArrayList<>();
        List<EMDisableAssess> update = new ArrayList<>();
        for (EMDisableAssess et : list) {
            if (ObjectUtils.isEmpty(et.getEmdisableassessid()) || ObjectUtils.isEmpty(getById(et.getEmdisableassessid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMDisableAssess> selectByPfteamid(String pfteamid) {
        return baseMapper.selectByPfteamid(pfteamid);
    }
    @Override
    public void removeByPfteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMDisableAssess>().eq("pfteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMDisableAssess> searchDefault(EMDisableAssessSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMDisableAssess> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMDisableAssess>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMDisableAssess et){
        //实体关系[DER1N_EMDISABLEASSESS_PFTEAM_PFTEAMID]
        if(!ObjectUtils.isEmpty(et.getPfteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam=et.getPfteam();
            if(ObjectUtils.isEmpty(pfteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getPfteamid());
                et.setPfteam(majorEntity);
                pfteam=majorEntity;
            }
            et.setPfteamname(pfteam.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMDisableAssess> getEmdisableassessByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMDisableAssess> getEmdisableassessByEntities(List<EMDisableAssess> entities) {
        List ids =new ArrayList();
        for(EMDisableAssess entity : entities){
            Serializable id=entity.getEmdisableassessid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMDisableAssessService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



