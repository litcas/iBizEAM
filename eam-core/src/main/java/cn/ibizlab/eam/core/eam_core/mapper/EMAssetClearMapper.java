package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClear;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClearSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMAssetClearMapper extends BaseMapper<EMAssetClear> {

    Page<EMAssetClear> searchDefault(IPage page, @Param("srf") EMAssetClearSearchContext context, @Param("ew") Wrapper<EMAssetClear> wrapper);
    @Override
    EMAssetClear selectById(Serializable id);
    @Override
    int insert(EMAssetClear entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMAssetClear entity);
    @Override
    int update(@Param(Constants.ENTITY) EMAssetClear entity, @Param("ew") Wrapper<EMAssetClear> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMAssetClear> selectByEmassetid(@Param("emassetid") Serializable emassetid);

}
