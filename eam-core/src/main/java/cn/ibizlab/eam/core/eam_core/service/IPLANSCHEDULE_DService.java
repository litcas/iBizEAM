package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_D;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_DSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PLANSCHEDULE_D] 服务对象接口
 */
public interface IPLANSCHEDULE_DService extends IService<PLANSCHEDULE_D> {

    boolean create(PLANSCHEDULE_D et);
    void createBatch(List<PLANSCHEDULE_D> list);
    boolean update(PLANSCHEDULE_D et);
    void updateBatch(List<PLANSCHEDULE_D> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PLANSCHEDULE_D get(String key);
    PLANSCHEDULE_D getDraft(PLANSCHEDULE_D et);
    boolean checkKey(PLANSCHEDULE_D et);
    boolean save(PLANSCHEDULE_D et);
    void saveBatch(List<PLANSCHEDULE_D> list);
    Page<PLANSCHEDULE_D> searchDefault(PLANSCHEDULE_DSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PLANSCHEDULE_D> getPlanscheduleDByIds(List<String> ids);
    List<PLANSCHEDULE_D> getPlanscheduleDByEntities(List<PLANSCHEDULE_D> entities);
}


