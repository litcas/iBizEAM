package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpare;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQSpareMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[备件包] 服务对象接口实现
 */
@Slf4j
@Service("EMEQSpareServiceImpl")
public class EMEQSpareServiceImpl extends ServiceImpl<EMEQSpareMapper, EMEQSpare> implements IEMEQSpareService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSpareDetailService emeqsparedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSpareMapService emeqsparemapService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQSpare et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqspareid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQSpare> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQSpare et) {
        emobjectService.update(emeqspareInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqspareid", et.getEmeqspareid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqspareid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQSpare> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQSpare get(String key) {
        EMEQSpare et = getById(key);
        if(et == null){
            et = new EMEQSpare();
            et.setEmeqspareid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQSpare getDraft(EMEQSpare et) {
        return et;
    }

    @Override
    public boolean checkKey(EMEQSpare et) {
        return (!ObjectUtils.isEmpty(et.getEmeqspareid())) && (!Objects.isNull(this.getById(et.getEmeqspareid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQSpare et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQSpare et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQSpare> list) {
        List<EMEQSpare> create = new ArrayList<>();
        List<EMEQSpare> update = new ArrayList<>();
        for (EMEQSpare et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqspareid()) || ObjectUtils.isEmpty(getById(et.getEmeqspareid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQSpare> list) {
        List<EMEQSpare> create = new ArrayList<>();
        List<EMEQSpare> update = new ArrayList<>();
        for (EMEQSpare et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqspareid()) || ObjectUtils.isEmpty(getById(et.getEmeqspareid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQSpare> searchDefault(EMEQSpareSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSpare> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSpare>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQSpareInheritMapping emeqspareInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQSpare et){
        if(ObjectUtils.isEmpty(et.getEmeqspareid()))
            et.setEmeqspareid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emeqspareInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","SPARE");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQSpare> getEmeqspareByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQSpare> getEmeqspareByEntities(List<EMEQSpare> entities) {
        List ids =new ArrayList();
        for(EMEQSpare entity : entities){
            Serializable id=entity.getEmeqspareid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQSpareService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



