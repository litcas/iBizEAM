package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWGSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWGService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMDRWGMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[文档] 服务对象接口实现
 */
@Slf4j
@Service("EMDRWGServiceImpl")
public class EMDRWGServiceImpl extends ServiceImpl<EMDRWGMapper, EMDRWG> implements IEMDRWGService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDRWGMapService emdrwgmapService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMDRWG et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdrwgid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMDRWG> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMDRWG et) {
        emobjectService.update(emdrwgInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emdrwgid", et.getEmdrwgid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdrwgid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMDRWG> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMDRWG get(String key) {
        EMDRWG et = getById(key);
        if(et == null){
            et = new EMDRWG();
            et.setEmdrwgid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMDRWG getDraft(EMDRWG et) {
        return et;
    }

    @Override
    public boolean checkKey(EMDRWG et) {
        return (!ObjectUtils.isEmpty(et.getEmdrwgid())) && (!Objects.isNull(this.getById(et.getEmdrwgid())));
    }
    @Override
    @Transactional
    public EMDRWG genId(EMDRWG et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean genIdBatch(List<EMDRWG> etList) {
        for(EMDRWG et : etList) {
            genId(et);
        }
        return true;
    }

    @Override
    @Transactional
    public boolean save(EMDRWG et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMDRWG et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMDRWG> list) {
        List<EMDRWG> create = new ArrayList<>();
        List<EMDRWG> update = new ArrayList<>();
        for (EMDRWG et : list) {
            if (ObjectUtils.isEmpty(et.getEmdrwgid()) || ObjectUtils.isEmpty(getById(et.getEmdrwgid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMDRWG> list) {
        List<EMDRWG> create = new ArrayList<>();
        List<EMDRWG> update = new ArrayList<>();
        for (EMDRWG et : list) {
            if (ObjectUtils.isEmpty(et.getEmdrwgid()) || ObjectUtils.isEmpty(getById(et.getEmdrwgid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMDRWG> searchDefault(EMDRWGSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMDRWG> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMDRWG>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMDRWGInheritMapping emdrwgInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMDRWG et){
        if(ObjectUtils.isEmpty(et.getEmdrwgid()))
            et.setEmdrwgid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emdrwgInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","DRWG");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMDRWG> getEmdrwgByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMDRWG> getEmdrwgByEntities(List<EMDRWG> entities) {
        List ids =new ArrayList();
        for(EMDRWG entity : entities){
            Serializable id=entity.getEmdrwgid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMDRWGService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



