package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMResService;
import cn.ibizlab.eam.core.eam_core.filter.EMResServiceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMResService] 服务对象接口
 */
public interface IEMResServiceService extends IService<EMResService> {

    boolean create(EMResService et);
    void createBatch(List<EMResService> list);
    boolean update(EMResService et);
    void updateBatch(List<EMResService> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMResService get(String key);
    EMResService getDraft(EMResService et);
    boolean checkKey(EMResService et);
    boolean save(EMResService et);
    void saveBatch(List<EMResService> list);
    Page<EMResService> searchDefault(EMResServiceSearchContext context);
    List<EMResService> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMResService> selectByResrefobjid(String emresrefobjid);
    void removeByResrefobjid(String emresrefobjid);
    List<EMResService> selectByResid(String emserviceid);
    void removeByResid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMResService> getEmresserviceByIds(List<String> ids);
    List<EMResService> getEmresserviceByEntities(List<EMResService> entities);
}


