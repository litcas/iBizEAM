package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFDept;
/**
 * 关系型数据实体[PFDept] 查询条件对象
 */
@Slf4j
@Data
public class PFDeptSearchContext extends QueryWrapperContext<PFDept> {

	private String n_mgrempname_eq;//[主管]
	public void setN_mgrempname_eq(String n_mgrempname_eq) {
        this.n_mgrempname_eq = n_mgrempname_eq;
        if(!ObjectUtils.isEmpty(this.n_mgrempname_eq)){
            this.getSearchCond().eq("mgrempname", n_mgrempname_eq);
        }
    }
	private String n_mgrempname_like;//[主管]
	public void setN_mgrempname_like(String n_mgrempname_like) {
        this.n_mgrempname_like = n_mgrempname_like;
        if(!ObjectUtils.isEmpty(this.n_mgrempname_like)){
            this.getSearchCond().like("mgrempname", n_mgrempname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_deptinfo_like;//[部门信息]
	public void setN_deptinfo_like(String n_deptinfo_like) {
        this.n_deptinfo_like = n_deptinfo_like;
        if(!ObjectUtils.isEmpty(this.n_deptinfo_like)){
            this.getSearchCond().like("deptinfo", n_deptinfo_like);
        }
    }
	private String n_deptpid_eq;//[上级部门]
	public void setN_deptpid_eq(String n_deptpid_eq) {
        this.n_deptpid_eq = n_deptpid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptpid_eq)){
            this.getSearchCond().eq("deptpid", n_deptpid_eq);
        }
    }
	private String n_mgrempid_eq;//[主管]
	public void setN_mgrempid_eq(String n_mgrempid_eq) {
        this.n_mgrempid_eq = n_mgrempid_eq;
        if(!ObjectUtils.isEmpty(this.n_mgrempid_eq)){
            this.getSearchCond().eq("mgrempid", n_mgrempid_eq);
        }
    }
	private String n_pfdeptname_like;//[部门名称]
	public void setN_pfdeptname_like(String n_pfdeptname_like) {
        this.n_pfdeptname_like = n_pfdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_pfdeptname_like)){
            this.getSearchCond().like("pfdeptname", n_pfdeptname_like);
        }
    }
	private Integer n_sdept_eq;//[统计归口部门]
	public void setN_sdept_eq(Integer n_sdept_eq) {
        this.n_sdept_eq = n_sdept_eq;
        if(!ObjectUtils.isEmpty(this.n_sdept_eq)){
            this.getSearchCond().eq("sdept", n_sdept_eq);
        }
    }
	private String n_deptpname_eq;//[上级部门]
	public void setN_deptpname_eq(String n_deptpname_eq) {
        this.n_deptpname_eq = n_deptpname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptpname_eq)){
            this.getSearchCond().eq("deptpname", n_deptpname_eq);
        }
    }
	private String n_deptpname_like;//[上级部门]
	public void setN_deptpname_like(String n_deptpname_like) {
        this.n_deptpname_like = n_deptpname_like;
        if(!ObjectUtils.isEmpty(this.n_deptpname_like)){
            this.getSearchCond().like("deptpname", n_deptpname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfdeptname", query)
            );
		 }
	}
}



