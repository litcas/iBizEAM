package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMResItem;
import cn.ibizlab.eam.core.eam_core.filter.EMResItemSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMResItem] 服务对象接口
 */
public interface IEMResItemService extends IService<EMResItem> {

    boolean create(EMResItem et);
    void createBatch(List<EMResItem> list);
    boolean update(EMResItem et);
    void updateBatch(List<EMResItem> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMResItem get(String key);
    EMResItem getDraft(EMResItem et);
    boolean checkKey(EMResItem et);
    boolean save(EMResItem et);
    void saveBatch(List<EMResItem> list);
    Page<Map> searchAmountByTypeByEQ(EMResItemSearchContext context);
    Page<EMResItem> searchDefault(EMResItemSearchContext context);
    Page<EMResItem> searchUsedByEQ(EMResItemSearchContext context);
    Page<EMResItem> searchUsedByItem(EMResItemSearchContext context);
    List<EMResItem> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMResItem> selectByResid(String emitemid);
    void removeByResid(String emitemid);
    List<EMResItem> selectByResrefobjid(String emresrefobjid);
    void removeByResrefobjid(String emresrefobjid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMResItem> getEmresitemByIds(List<String> ids);
    List<EMResItem> getEmresitemByEntities(List<EMResItem> entities);
}


