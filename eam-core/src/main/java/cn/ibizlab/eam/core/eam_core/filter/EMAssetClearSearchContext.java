package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClear;
/**
 * 关系型数据实体[EMAssetClear] 查询条件对象
 */
@Slf4j
@Data
public class EMAssetClearSearchContext extends QueryWrapperContext<EMAssetClear> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emassetclearname_like;//[资产清盘记录名称]
	public void setN_emassetclearname_like(String n_emassetclearname_like) {
        this.n_emassetclearname_like = n_emassetclearname_like;
        if(!ObjectUtils.isEmpty(this.n_emassetclearname_like)){
            this.getSearchCond().like("emassetclearname", n_emassetclearname_like);
        }
    }
	private String n_emassetname_eq;//[资产]
	public void setN_emassetname_eq(String n_emassetname_eq) {
        this.n_emassetname_eq = n_emassetname_eq;
        if(!ObjectUtils.isEmpty(this.n_emassetname_eq)){
            this.getSearchCond().eq("emassetname", n_emassetname_eq);
        }
    }
	private String n_emassetname_like;//[资产]
	public void setN_emassetname_like(String n_emassetname_like) {
        this.n_emassetname_like = n_emassetname_like;
        if(!ObjectUtils.isEmpty(this.n_emassetname_like)){
            this.getSearchCond().like("emassetname", n_emassetname_like);
        }
    }
	private String n_emassetid_eq;//[资产]
	public void setN_emassetid_eq(String n_emassetid_eq) {
        this.n_emassetid_eq = n_emassetid_eq;
        if(!ObjectUtils.isEmpty(this.n_emassetid_eq)){
            this.getSearchCond().eq("emassetid", n_emassetid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassetclearname", query)
            );
		 }
	}
}



