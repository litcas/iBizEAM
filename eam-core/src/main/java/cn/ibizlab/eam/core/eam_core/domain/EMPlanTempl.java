package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPLANTEMPL_BASE", resultMap = "EMPlanTemplResultMap")
@ApiModel("计划模板")
public class EMPlanTempl extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 计划模板信息
     */
    @TableField(exist = false)
    @JSONField(name = "plantemplinfo")
    @JsonProperty("plantemplinfo")
    @ApiModelProperty("计划模板信息")
    private String plantemplinfo;
    /**
     * 制定人
     */
    @TableField(value = "mpersonname")
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @ApiModelProperty("制定人")
    private String mpersonname;
    /**
     * 计划模板名称
     */
    @TableField(value = "emplantemplname")
    @JSONField(name = "emplantemplname")
    @JsonProperty("emplantemplname")
    @ApiModelProperty("计划模板名称")
    private String emplantemplname;
    /**
     * 责任人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("责任人")
    private String rempname;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @ApiModelProperty("预算(￥)")
    private String prefee;
    /**
     * 持续时间(H)
     */
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 计划内容
     */
    @TableField(value = "plandesc")
    @JSONField(name = "plandesc")
    @JsonProperty("plandesc")
    @ApiModelProperty("计划内容")
    private String plandesc;
    /**
     * 制定人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @ApiModelProperty("制定人")
    private String mpersonid;
    /**
     * 计划类型
     */
    @TableField(value = "plantype")
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    @ApiModelProperty("计划类型")
    private String plantype;
    /**
     * 制定时间
     */
    @TableField(value = "mdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "mdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    @ApiModelProperty("制定时间")
    private Timestamp mdate;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @ApiModelProperty("接收人")
    private String recvpersonid;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;
    /**
     * 计划模板编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emplantemplid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emplantemplid")
    @JsonProperty("emplantemplid")
    @ApiModelProperty("计划模板编号")
    private String emplantemplid;
    /**
     * 多任务?
     */
    @DEField(defaultValue = "0")
    @TableField(value = "mtflag")
    @JSONField(name = "mtflag")
    @JsonProperty("mtflag")
    @ApiModelProperty("多任务?")
    private Integer mtflag;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    @ApiModelProperty("详细内容")
    private String content;
    /**
     * 生成工单种类
     */
    @DEField(defaultValue = "INNER")
    @TableField(value = "emwotype")
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @ApiModelProperty("生成工单种类")
    private String emwotype;
    /**
     * 计划周期(天)
     */
    @TableField(value = "plancvl")
    @JSONField(name = "plancvl")
    @JsonProperty("plancvl")
    @ApiModelProperty("计划周期(天)")
    private Double plancvl;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @ApiModelProperty("归档")
    private String archive;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptname")
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @ApiModelProperty("责任部门")
    private String rdeptname;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("责任人")
    private String rempid;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @ApiModelProperty("责任部门")
    private String rdeptid;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonname")
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @ApiModelProperty("接收人")
    private String recvpersonname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 计划状态
     */
    @DEField(defaultValue = "1")
    @TableField(value = "planstate")
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    @ApiModelProperty("计划状态")
    private Integer planstate;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @ApiModelProperty("服务商")
    private String rservicename;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @ApiModelProperty("责任班组")
    private String rteamname;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @ApiModelProperty("总帐科目")
    private String acclassname;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @ApiModelProperty("总帐科目")
    private String acclassid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @ApiModelProperty("服务商")
    private String rserviceid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @ApiModelProperty("责任班组")
    private String rteamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [制定人]
     */
    public void setMpersonname(String mpersonname) {
        this.mpersonname = mpersonname;
        this.modify("mpersonname", mpersonname);
    }

    /**
     * 设置 [计划模板名称]
     */
    public void setEmplantemplname(String emplantemplname) {
        this.emplantemplname = emplantemplname;
        this.modify("emplantemplname", emplantemplname);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(String prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [计划内容]
     */
    public void setPlandesc(String plandesc) {
        this.plandesc = plandesc;
        this.modify("plandesc", plandesc);
    }

    /**
     * 设置 [制定人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [计划类型]
     */
    public void setPlantype(String plantype) {
        this.plantype = plantype;
        this.modify("plantype", plantype);
    }

    /**
     * 设置 [制定时间]
     */
    public void setMdate(Timestamp mdate) {
        this.mdate = mdate;
        this.modify("mdate", mdate);
    }

    /**
     * 格式化日期 [制定时间]
     */
    public String formatMdate() {
        if (this.mdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(mdate);
    }
    /**
     * 设置 [接收人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }

    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [多任务?]
     */
    public void setMtflag(Integer mtflag) {
        this.mtflag = mtflag;
        this.modify("mtflag", mtflag);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [生成工单种类]
     */
    public void setEmwotype(String emwotype) {
        this.emwotype = emwotype;
        this.modify("emwotype", emwotype);
    }

    /**
     * 设置 [计划周期(天)]
     */
    public void setPlancvl(Double plancvl) {
        this.plancvl = plancvl;
        this.modify("plancvl", plancvl);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptname(String rdeptname) {
        this.rdeptname = rdeptname;
        this.modify("rdeptname", rdeptname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonname(String recvpersonname) {
        this.recvpersonname = recvpersonname;
        this.modify("recvpersonname", recvpersonname);
    }

    /**
     * 设置 [计划状态]
     */
    public void setPlanstate(Integer planstate) {
        this.planstate = planstate;
        this.modify("planstate", planstate);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emplantemplid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


