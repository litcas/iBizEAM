package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICellSetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellSetupSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICellSetup] 服务对象接口
 */
public interface IEMEICellSetupService extends IService<EMEICellSetup> {

    boolean create(EMEICellSetup et);
    void createBatch(List<EMEICellSetup> list);
    boolean update(EMEICellSetup et);
    void updateBatch(List<EMEICellSetup> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICellSetup get(String key);
    EMEICellSetup getDraft(EMEICellSetup et);
    boolean checkKey(EMEICellSetup et);
    boolean save(EMEICellSetup et);
    void saveBatch(List<EMEICellSetup> list);
    Page<EMEICellSetup> searchDefault(EMEICellSetupSearchContext context);
    List<EMEICellSetup> selectByEiobjid(String emeicellid);
    void removeByEiobjid(String emeicellid);
    List<EMEICellSetup> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEICellSetup> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICellSetup> getEmeicellsetupByIds(List<String> ids);
    List<EMEICellSetup> getEmeicellsetupByEntities(List<EMEICellSetup> entities);
}


