package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[材料申购清单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMSUBLIST_BASE", resultMap = "EMSubListResultMap")
@ApiModel("材料申购清单")
public class EMSubList extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 材料申购预控清单标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emsublistid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emsublistid")
    @JsonProperty("emsublistid")
    @ApiModelProperty("材料申购预控清单标识")
    private String emsublistid;
    /**
     * 时间
     */
    @TableField(value = "time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "time", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("time")
    @ApiModelProperty("时间")
    private Timestamp time;
    /**
     * 型号
     */
    @TableField(value = "version")
    @JSONField(name = "version")
    @JsonProperty("version")
    @ApiModelProperty("型号")
    private String version;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 材料配件名称
     */
    @TableField(value = "emsublistname")
    @JSONField(name = "emsublistname")
    @JsonProperty("emsublistname")
    @ApiModelProperty("材料配件名称")
    private String emsublistname;
    /**
     * 初步审核
     */
    @TableField(value = "preliminary")
    @JSONField(name = "preliminary")
    @JsonProperty("preliminary")
    @ApiModelProperty("初步审核")
    private String preliminary;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 说明
     */
    @TableField(value = "remarks")
    @JSONField(name = "remarks")
    @JsonProperty("remarks")
    @ApiModelProperty("说明")
    private String remarks;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 用途
     */
    @TableField(value = "application")
    @JSONField(name = "application")
    @JsonProperty("application")
    @ApiModelProperty("用途")
    private String application;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @ApiModelProperty("班组")
    private String pfteamname;
    /**
     * 班组
     */
    @TableField(value = "pfteamid")
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @ApiModelProperty("班组")
    private String pfteamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam;



    /**
     * 设置 [时间]
     */
    public void setTime(Timestamp time) {
        this.time = time;
        this.modify("time", time);
    }

    /**
     * 格式化日期 [时间]
     */
    public String formatTime() {
        if (this.time == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }
    /**
     * 设置 [型号]
     */
    public void setVersion(String version) {
        this.version = version;
        this.modify("version", version);
    }

    /**
     * 设置 [材料配件名称]
     */
    public void setEmsublistname(String emsublistname) {
        this.emsublistname = emsublistname;
        this.modify("emsublistname", emsublistname);
    }

    /**
     * 设置 [初步审核]
     */
    public void setPreliminary(String preliminary) {
        this.preliminary = preliminary;
        this.modify("preliminary", preliminary);
    }

    /**
     * 设置 [说明]
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
        this.modify("remarks", remarks);
    }

    /**
     * 设置 [用途]
     */
    public void setApplication(String application) {
        this.application = application;
        this.modify("application", application);
    }

    /**
     * 设置 [班组]
     */
    public void setPfteamid(String pfteamid) {
        this.pfteamid = pfteamid;
        this.modify("pfteamid", pfteamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emsublistid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


