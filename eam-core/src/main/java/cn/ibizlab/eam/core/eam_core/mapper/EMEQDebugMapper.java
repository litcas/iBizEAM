package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEQDebug;
import cn.ibizlab.eam.core.eam_core.filter.EMEQDebugSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEQDebugMapper extends BaseMapper<EMEQDebug> {

    Page<EMEQDebug> searchCalendar(IPage page, @Param("srf") EMEQDebugSearchContext context, @Param("ew") Wrapper<EMEQDebug> wrapper);
    Page<EMEQDebug> searchDefault(IPage page, @Param("srf") EMEQDebugSearchContext context, @Param("ew") Wrapper<EMEQDebug> wrapper);
    @Override
    EMEQDebug selectById(Serializable id);
    @Override
    int insert(EMEQDebug entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEQDebug entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEQDebug entity, @Param("ew") Wrapper<EMEQDebug> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEQDebug> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMEQDebug> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMEQDebug> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMEQDebug> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid);

    List<EMEQDebug> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid);

    List<EMEQDebug> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid);

    List<EMEQDebug> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid);

    List<EMEQDebug> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEQDebug> selectByWoid(@Param("emwoid") Serializable emwoid);

    List<EMEQDebug> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
