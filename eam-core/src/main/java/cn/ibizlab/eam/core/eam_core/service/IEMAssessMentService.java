package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAssessMent;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAssessMent] 服务对象接口
 */
public interface IEMAssessMentService extends IService<EMAssessMent> {

    boolean create(EMAssessMent et);
    void createBatch(List<EMAssessMent> list);
    boolean update(EMAssessMent et);
    void updateBatch(List<EMAssessMent> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAssessMent get(String key);
    EMAssessMent getDraft(EMAssessMent et);
    boolean checkKey(EMAssessMent et);
    boolean save(EMAssessMent et);
    void saveBatch(List<EMAssessMent> list);
    Page<EMAssessMent> searchDefault(EMAssessMentSearchContext context);
    List<EMAssessMent> selectByPfteamid(String pfteamid);
    void removeByPfteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAssessMent> getEmassessmentByIds(List<String> ids);
    List<EMAssessMent> getEmassessmentByEntities(List<EMAssessMent> entities);
}


