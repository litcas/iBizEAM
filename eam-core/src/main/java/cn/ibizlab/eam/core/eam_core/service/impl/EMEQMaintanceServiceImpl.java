package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMaintance;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMaintanceSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQMaintanceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[抢修记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEQMaintanceServiceImpl")
public class EMEQMaintanceServiceImpl extends ServiceImpl<EMEQMaintanceMapper, EMEQMaintance> implements IEMEQMaintanceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOACService emrfoacService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService emrfocaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService emrfomoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQMaintance et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqmaintanceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQMaintance> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQMaintance et) {
        fillParentData(et);
        emeqahService.update(emeqmaintanceInheritMapping.toEmeqah(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqmaintanceid", et.getEmeqmaintanceid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqmaintanceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQMaintance> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emeqahService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQMaintance get(String key) {
        EMEQMaintance et = getById(key);
        if(et == null){
            et = new EMEQMaintance();
            et.setEmeqmaintanceid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQMaintance getDraft(EMEQMaintance et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQMaintance et) {
        return (!ObjectUtils.isEmpty(et.getEmeqmaintanceid())) && (!Objects.isNull(this.getById(et.getEmeqmaintanceid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQMaintance et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQMaintance et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQMaintance> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQMaintance> create = new ArrayList<>();
        List<EMEQMaintance> update = new ArrayList<>();
        for (EMEQMaintance et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqmaintanceid()) || ObjectUtils.isEmpty(getById(et.getEmeqmaintanceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQMaintance> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQMaintance> create = new ArrayList<>();
        List<EMEQMaintance> update = new ArrayList<>();
        for (EMEQMaintance et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqmaintanceid()) || ObjectUtils.isEmpty(getById(et.getEmeqmaintanceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQMaintance> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }
    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMEQMaintance> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQMaintance> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("objid",emobjectid));
    }

	@Override
    public List<EMEQMaintance> selectByRfoacid(String emrfoacid) {
        return baseMapper.selectByRfoacid(emrfoacid);
    }
    @Override
    public void removeByRfoacid(String emrfoacid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rfoacid",emrfoacid));
    }

	@Override
    public List<EMEQMaintance> selectByRfocaid(String emrfocaid) {
        return baseMapper.selectByRfocaid(emrfocaid);
    }
    @Override
    public void removeByRfocaid(String emrfocaid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rfocaid",emrfocaid));
    }

	@Override
    public List<EMEQMaintance> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }
    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rfodeid",emrfodeid));
    }

	@Override
    public List<EMEQMaintance> selectByRfomoid(String emrfomoid) {
        return baseMapper.selectByRfomoid(emrfomoid);
    }
    @Override
    public void removeByRfomoid(String emrfomoid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rfomoid",emrfomoid));
    }

	@Override
    public List<EMEQMaintance> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }
    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMEQMaintance> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }
    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("woid",emwoid));
    }

	@Override
    public List<EMEQMaintance> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }
    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMEQMaintance>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 日历查询
     */
    @Override
    public Page<EMEQMaintance> searchCalendar(EMEQMaintanceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQMaintance> pages=baseMapper.searchCalendar(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQMaintance>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQMaintance> searchDefault(EMEQMaintanceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQMaintance> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQMaintance>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQMaintance et){
        //实体关系[DER1N_EMEQMAINTANCE_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMRFOAC_RFOACID]
        if(!ObjectUtils.isEmpty(et.getRfoacid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac=et.getRfoac();
            if(ObjectUtils.isEmpty(rfoac)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOAC majorEntity=emrfoacService.get(et.getRfoacid());
                et.setRfoac(majorEntity);
                rfoac=majorEntity;
            }
            et.setRfoacname(rfoac.getEmrfoacname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMRFOCA_RFOCAID]
        if(!ObjectUtils.isEmpty(et.getRfocaid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca=et.getRfoca();
            if(ObjectUtils.isEmpty(rfoca)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOCA majorEntity=emrfocaService.get(et.getRfocaid());
                et.setRfoca(majorEntity);
                rfoca=majorEntity;
            }
            et.setRfocaname(rfoca.getEmrfocaname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMRFOMO_RFOMOID]
        if(!ObjectUtils.isEmpty(et.getRfomoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo=et.getRfomo();
            if(ObjectUtils.isEmpty(rfomo)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOMO majorEntity=emrfomoService.get(et.getRfomoid());
                et.setRfomo(majorEntity);
                rfomo=majorEntity;
            }
            et.setRfomoname(rfomo.getEmrfomoname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQMAINTANCE_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
        //实体关系[DER1N_EMEQMAINTANCE_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQMaintanceInheritMapping emeqmaintanceInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQMaintance et){
        if(ObjectUtils.isEmpty(et.getEmeqmaintanceid()))
            et.setEmeqmaintanceid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMEQAH emeqah =emeqmaintanceInheritMapping.toEmeqah(et);
        emeqah.set("emeqahtype","MAINTANCE");
        emeqahService.create(emeqah);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQMaintance> getEmeqmaintanceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQMaintance> getEmeqmaintanceByEntities(List<EMEQMaintance> entities) {
        List ids =new ArrayList();
        for(EMEQMaintance entity : entities){
            Serializable id=entity.getEmeqmaintanceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQMaintanceService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



