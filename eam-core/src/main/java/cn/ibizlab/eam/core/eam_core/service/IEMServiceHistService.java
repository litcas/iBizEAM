package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMServiceHist;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceHistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMServiceHist] 服务对象接口
 */
public interface IEMServiceHistService extends IService<EMServiceHist> {

    boolean create(EMServiceHist et);
    void createBatch(List<EMServiceHist> list);
    boolean update(EMServiceHist et);
    void updateBatch(List<EMServiceHist> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMServiceHist get(String key);
    EMServiceHist getDraft(EMServiceHist et);
    boolean checkKey(EMServiceHist et);
    boolean save(EMServiceHist et);
    void saveBatch(List<EMServiceHist> list);
    Page<EMServiceHist> searchDefault(EMServiceHistSearchContext context);
    List<EMServiceHist> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMServiceHist> getEmservicehistByIds(List<String> ids);
    List<EMServiceHist> getEmservicehistByEntities(List<EMServiceHist> entities);
}


