import EMItemPUse_en_US_Base from './emitem-puse_en_US_base';

function getLocaleResource(){
    const EMItemPUse_en_US_OwnData = {};
    const targetData = Object.assign(EMItemPUse_en_US_Base(), EMItemPUse_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
