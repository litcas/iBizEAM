import EMProduct_zh_CN_Base from './emproduct_zh_CN_base';

function getLocaleResource(){
    const EMProduct_zh_CN_OwnData = {};
    const targetData = Object.assign(EMProduct_zh_CN_Base(), EMProduct_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;