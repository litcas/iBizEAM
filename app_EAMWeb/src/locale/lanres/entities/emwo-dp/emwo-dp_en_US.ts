import EMWO_DP_en_US_Base from './emwo-dp_en_US_base';

function getLocaleResource(){
    const EMWO_DP_en_US_OwnData = {};
    const targetData = Object.assign(EMWO_DP_en_US_Base(), EMWO_DP_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
