import EMEQMaintance_en_US_Base from './emeqmaintance_en_US_base';

function getLocaleResource(){
    const EMEQMaintance_en_US_OwnData = {};
    const targetData = Object.assign(EMEQMaintance_en_US_Base(), EMEQMaintance_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
