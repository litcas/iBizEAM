import EMPlanTempl_zh_CN_Base from './emplan-templ_zh_CN_base';

function getLocaleResource(){
    const EMPlanTempl_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPlanTempl_zh_CN_Base(), EMPlanTempl_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;