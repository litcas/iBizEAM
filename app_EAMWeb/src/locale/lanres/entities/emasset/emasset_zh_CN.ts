import EMAsset_zh_CN_Base from './emasset_zh_CN_base';

function getLocaleResource(){
    const EMAsset_zh_CN_OwnData = {};
    const targetData = Object.assign(EMAsset_zh_CN_Base(), EMAsset_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;