import EMEQLCTMap_en_US_Base from './emeqlctmap_en_US_base';

function getLocaleResource(){
    const EMEQLCTMap_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLCTMap_en_US_Base(), EMEQLCTMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
