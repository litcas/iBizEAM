import EMObject_zh_CN_Base from './emobject_zh_CN_base';

function getLocaleResource(){
    const EMObject_zh_CN_OwnData = {};
    const targetData = Object.assign(EMObject_zh_CN_Base(), EMObject_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;