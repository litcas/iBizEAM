import EMService_en_US_Base from './emservice_en_US_base';

function getLocaleResource(){
    const EMService_en_US_OwnData = {};
    const targetData = Object.assign(EMService_en_US_Base(), EMService_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
