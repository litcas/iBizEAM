import EMWO_INNER_en_US_Base from './emwo-inner_en_US_base';

function getLocaleResource(){
    const EMWO_INNER_en_US_OwnData = {};
    const targetData = Object.assign(EMWO_INNER_en_US_Base(), EMWO_INNER_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
