import EMWO_INNER_zh_CN_Base from './emwo-inner_zh_CN_base';

function getLocaleResource(){
    const EMWO_INNER_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWO_INNER_zh_CN_Base(), EMWO_INNER_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;