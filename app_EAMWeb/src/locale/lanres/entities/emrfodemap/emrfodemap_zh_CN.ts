import EMRFODEMap_zh_CN_Base from './emrfodemap_zh_CN_base';

function getLocaleResource(){
    const EMRFODEMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFODEMap_zh_CN_Base(), EMRFODEMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;