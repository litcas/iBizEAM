import PFUnit_zh_CN_Base from './pfunit_zh_CN_base';

function getLocaleResource(){
    const PFUnit_zh_CN_OwnData = {};
    const targetData = Object.assign(PFUnit_zh_CN_Base(), PFUnit_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;