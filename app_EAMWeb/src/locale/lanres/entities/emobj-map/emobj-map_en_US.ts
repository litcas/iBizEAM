import EMObjMap_en_US_Base from './emobj-map_en_US_base';

function getLocaleResource(){
    const EMObjMap_en_US_OwnData = {};
    const targetData = Object.assign(EMObjMap_en_US_Base(), EMObjMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
