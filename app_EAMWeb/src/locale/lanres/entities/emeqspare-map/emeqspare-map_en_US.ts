import EMEQSpareMap_en_US_Base from './emeqspare-map_en_US_base';

function getLocaleResource(){
    const EMEQSpareMap_en_US_OwnData = {};
    const targetData = Object.assign(EMEQSpareMap_en_US_Base(), EMEQSpareMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
