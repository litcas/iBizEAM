import PLANSCHEDULE_T_zh_CN_Base from './planschedule-t_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_T_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_T_zh_CN_Base(), PLANSCHEDULE_T_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;