import EMEQSetup_zh_CN_Base from './emeqsetup_zh_CN_base';

function getLocaleResource(){
    const EMEQSetup_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQSetup_zh_CN_Base(), EMEQSetup_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;