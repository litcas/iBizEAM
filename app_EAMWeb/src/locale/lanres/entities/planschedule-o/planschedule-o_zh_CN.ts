import PLANSCHEDULE_O_zh_CN_Base from './planschedule-o_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_O_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_O_zh_CN_Base(), PLANSCHEDULE_O_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;