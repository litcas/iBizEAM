import EMBrand_en_US_Base from './embrand_en_US_base';

function getLocaleResource(){
    const EMBrand_en_US_OwnData = {};
    const targetData = Object.assign(EMBrand_en_US_Base(), EMBrand_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
