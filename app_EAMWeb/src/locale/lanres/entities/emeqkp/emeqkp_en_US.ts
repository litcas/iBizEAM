import EMEQKP_en_US_Base from './emeqkp_en_US_base';

function getLocaleResource(){
    const EMEQKP_en_US_OwnData = {};
    const targetData = Object.assign(EMEQKP_en_US_Base(), EMEQKP_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
