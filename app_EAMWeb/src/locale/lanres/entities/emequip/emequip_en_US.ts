import EMEquip_en_US_Base from './emequip_en_US_base';

function getLocaleResource(){
    const EMEquip_en_US_OwnData = {};
    const targetData = Object.assign(EMEquip_en_US_Base(), EMEquip_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
