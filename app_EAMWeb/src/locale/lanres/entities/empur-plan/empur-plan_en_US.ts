import EMPurPlan_en_US_Base from './empur-plan_en_US_base';

function getLocaleResource(){
    const EMPurPlan_en_US_OwnData = {};
    const targetData = Object.assign(EMPurPlan_en_US_Base(), EMPurPlan_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
