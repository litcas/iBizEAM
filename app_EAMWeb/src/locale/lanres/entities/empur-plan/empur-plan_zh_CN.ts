import EMPurPlan_zh_CN_Base from './empur-plan_zh_CN_base';

function getLocaleResource(){
    const EMPurPlan_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPurPlan_zh_CN_Base(), EMPurPlan_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;