import EMEQCheck_en_US_Base from './emeqcheck_en_US_base';

function getLocaleResource(){
    const EMEQCheck_en_US_OwnData = {};
    const targetData = Object.assign(EMEQCheck_en_US_Base(), EMEQCheck_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
