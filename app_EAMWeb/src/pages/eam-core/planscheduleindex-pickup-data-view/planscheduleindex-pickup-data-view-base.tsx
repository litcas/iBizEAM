import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { IndexPickupDataViewBase } from '@/studio-core';
import PLANSCHEDULEService from '@/service/planschedule/planschedule-service';
import PLANSCHEDULEAuthService from '@/authservice/planschedule/planschedule-auth-service';
import PLANSCHEDULEUIService from '@/uiservice/planschedule/planschedule-ui-service';

/**
 * 计划时刻设置索引关系选择数据视图视图基类
 *
 * @export
 * @class PLANSCHEDULEIndexPickupDataViewBase
 * @extends {IndexPickupDataViewBase}
 */
export class PLANSCHEDULEIndexPickupDataViewBase extends IndexPickupDataViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected appDeName: string = 'planschedule';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected appDeKey: string = 'planscheduleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected appDeMajor: string = 'planschedulename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */ 
    protected dataControl: string = 'dataview';

    /**
     * 实体服务对象
     *
     * @type {PLANSCHEDULEService}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected appEntityService: PLANSCHEDULEService = new PLANSCHEDULEService;

    /**
     * 实体权限服务对象
     *
     * @type PLANSCHEDULEUIService
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    public appUIService: PLANSCHEDULEUIService = new PLANSCHEDULEUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected model: any = {
        srfCaption: 'entities.planschedule.views.indexpickupdataview.caption',
        srfTitle: 'entities.planschedule.views.indexpickupdataview.title',
        srfSubTitle: 'entities.planschedule.views.indexpickupdataview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    protected containerModel: any = {
        view_dataview: {
            name: 'dataview',
            type: 'DATAVIEW',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
	protected viewtag: string = '797682a7f879312b3c1171981d68bdfe';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */ 
    protected viewName: string = 'PLANSCHEDULEIndexPickupDataView';



    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PLANSCHEDULEIndexPickupDataViewBase
     */
    public engineInit(): void {
    }


}