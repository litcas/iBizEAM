import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWOServiceBase from './emwo-service-base';


/**
 * 工单服务对象
 *
 * @export
 * @class EMWOService
 * @extends {EMWOServiceBase}
 */
export default class EMWOService extends EMWOServiceBase {

    /**
     * Creates an instance of  EMWOService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}