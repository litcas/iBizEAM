import { Http } from '@/utils';
import { Util } from '@/utils';
import GenIdLogicBase from './gen-id-logic-base';

/**
 * 生成订单编号
 *
 * @export
 * @class GenIdLogic
 */
export default class GenIdLogic extends GenIdLogicBase{

    /**
     * Creates an instance of  GenIdLogic
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}