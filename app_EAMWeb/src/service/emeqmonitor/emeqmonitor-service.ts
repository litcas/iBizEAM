import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQMonitorServiceBase from './emeqmonitor-service-base';


/**
 * 设备状态监控服务对象
 *
 * @export
 * @class EMEQMonitorService
 * @extends {EMEQMonitorServiceBase}
 */
export default class EMEQMonitorService extends EMEQMonitorServiceBase {

    /**
     * Creates an instance of  EMEQMonitorService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMonitorService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}