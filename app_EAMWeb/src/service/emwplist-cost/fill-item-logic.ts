import { Http } from '@/utils';
import { Util } from '@/utils';
import FillItemLogicBase from './fill-item-logic-base';

/**
 * FillItem
 *
 * @export
 * @class FillItemLogic
 */
export default class FillItemLogic extends FillItemLogicBase{

    /**
     * Creates an instance of  FillItemLogic
     * 
     * @param {*} [opts={}]
     * @memberof  FillItemLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}