import { Http } from '@/utils';
import { Util } from '@/utils';
import RejectedLogicBase from './rejected-logic-base';

/**
 * 驳回
 *
 * @export
 * @class RejectedLogic
 */
export default class RejectedLogic extends RejectedLogicBase{

    /**
     * Creates an instance of  RejectedLogic
     * 
     * @param {*} [opts={}]
     * @memberof  RejectedLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}