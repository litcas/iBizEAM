import { Http } from '@/utils';
import { Util } from '@/utils';
import EMDRWGMapServiceBase from './emdrwgmap-service-base';


/**
 * 文档引用服务对象
 *
 * @export
 * @class EMDRWGMapService
 * @extends {EMDRWGMapServiceBase}
 */
export default class EMDRWGMapService extends EMDRWGMapServiceBase {

    /**
     * Creates an instance of  EMDRWGMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}