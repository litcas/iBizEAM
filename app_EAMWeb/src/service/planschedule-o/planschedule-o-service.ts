import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULE_OServiceBase from './planschedule-o-service-base';


/**
 * 自定义间隔天数服务对象
 *
 * @export
 * @class PLANSCHEDULE_OService
 * @extends {PLANSCHEDULE_OServiceBase}
 */
export default class PLANSCHEDULE_OService extends PLANSCHEDULE_OServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_OService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_OService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}