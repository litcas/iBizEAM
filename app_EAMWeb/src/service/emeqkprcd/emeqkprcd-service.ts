import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQKPRCDServiceBase from './emeqkprcd-service-base';


/**
 * 关键点记录服务对象
 *
 * @export
 * @class EMEQKPRCDService
 * @extends {EMEQKPRCDServiceBase}
 */
export default class EMEQKPRCDService extends EMEQKPRCDServiceBase {

    /**
     * Creates an instance of  EMEQKPRCDService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPRCDService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}