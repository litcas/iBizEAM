import PLANSCHEDULE_MAuthServiceBase from './planschedule-m-auth-service-base';


/**
 * 计划_按月权限服务对象
 *
 * @export
 * @class PLANSCHEDULE_MAuthService
 * @extends {PLANSCHEDULE_MAuthServiceBase}
 */
export default class PLANSCHEDULE_MAuthService extends PLANSCHEDULE_MAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_MAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_MAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}