import EMACClassAuthServiceBase from './emacclass-auth-service-base';


/**
 * 总帐科目权限服务对象
 *
 * @export
 * @class EMACClassAuthService
 * @extends {EMACClassAuthServiceBase}
 */
export default class EMACClassAuthService extends EMACClassAuthServiceBase {

    /**
     * Creates an instance of  EMACClassAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMACClassAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}