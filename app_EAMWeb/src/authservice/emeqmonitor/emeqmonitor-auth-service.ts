import EMEQMonitorAuthServiceBase from './emeqmonitor-auth-service-base';


/**
 * 设备状态监控权限服务对象
 *
 * @export
 * @class EMEQMonitorAuthService
 * @extends {EMEQMonitorAuthServiceBase}
 */
export default class EMEQMonitorAuthService extends EMEQMonitorAuthServiceBase {

    /**
     * Creates an instance of  EMEQMonitorAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMonitorAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}