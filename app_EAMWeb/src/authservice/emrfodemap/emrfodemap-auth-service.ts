import EMRFODEMapAuthServiceBase from './emrfodemap-auth-service-base';


/**
 * 现象引用权限服务对象
 *
 * @export
 * @class EMRFODEMapAuthService
 * @extends {EMRFODEMapAuthServiceBase}
 */
export default class EMRFODEMapAuthService extends EMRFODEMapAuthServiceBase {

    /**
     * Creates an instance of  EMRFODEMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}