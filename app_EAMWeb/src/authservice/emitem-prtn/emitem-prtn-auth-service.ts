import EMItemPRtnAuthServiceBase from './emitem-prtn-auth-service-base';


/**
 * 还料单权限服务对象
 *
 * @export
 * @class EMItemPRtnAuthService
 * @extends {EMItemPRtnAuthServiceBase}
 */
export default class EMItemPRtnAuthService extends EMItemPRtnAuthServiceBase {

    /**
     * Creates an instance of  EMItemPRtnAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPRtnAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}