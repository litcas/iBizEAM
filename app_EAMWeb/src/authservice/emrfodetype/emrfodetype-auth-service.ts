import EMRFODETypeAuthServiceBase from './emrfodetype-auth-service-base';


/**
 * 现象分类权限服务对象
 *
 * @export
 * @class EMRFODETypeAuthService
 * @extends {EMRFODETypeAuthServiceBase}
 */
export default class EMRFODETypeAuthService extends EMRFODETypeAuthServiceBase {

    /**
     * Creates an instance of  EMRFODETypeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODETypeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}