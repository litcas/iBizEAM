import PFEmpPostMapAuthServiceBase from './pfemp-post-map-auth-service-base';


/**
 * 人事关系权限服务对象
 *
 * @export
 * @class PFEmpPostMapAuthService
 * @extends {PFEmpPostMapAuthServiceBase}
 */
export default class PFEmpPostMapAuthService extends PFEmpPostMapAuthServiceBase {

    /**
     * Creates an instance of  PFEmpPostMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpPostMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}