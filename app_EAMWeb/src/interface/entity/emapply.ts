/**
 * 外委申请
 *
 * @export
 * @interface EMApply
 */
export interface EMApply {

    /**
     * 外委类型
     *
     * @returns {*}
     * @memberof EMApply
     */
    entrustlist?: any;

    /**
     * 外委申请内容
     *
     * @returns {*}
     * @memberof EMApply
     */
    applydesc?: any;

    /**
     * 申请汇报状态
     *
     * @returns {*}
     * @memberof EMApply
     */
    applystate?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMApply
     */
    activelengths?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMApply
     */
    updateman?: any;

    /**
     * 处理备注
     *
     * @returns {*}
     * @memberof EMApply
     */
    dpdesc?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMApply
     */
    mfee?: any;

    /**
     * 申请信息
     *
     * @returns {*}
     * @memberof EMApply
     */
    applyinfo?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMApply
     */
    wfstep?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMApply
     */
    shuifei?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMApply
     */
    sfee?: any;

    /**
     * 外委申请名称
     *
     * @returns {*}
     * @memberof EMApply
     */
    emapplyname?: any;

    /**
     * 申请类型
     *
     * @returns {*}
     * @memberof EMApply
     */
    applytype?: any;

    /**
     * 希望完成早于
     *
     * @returns {*}
     * @memberof EMApply
     */
    applyedate?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMApply
     */
    mpersonid?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMApply
     */
    wfinstanceid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMApply
     */
    description?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMApply
     */
    pfee?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMApply
     */
    updatedate?: any;

    /**
     * 预算费用(￥)
     *
     * @returns {*}
     * @memberof EMApply
     */
    prefee1?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMApply
     */
    createman?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMApply
     */
    rempid?: any;

    /**
     * 发票号
     *
     * @returns {*}
     * @memberof EMApply
     */
    fp?: any;

    /**
     * 总费用
     *
     * @returns {*}
     * @memberof EMApply
     */
    zfy?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMApply
     */
    rempname?: any;

    /**
     * 希望开始于
     *
     * @returns {*}
     * @memberof EMApply
     */
    applybdate?: any;

    /**
     * 费用(￥)
     *
     * @returns {*}
     * @memberof EMApply
     */
    prefee?: any;

    /**
     * 关闭日期
     *
     * @returns {*}
     * @memberof EMApply
     */
    closedate?: any;

    /**
     * 外委编号
     *
     * @returns {*}
     * @memberof EMApply
     */
    emapplyid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMApply
     */
    createdate?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMApply
     */
    wfstate?: any;

    /**
     * 计划类型
     *
     * @returns {*}
     * @memberof EMApply
     */
    plantype?: any;

    /**
     * 关闭人
     *
     * @returns {*}
     * @memberof EMApply
     */
    closeempid?: any;

    /**
     * 申请日期
     *
     * @returns {*}
     * @memberof EMApply
     */
    applydate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMApply
     */
    rdeptname?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMApply
     */
    mpersonname?: any;

    /**
     * 关闭人
     *
     * @returns {*}
     * @memberof EMApply
     */
    closeempname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMApply
     */
    orgid?: any;

    /**
     * 发票附件
     *
     * @returns {*}
     * @memberof EMApply
     */
    invoiceattach?: any;

    /**
     * 审批意见
     *
     * @returns {*}
     * @memberof EMApply
     */
    spyj?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMApply
     */
    enable?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMApply
     */
    priority?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMApply
     */
    rdeptid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfomoname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMApply
     */
    rservicename?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMApply
     */
    equipname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfodename?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfoacname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMApply
     */
    objname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMApply
     */
    rteamname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfocaname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMApply
     */
    equipid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMApply
     */
    rteamid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMApply
     */
    objid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfoacid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfomoid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMApply
     */
    rserviceid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfocaid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMApply
     */
    rfodeid?: any;
}