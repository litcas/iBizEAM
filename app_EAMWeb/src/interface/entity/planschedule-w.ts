/**
 * 计划_按周
 *
 * @export
 * @interface PLANSCHEDULE_W
 */
export interface PLANSCHEDULE_W {

    /**
     * 计划_按周标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    planschedule_wid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    createman?: any;

    /**
     * 计划_按周名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    planschedule_wname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    updatedate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    scheduleparam?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    emplanid?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    intervalminute?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    cyclestarttime?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    cycleendtime?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    emplanname?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    schedulestate?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    scheduletype?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    description?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    lastminute?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    rundate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    scheduleparam4?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    scheduleparam2?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    scheduleparam3?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    runtime?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_W
     */
    taskid?: any;
}