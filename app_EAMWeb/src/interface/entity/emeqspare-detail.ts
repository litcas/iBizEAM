/**
 * 备件包明细
 *
 * @export
 * @interface EMEQSpareDetail
 */
export interface EMEQSpareDetail {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    orgid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    createman?: any;

    /**
     * 备件包明细标识
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    emeqsparedetailid?: any;

    /**
     * 备件包明细
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    emeqsparedetailname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    description?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    itemname?: any;

    /**
     * 备件包
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    eqsparename?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    itemid?: any;

    /**
     * 备件包
     *
     * @returns {*}
     * @memberof EMEQSpareDetail
     */
    eqspareid?: any;
}