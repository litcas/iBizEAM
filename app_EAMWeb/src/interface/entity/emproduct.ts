/**
 * 试用品
 *
 * @export
 * @interface EMProduct
 */
export interface EMProduct {

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMProduct
     */
    deptname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMProduct
     */
    description?: any;

    /**
     * 试用品名称
     *
     * @returns {*}
     * @memberof EMProduct
     */
    emproductname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMProduct
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMProduct
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMProduct
     */
    createman?: any;

    /**
     * 用料人
     *
     * @returns {*}
     * @memberof EMProduct
     */
    empname?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMProduct
     */
    wfinstanceid?: any;

    /**
     * 开始时间
     *
     * @returns {*}
     * @memberof EMProduct
     */
    begintime?: any;

    /**
     * 试用品状态
     *
     * @returns {*}
     * @memberof EMProduct
     */
    productstate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMProduct
     */
    orgid?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMProduct
     */
    price?: any;

    /**
     * 试用品编号
     *
     * @returns {*}
     * @memberof EMProduct
     */
    emproductid?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof EMProduct
     */
    psum?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMProduct
     */
    aempname?: any;

    /**
     * 用途
     *
     * @returns {*}
     * @memberof EMProduct
     */
    useto?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMProduct
     */
    aempid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMProduct
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMProduct
     */
    updatedate?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMProduct
     */
    wfstate?: any;

    /**
     * 用料人
     *
     * @returns {*}
     * @memberof EMProduct
     */
    empid?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMProduct
     */
    deptid?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMProduct
     */
    wfstep?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMProduct
     */
    equipname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMProduct
     */
    itemname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMProduct
     */
    objname?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMProduct
     */
    teamname?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMProduct
     */
    servicename?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMProduct
     */
    serviceid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMProduct
     */
    itemid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMProduct
     */
    equipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMProduct
     */
    objid?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMProduct
     */
    teamid?: any;
}