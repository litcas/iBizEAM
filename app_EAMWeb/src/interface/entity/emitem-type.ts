/**
 * 物品类型
 *
 * @export
 * @interface EMItemType
 */
export interface EMItemType {

    /**
     * 物品类型标识
     *
     * @returns {*}
     * @memberof EMItemType
     */
    emitemtypeid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemType
     */
    description?: any;

    /**
     * 物品类型代码
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemtypecode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemType
     */
    orgid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemType
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemType
     */
    enable?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemType
     */
    createman?: any;

    /**
     * 物品类型名称
     *
     * @returns {*}
     * @memberof EMItemType
     */
    emitemtypename?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemType
     */
    updateman?: any;

    /**
     * 物品类型信息
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemtypeinfo?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemType
     */
    createdate?: any;

    /**
     * 上级类型代码
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemtypepcode?: any;

    /**
     * 一级类
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itembtypename?: any;

    /**
     * 二级类
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemmtypename?: any;

    /**
     * 上级类型
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemtypepname?: any;

    /**
     * 上级类型
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemtypepid?: any;

    /**
     * 二级类
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itemmtypeid?: any;

    /**
     * 一级类
     *
     * @returns {*}
     * @memberof EMItemType
     */
    itembtypeid?: any;
}