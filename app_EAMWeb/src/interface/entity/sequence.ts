/**
 * 序列号
 *
 * @export
 * @interface SEQUENCE
 */
export interface SEQUENCE {

    /**
     * 序列号名称
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    sequencename?: any;

    /**
     * 序列号标识
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    sequenceid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    updateman?: any;

    /**
     * 实体名
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    code?: any;

    /**
     * 实现
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    implementation?: any;

    /**
     * 前缀
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    prefix?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    enable?: any;

    /**
     * 后缀
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    suffix?: any;

    /**
     * 序号增长
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    numincrement?: any;

    /**
     * 序号长度
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    padding?: any;

    /**
     * 下一号码
     *
     * @returns {*}
     * @memberof SEQUENCE
     */
    numnext?: any;
}