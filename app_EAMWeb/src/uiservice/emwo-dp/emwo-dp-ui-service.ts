import EMWO_DPUIServiceBase from './emwo-dp-ui-service-base';

/**
 * 点检工单UI服务对象
 *
 * @export
 * @class EMWO_DPUIService
 */
export default class EMWO_DPUIService extends EMWO_DPUIServiceBase {

    /**
     * Creates an instance of  EMWO_DPUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_DPUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}