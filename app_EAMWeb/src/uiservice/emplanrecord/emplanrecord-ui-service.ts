import EMPLANRECORDUIServiceBase from './emplanrecord-ui-service-base';

/**
 * 触发记录UI服务对象
 *
 * @export
 * @class EMPLANRECORDUIService
 */
export default class EMPLANRECORDUIService extends EMPLANRECORDUIServiceBase {

    /**
     * Creates an instance of  EMPLANRECORDUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPLANRECORDUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}