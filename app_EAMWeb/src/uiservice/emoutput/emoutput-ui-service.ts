import EMOutputUIServiceBase from './emoutput-ui-service-base';

/**
 * 能力UI服务对象
 *
 * @export
 * @class EMOutputUIService
 */
export default class EMOutputUIService extends EMOutputUIServiceBase {

    /**
     * Creates an instance of  EMOutputUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}