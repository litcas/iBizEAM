import EMPurPlanUIServiceBase from './empur-plan-ui-service-base';

/**
 * 计划修理UI服务对象
 *
 * @export
 * @class EMPurPlanUIService
 */
export default class EMPurPlanUIService extends EMPurPlanUIServiceBase {

    /**
     * Creates an instance of  EMPurPlanUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPurPlanUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}