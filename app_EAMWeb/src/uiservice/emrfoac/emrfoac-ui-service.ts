import EMRFOACUIServiceBase from './emrfoac-ui-service-base';

/**
 * 方案UI服务对象
 *
 * @export
 * @class EMRFOACUIService
 */
export default class EMRFOACUIService extends EMRFOACUIServiceBase {

    /**
     * Creates an instance of  EMRFOACUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOACUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}