import EMPlanDetailUIServiceBase from './emplan-detail-ui-service-base';

/**
 * 计划步骤UI服务对象
 *
 * @export
 * @class EMPlanDetailUIService
 */
export default class EMPlanDetailUIService extends EMPlanDetailUIServiceBase {

    /**
     * Creates an instance of  EMPlanDetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanDetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}