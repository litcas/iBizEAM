import EMAssetClassUIServiceBase from './emasset-class-ui-service-base';

/**
 * 资产类别UI服务对象
 *
 * @export
 * @class EMAssetClassUIService
 */
export default class EMAssetClassUIService extends EMAssetClassUIServiceBase {

    /**
     * Creates an instance of  EMAssetClassUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClassUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}