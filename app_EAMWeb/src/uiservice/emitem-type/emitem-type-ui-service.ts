import EMItemTypeUIServiceBase from './emitem-type-ui-service-base';

/**
 * 物品类型UI服务对象
 *
 * @export
 * @class EMItemTypeUIService
 */
export default class EMItemTypeUIService extends EMItemTypeUIServiceBase {

    /**
     * Creates an instance of  EMItemTypeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTypeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}