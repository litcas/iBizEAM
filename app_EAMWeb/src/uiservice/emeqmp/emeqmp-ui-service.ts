import EMEQMPUIServiceBase from './emeqmp-ui-service-base';

/**
 * 设备仪表UI服务对象
 *
 * @export
 * @class EMEQMPUIService
 */
export default class EMEQMPUIService extends EMEQMPUIServiceBase {

    /**
     * Creates an instance of  EMEQMPUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}