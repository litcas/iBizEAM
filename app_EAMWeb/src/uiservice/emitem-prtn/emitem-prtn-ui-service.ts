import EMItemPRtnUIServiceBase from './emitem-prtn-ui-service-base';

/**
 * 还料单UI服务对象
 *
 * @export
 * @class EMItemPRtnUIService
 */
export default class EMItemPRtnUIService extends EMItemPRtnUIServiceBase {

    /**
     * Creates an instance of  EMItemPRtnUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPRtnUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}