import EMACClassUIServiceBase from './emacclass-ui-service-base';

/**
 * 总帐科目UI服务对象
 *
 * @export
 * @class EMACClassUIService
 */
export default class EMACClassUIService extends EMACClassUIServiceBase {

    /**
     * Creates an instance of  EMACClassUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMACClassUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}