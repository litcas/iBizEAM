import EMRFODEUIServiceBase from './emrfode-ui-service-base';

/**
 * 现象UI服务对象
 *
 * @export
 * @class EMRFODEUIService
 */
export default class EMRFODEUIService extends EMRFODEUIServiceBase {

    /**
     * Creates an instance of  EMRFODEUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}