/**
 * OverallEVL 部件模型
 *
 * @export
 * @class OverallEVLModel
 */
export default class OverallEVLModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof OverallEVLDashboard_sysportlet1_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}