import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWOService from '@/service/emwo/emwo-service';
import WoCalendarExpModel from './wo-calendar-exp-calendar-model';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import EMWO_OSCService from '@/service/emwo-osc/emwo-osc-service';
import EMWO_DPService from '@/service/emwo-dp/emwo-dp-service';
import EMWO_ENService from '@/service/emwo-en/emwo-en-service';


/**
 * WoCalendarExp 部件服务对象
 *
 * @export
 * @class WoCalendarExpService
 */
export default class WoCalendarExpService extends ControlService {

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof WoCalendarExpService
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof WoCalendarExpService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of WoCalendarExpService.
     * 
     * @param {*} [opts={}]
     * @memberof WoCalendarExpService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new WoCalendarExpModel();
    }

    /**
     * 内部工单服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof WoCalendarExpService
     */
    public emwo_innerService: EMWO_INNERService = new EMWO_INNERService();
    /**
     * 外委保养工单服务对象
     *
     * @type {EMWO_OSCService}
     * @memberof WoCalendarExpService
     */
    public emwo_oscService: EMWO_OSCService = new EMWO_OSCService();
    /**
     * 点检工单服务对象
     *
     * @type {EMWO_DPService}
     * @memberof WoCalendarExpService
     */
    public emwo_dpService: EMWO_DPService = new EMWO_DPService();
    /**
     * 能耗登记工单服务对象
     *
     * @type {EMWO_ENService}
     * @memberof WoCalendarExpService
     */
    public emwo_enService: EMWO_ENService = new EMWO_ENService();

    /**
     * 事件配置集合
     *
     * @public
     * @type {any[]}
     * @memberof WoCalendarExp
     */
    public eventsConfig: any[] = [
        {
          itemName : '内部工单',
          itemType : 'EMWO_INNER',
          color : 'rgba(0, 47, 255, 1)',
          textColor : '',
        },
        {
          itemName : '外委工单',
          itemType : 'EMWO_OSC',
          color : 'rgba(255, 115, 0, 1)',
          textColor : '',
        },
        {
          itemName : '点检工单',
          itemType : 'EMWO_DP',
          color : 'rgba(149, 117, 205, 1)',
          textColor : '',
        },
        {
          itemName : '能耗工单',
          itemType : 'EMWO_EN',
          color : 'rgba(0, 255, 30, 1)',
          textColor : '',
        },
    ];

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof WoCalendarExpService
     */
    @Errorlog
    public search(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let _this = this;
        return new Promise((resolve: any, reject: any) => {
            let promises:any = [];
            let tempRequest:any;
            tempRequest = this.handleRequestData(action,context,data,true,"EMWO_INNER");
            promises.push(this.emwo_innerService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMWO_OSC");
            promises.push(this.emwo_oscService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMWO_DP");
            promises.push(this.emwo_dpService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMWO_EN");
            promises.push(this.emwo_enService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            Promise.all(promises).then((resArray: any) => {
                let _data:any = [];
                resArray.forEach((response:any,resIndex:number) => {
                    if (!response || response.status !== 200) {
                        return;
                    }
                    let _response: any = JSON.parse(JSON.stringify(response));
                    _response.data.forEach((item:any,index:number) =>{
                        _response.data[index].color = _this.eventsConfig[resIndex].color;
                        _response.data[index].textColor = _this.eventsConfig[resIndex].textColor;
                        _response.data[index].itemType = _this.eventsConfig[resIndex].itemType;
                    });
                    ;
                    _this.handleResponse(action, _response,false,_this.eventsConfig[resIndex].itemType);
                    _data.push(..._response.data);
                });
                // 排序
                _data.sort((a:any, b:any)=>{
                    let dateA = new Date(Date.parse(a.start.replace(/-/g, "/")));
                    let dateB = new Date(Date.parse(b.start.replace(/-/g, "/")));
                    return dateA > dateB ? 1 : -1 ;
                });
                let result = {status: 200, data: _data};
                resolve(result);
            }).catch((response: any) => {
                reject(response);
            });  
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof WoCalendarExpService
     */
    @Errorlog
    public update(itemType: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            let result: any;
            let tempRequest:any;
            switch(itemType) {
                case "EMWO_INNER":
                    tempRequest = this.handleRequestData("",context,data,false,"EMWO_INNER");
                    result = this.emwo_innerService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMWO_OSC":
                    tempRequest = this.handleRequestData("",context,data,false,"EMWO_OSC");
                    result = this.emwo_oscService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMWO_DP":
                    tempRequest = this.handleRequestData("",context,data,false,"EMWO_DP");
                    result = this.emwo_dpService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMWO_EN":
                    tempRequest = this.handleRequestData("",context,data,false,"EMWO_EN");
                    result = this.emwo_enService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
            }
            if(result){
                result.then((response: any) => {
                    this.handleResponse("", response);
                    resolve(response);
                }).catch((response: any) => {
                    reject(response);
                });
            }else{
              reject("没有匹配的实体服务");
            }
        });
    }

    /**
     * 处理request请求数据
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof ControlService
     */
    public handleRequestData(action: string,context:any ={},data: any = {},isMerge:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        return super.handleRequestData(action,context,data,isMerge);
    }

    /**
     * 处理response返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlService
     */
    public async handleResponse(action: string, response: any,isCreate:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        super.handleResponse(action,response,isCreate);
    }

}