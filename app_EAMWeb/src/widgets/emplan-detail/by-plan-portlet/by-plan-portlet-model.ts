/**
 * ByPlan 部件模型
 *
 * @export
 * @class ByPlanModel
 */
export default class ByPlanModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByPlanModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emplandetailname',
      },
      {
        name: 'orgid',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'enable',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'orderflag',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emplandetail',
        prop: 'emplandetailid',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'archive',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'content',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'plandetaildesc',
      },
      {
        name: 'planname',
      },
      {
        name: 'dptype',
      },
      {
        name: 'equipname',
      },
      {
        name: 'dpname',
      },
      {
        name: 'objname',
      },
      {
        name: 'objid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'planid',
      },
      {
        name: 'equipid',
      },
    ]
  }


}
