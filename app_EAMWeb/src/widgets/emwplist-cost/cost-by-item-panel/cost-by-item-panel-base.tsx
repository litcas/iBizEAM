import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import CostByItemService from './cost-by-item-panel-service';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import CostByItemModel from './cost-by-item-panel-model';
import CodeListService from "@service/app/codelist-service";

/**
 * dashboard_sysportlet6_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {CostByItemPanelBase}
 */
export class CostByItemPanelBase extends PanelControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {CostByItemService}
     * @memberof CostByItemPanelBase
     */
    public service: CostByItemService = new CostByItemService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof CostByItemPanelBase
     */
    public appEntityService: EMWPListCostService = new EMWPListCostService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPanelBase
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPanelBase
     */
    protected appDeLogicName: string = '询价单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListCostUIService}
     * @memberof CostByItemBase
     */  
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof CostByItem
     */
    public detailsModel: any = {
        adate: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'adate', panel: this })
,
        labservicename: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'labservicename', panel: this })
,
        price: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'price', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof CostByItem
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                




    }

    /**
     * 数据模型对象
     *
     * @type {CostByItemModel}
     * @memberof CostByItem
     */
    public dataModel: CostByItemModel = new CostByItemModel();

    /**
     * 界面行为标识数组
     *
     * @type {Array<any>}
     * @memberof CostByItem
     */
    public actionList:Array<any> = [];

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof CostByItem
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}