/**
 * IndexPickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class IndexPickupViewpickupviewpanelModel
 */
export default class IndexPickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof IndexPickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'planschedule',
        prop: 'planscheduleid',
      },
      {
        name: 'createman',
      },
      {
        name: 'planschedulename',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'scheduletype',
      },
      {
        name: 'cyclestarttime',
      },
      {
        name: 'cycleendtime',
      },
      {
        name: 'description',
      },
      {
        name: 'intervalminute',
      },
      {
        name: 'rundate',
      },
      {
        name: 'runtime',
      },
      {
        name: 'scheduleparam',
      },
      {
        name: 'scheduleparam2',
      },
      {
        name: 'scheduleparam3',
      },
      {
        name: 'scheduleparam4',
      },
      {
        name: 'schedulestate',
      },
      {
        name: 'emplanid',
      },
      {
        name: 'emplanname',
      },
      {
        name: 'lastminute',
      },
      {
        name: 'taskid',
      },
    ]
  }


}