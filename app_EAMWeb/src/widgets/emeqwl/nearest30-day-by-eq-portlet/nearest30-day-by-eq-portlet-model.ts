/**
 * Nearest30DayByEQ 部件模型
 *
 * @export
 * @class Nearest30DayByEQModel
 */
export default class Nearest30DayByEQModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Nearest30DayByEQModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'bdate',
      },
      {
        name: 'nval',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'edate',
      },
      {
        name: 'emeqwlname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'enable',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createman',
      },
      {
        name: 'emeqwl',
        prop: 'emeqwlid',
      },
      {
        name: 'woname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'objname',
      },
      {
        name: 'equipid',
      },
      {
        name: 'woid',
      },
      {
        name: 'objid',
      },
    ]
  }


}
