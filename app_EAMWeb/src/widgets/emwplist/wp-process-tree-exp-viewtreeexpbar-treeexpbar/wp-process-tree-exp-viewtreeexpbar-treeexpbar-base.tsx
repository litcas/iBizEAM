import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, TreeExpBarControlBase } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import WpProcessTreeExpViewtreeexpbarService from './wp-process-tree-exp-viewtreeexpbar-treeexpbar-service';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';

/**
 * treeexpbar部件基类
 *
 * @export
 * @class TreeExpBarControlBase
 * @extends {WpProcessTreeExpViewtreeexpbarTreeExpBarBase}
 */
export class WpProcessTreeExpViewtreeexpbarTreeExpBarBase extends TreeExpBarControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected controlType: string = 'TREEEXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {WpProcessTreeExpViewtreeexpbarService}
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    public service: WpProcessTreeExpViewtreeexpbarService = new WpProcessTreeExpViewtreeexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    public appEntityService: EMWPListService = new EMWPListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListUIService}
     * @memberof WpProcessTreeExpViewtreeexpbarBase
     */  
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);

    /**
     * treeexpbar_tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_selectionchange($event: any, $event2?: any) {
        this.treeexpbar_selectionchange($event, 'treeexpbar_tree', $event2);
    }

    /**
     * treeexpbar_tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WpProcessTreeExpViewtreeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_load($event: any, $event2?: any) {
        this.treeexpbar_load($event, 'treeexpbar_tree', $event2);
    }


    /**
     * 控件宽度
     *
     * @type {number}
     * @memberof WpProcessTreeExpViewtreeexpbarBase
     */
    public ctrlWidth:number = 250;

    /**
     * 获取关系项视图
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof WpProcessTreeExpViewtreeexpbarBase
     */
    public getExpItemView(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        if (Object.is(expmode, 'WAITCOST')) {
            return {  
                viewname: 'emwplist-wait-cost-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'POD')) {
            return {  
                viewname: 'emwplist-pod-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'ALLDETAIL')) {
            return {  
                viewname: 'empodetailgrid-view', 
                parentdata: {},
                deKeyField:'empodetail'
			};
        }
        if (Object.is(expmode, 'WAITIN')) {
            return {  
                viewname: 'emitem-rin-wait-in-grid-view', 
                parentdata: {},
                deKeyField:'emitemrin'
			};
        }
        if (Object.is(expmode, 'WAITPO')) {
            return {  
                viewname: 'emwplist-wait-po-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'PUTIN')) {
            return {  
                viewname: 'emitem-rin-put-in-grid-view', 
                parentdata: {},
                deKeyField:'emitemrin'
			};
        }
        if (Object.is(expmode, 'CLOSEDORDER')) {
            return {  
                viewname: 'empoclosed-order-grid-view', 
                parentdata: {},
                deKeyField:'empo'
			};
        }
        if (Object.is(expmode, 'DRAFT')) {
            return {  
                viewname: 'emwplist-draft-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'ALLWPLIST')) {
            return {  
                viewname: 'emwplistgrid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'PLACEORDER')) {
            return {  
                viewname: 'empoplace-order-grid-view', 
                parentdata: {},
                deKeyField:'empo'
			};
        }
        if (Object.is(expmode, 'WAITBOOK')) {
            return {  
                viewname: 'empodetail-wait-book-grid-view', 
                parentdata: {},
                deKeyField:'empodetail'
			};
        }
        if (Object.is(expmode, 'CLOSEDDETAIL')) {
            return {  
                viewname: 'empodetail-closed-detail-grid-view', 
                parentdata: {},
                deKeyField:'empodetail'
			};
        }
        if (Object.is(expmode, 'ALLPO')) {
            return {  
                viewname: 'empogrid-view', 
                parentdata: {},
                deKeyField:'empo'
			};
        }
        if (Object.is(expmode, 'ONORDER')) {
            return {  
                viewname: 'empoon-order-grid-view', 
                parentdata: {},
                deKeyField:'empo'
			};
        }
        if (Object.is(expmode, 'CONFIMCOST')) {
            return {  
                viewname: 'emwplist-confim-cost-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'WAITCHECK')) {
            return {  
                viewname: 'empodetail-wait-check-grid-view', 
                parentdata: {},
                deKeyField:'empodetail'
			};
        }
        if (Object.is(expmode, 'ALLRIN')) {
            return {  
                viewname: 'emitem-rin-grid-view', 
                parentdata: {},
                deKeyField:'emitemrin'
			};
        }
        if (Object.is(expmode, 'CANCEL')) {
            return {  
                viewname: 'emwplist-cancel-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        if (Object.is(expmode, 'IN')) {
            return {  
                viewname: 'emwplist-in-grid-view', 
                parentdata: {},
                deKeyField:'emwplist'
			};
        }
        return null;
    }

    /**
    * 执行mounted后的逻辑
    *
    * @memberof WpProcessTreeExpViewtreeexpbarBase
    */
    public ctrlMounted(){ 
        if(this.$store.getters.getViewSplit(this.viewUID)){
            this.split = this.$store.getters.getViewSplit(this.viewUID);
        }else{
            let containerWidth:number = (document.getElementById("wpprocesstreeexpviewtreeexpbar") as any).offsetWidth;
            if(this.ctrlWidth){
                    this.split = this.ctrlWidth/containerWidth;
            }
            this.$store.commit("setViewSplit",{viewUID:this.viewUID,viewSplit:this.split}); 
        }  
    }

    /**
     * 视图数据加载完成
     *
     * @param {*} $event
     * @memberof WpProcessTreeExpViewtreeexpbarBase
     */
    public onViewLoad($event: any): void {
        this.$emit('load', $event);
    }
}