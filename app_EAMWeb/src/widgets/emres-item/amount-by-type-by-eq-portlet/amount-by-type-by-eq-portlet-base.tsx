import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, MainControlBase } from '@/studio-core';
import EMResItemService from '@/service/emres-item/emres-item-service';
import AmountByTypeByEQService from './amount-by-type-by-eq-portlet-service';
import EMResItemUIService from '@/uiservice/emres-item/emres-item-ui-service';
import { Environment } from '@/environments/environment';
import UIService from '@/uiservice/ui-service';

/**
 * dashboard_sysportlet6部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {AmountByTypeByEQPortletBase}
 */
export class AmountByTypeByEQPortletBase extends MainControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof AmountByTypeByEQPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {AmountByTypeByEQService}
     * @memberof AmountByTypeByEQPortletBase
     */
    public service: AmountByTypeByEQService = new AmountByTypeByEQService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMResItemService}
     * @memberof AmountByTypeByEQPortletBase
     */
    public appEntityService: EMResItemService = new EMResItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AmountByTypeByEQPortletBase
     */
    protected appDeName: string = 'emresitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof AmountByTypeByEQPortletBase
     */
    protected appDeLogicName: string = '物品资源';

    /**
     * 界面UI服务对象
     *
     * @type {EMResItemUIService}
     * @memberof AmountByTypeByEQBase
     */  
    public appUIService: EMResItemUIService = new EMResItemUIService(this.$store);


    /**
     * 长度
     *
     * @type {number}
     * @memberof AmountByTypeByEQ
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof AmountByTypeByEQ
     */
    @Prop() public width?: number;

    /**
     * 门户部件类型
     *
     * @type {number}
     * @memberof AmountByTypeByEQBase
     */
    public portletType: string = 'chart';

    /**
     * 界面行为模型数据
     *
     * @memberof AmountByTypeByEQBase
     */
    public uiactionModel: any = {
    }



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof AmountByTypeByEQBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof AmountByTypeByEQBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof AmountByTypeByEQBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof AmountByTypeByEQBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return '400px';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof AmountByTypeByEQBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof AmountByTypeByEQBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if(Object.is(tag, "all-portlet") && Object.is(action,'loadmodel')){
                   this.calcUIActionAuthState(data);
                }
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof AmountByTypeByEQBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof AmountByTypeByEQBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 计算界面行为权限
     *
     * @memberof AmountByTypeByEQBase
     */
    public calcUIActionAuthState(data:any = {}) {
        //  如果是操作栏，不计算权限
        if(this.portletType && Object.is('actionbar', this.portletType)) {
            return;
        }
        let _this: any = this;
        let uiservice: any = _this.appUIService ? _this.appUIService : new UIService(_this.$store);
        if(_this.uiactionModel){
            ViewTool.calcActionItemAuthState(data,_this.uiactionModel,uiservice);
        }
    }


    /**
     * 刷新
     *
     * @memberof AmountByTypeByEQBase
     */
    public refresh(args?: any) {
      this.viewState.next({ tag: 'dashboard_sysportlet6_chart', action: 'refresh', data: args });
    }

}
