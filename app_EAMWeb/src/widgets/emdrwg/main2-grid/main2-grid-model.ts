/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'drwgcode',
          prop: 'drwgcode',
          dataType: 'TEXT',
        },
        {
          name: 'emdrwgname',
          prop: 'emdrwgname',
          dataType: 'TEXT',
        },
        {
          name: 'drwgtype',
          prop: 'drwgtype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'drwgstate',
          prop: 'drwgstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'lct',
          prop: 'lct',
          dataType: 'TEXT',
        },
        {
          name: 'efilecontent',
          prop: 'efilecontent',
          dataType: 'LONGTEXT',
        },
        {
          name: 'bpersonname',
          prop: 'bpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emdrwgname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emdrwgid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emdrwgid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emdrwg',
          prop: 'emdrwgid',
        },
      {
        name: 'n_drwgcode_like',
        prop: 'n_drwgcode_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emdrwgname_like',
        prop: 'n_emdrwgname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_drwgtype_eq',
        prop: 'n_drwgtype_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_drwgstate_eq',
        prop: 'n_drwgstate_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}