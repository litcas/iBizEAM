/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'sequenceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'sequencename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'sequenceid',
        prop: 'sequenceid',
        dataType: 'GUID',
      },
      {
        name: 'sequencename',
        prop: 'sequencename',
        dataType: 'TEXT',
      },
      {
        name: 'code',
        prop: 'code',
        dataType: 'TEXT',
      },
      {
        name: 'implementation',
        prop: 'implementation',
        dataType: 'TEXT',
      },
      {
        name: 'prefix',
        prop: 'prefix',
        dataType: 'TEXT',
      },
      {
        name: 'suffix',
        prop: 'suffix',
        dataType: 'TEXT',
      },
      {
        name: 'padding',
        prop: 'padding',
        dataType: 'INT',
      },
      {
        name: 'numincrement',
        prop: 'numincrement',
        dataType: 'INT',
      },
      {
        name: 'numnext',
        prop: 'numnext',
        dataType: 'INT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'enable',
        prop: 'enable',
        dataType: 'YESNO',
      },
      {
        name: 'sequence',
        prop: 'sequenceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}