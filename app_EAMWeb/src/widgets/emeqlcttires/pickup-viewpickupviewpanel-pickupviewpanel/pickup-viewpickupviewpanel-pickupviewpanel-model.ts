/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'tiresstate',
      },
      {
        name: 'description',
      },
      {
        name: 'par',
      },
      {
        name: 'amount',
      },
      {
        name: 'enable',
      },
      {
        name: 'picparams',
      },
      {
        name: 'valve',
      },
      {
        name: 'replacedate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'eqmodelcode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'newoldflag',
      },
      {
        name: 'changp',
      },
      {
        name: 'systemparam',
      },
      {
        name: 'replacereason',
      },
      {
        name: 'lctdesc',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'tirestype',
      },
      {
        name: 'lcttiresinfo',
      },
      {
        name: 'haveinner',
      },
      {
        name: 'updateman',
      },
      {
        name: 'mccode',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'eqlocationinfo',
      },
      {
        name: 'equipname',
      },
      {
        name: 'equipid',
      },
      {
        name: 'emeqlcttires',
        prop: 'emeqlocationid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'mserviceid',
      },
    ]
  }


}