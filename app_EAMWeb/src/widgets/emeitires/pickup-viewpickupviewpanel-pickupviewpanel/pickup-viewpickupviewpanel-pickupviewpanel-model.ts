/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'empid',
      },
      {
        name: 'eicaminfo',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'macaddr',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emeitiresname',
      },
      {
        name: 'eistate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'disdesc',
      },
      {
        name: 'empname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emeitires',
        prop: 'emeitiresid',
      },
      {
        name: 'disdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'eqmodelcode',
      },
      {
        name: 'enable',
      },
      {
        name: 'equipname',
      },
      {
        name: 'itempusename',
      },
      {
        name: 'eqlocationname',
      },
      {
        name: 'eqlocationid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'itempuseid',
      },
    ]
  }


}