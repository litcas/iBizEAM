/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emplantemplid',
          prop: 'emplantemplid',
          dataType: 'GUID',
        },
        {
          name: 'emplantemplname',
          prop: 'emplantemplname',
          dataType: 'TEXT',
        },
        {
          name: 'plantype',
          prop: 'plantype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'planstate',
          prop: 'planstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'recvpersonname',
          prop: 'recvpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'mpersonname',
          prop: 'mpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'mdate',
          prop: 'mdate',
          dataType: 'DATETIME',
        },
        {
          name: 'activelengths',
          prop: 'activelengths',
          dataType: 'FLOAT',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'plandesc',
          prop: 'plandesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'mtflag',
          prop: 'mtflag',
          dataType: 'YESNO',
        },
        {
          name: 'prefee',
          prop: 'prefee',
          dataType: 'CURRENCY',
        },
        {
          name: 'archive',
          prop: 'archive',
          dataType: 'SMCODELIST',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emplantemplname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emplantemplid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emplantemplid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emplantempl',
          prop: 'emplantemplid',
        },
      {
        name: 'n_emplantemplname_like',
        prop: 'n_emplantemplname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_plantype_eq',
        prop: 'n_plantype_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}